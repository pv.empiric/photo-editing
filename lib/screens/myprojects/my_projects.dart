import 'package:flutter/material.dart';
import 'package:image_editing_flutter_app/screens/myprojects/components/Body.dart';

class MyProjects extends StatefulWidget {
  final String title;

  const MyProjects({Key key, this.title}) : super(key: key);

  @override
  _MyProjectsState createState() => _MyProjectsState(title);
}

class _MyProjectsState extends State<MyProjects> {
  final String title;
  _MyProjectsState(this.title);

  @override
  Widget build(BuildContext context) {
    return Body(title);
  }
}
