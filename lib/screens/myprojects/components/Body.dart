import 'dart:convert';
import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/components/textStyle.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/components/transitions.dart';
import 'package:image_editing_flutter_app/models/draft_template_model.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/models/my_projects.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/Custom_Constants/database_helper.dart';
import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate0/CelebrationTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate1/CelebrationTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate2/CelebrationTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate3/CelebrationTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate0/CoolTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate1/CoolTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate19/CoolTemplate19.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate2/CoolTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate20/CoolTemplate20.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate21/CoolTemplate21.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate3/CoolTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate4/CoolTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate5/CoolTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate6/CoolTemplate6.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate7/CoolTemplate7.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate8/CoolTemplate8.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate9/CoolTemplate9.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate10/CoolTemplate10.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate11/CoolTemplate11.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate12/CoolTemplate12.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate13/CoolTemplate13.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate14/CoolTemplate14.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate15/CoolTemplate15.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate16/CoolTemplate16.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate17/CoolTemplate17.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate18/CoolTemplate18.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate22/CoolTemplate22.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate23/CoolTemplate23.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate24/CoolTemplate24.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate0/FestivalTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate1/FestivalTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate2/FestivalTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate3/FestivalTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate4/FestivalTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate5/FestivalTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template0/FunuralTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template1/FunuralTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template2/FunuralTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template3/FunuralTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template4/FunuralTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template5/FunuralTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template6/FunuralTemplate6.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template7/FunuralTemplate7.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template8/FunuralTemplate8.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate0/NewTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate1/NewTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate2/NewTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate3/NewTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate4/NewTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate5/NewTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate6/NewTemplate6.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate7/NewTemplate7.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate8/NewTemplate8.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate9/NewTemplate9.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate10/NewTemplate10.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate11/NewTemplate11.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate12/NewTemplate12.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate13/NewTemplate13.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate14/NewTemplate14.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate0/SimpleTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate1/SimpleTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate10/SimpleTemplate10.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate11/SimpleTemplate11.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate12/SimpleTemplate12.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate13/SimpleTemplate13.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate14/SimpleTemplate14.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate15/SimpleTemplate15.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate16/SimpleTemplate16.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate17/SimpleTemplate17.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate18/SimpleTemplate18.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate19/SimpleTemplate19.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate2/SimpleTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate3/SimpleTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate4/SimpleTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate5/SimpleTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate6/SimpleTemplate6.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate7/SimpleTemplate7.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate8/SimpleTemplate8.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate9/SimpleTemplate9.dart';
import 'package:lottie/lottie.dart';
import 'package:image_editing_flutter_app/models/drag_drop.dart';
// ignore: unused_import
// import 'package:image_editing_flutter_app/screens/templates/new/Template1/photo_edit.dart';
import 'dart:io' as io;
import 'package:share/share.dart';
import 'package:image_editing_flutter_app/models/saved_template_model.dart';

class Body extends StatefulWidget {
  final String title;
  Body(this.title);

  @override
  _BodyState createState() => _BodyState(title);
}

class _BodyState extends State<Body> {
  final String title;

  _BodyState(this.title);

  String directory;
  List file = [];
  // List<Map<int,String>> imgList = [{}];
  List dateTime = [];
  // List<SavedTemplate> dataList = [];
  EditZoomController eC = Get.put(EditZoomController());

  @override
  void initState() {
    // print(eC.respomse_json.value);
    super.initState();
    // getdata();
  }

  DragDrop dataPotraid;

  @override
  Widget build(BuildContext context) {
    // ListofFiles();
     dataPotraid = DragDrop(
        screenwidth: MediaQuery.of(context).size.width,
        screenheight: MediaQuery.of(context).size.height - 80,
        // width: MediaQuery.of(context).size.width -
        //     2 * MediaQuery.of(context).size.width / 60,
        // height: (MediaQuery.of(context).size.width -
        //             2 * MediaQuery.of(context).size.width / 60) /
        //         double.parse(response["Templates"]["Main-Container"][0]
        //             ["Aspect-ratio"]) -
        //     MediaQuery.of(context).size.height / 60,
      );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding:
              const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
          child: Text(
            "My drafts",
            style: GoogleFonts.abrilFatface(
                fontSize: 25.0, color: Colors.grey[800]),
          ),
        ),
        Expanded(
                    child: Padding(
                      padding:  EdgeInsets.only(left: 10.0),
                      child: FutureBuilder(
                future: eC.ListofFilesDraft(),
                builder: (context, snapshot) {
            if (snapshot.hasData) {

              return Obx((){
                if (snapshot.data.length != 0) {
                  return GridView.builder(

                  // shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
            itemCount: eC.imgListDraft.length,
            gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 7.5,
              mainAxisSpacing: 7.5,
              childAspectRatio: 1.2,
            ),
            itemBuilder: (context, index) {
              print(snapshot.data[index][snapshot.data[index].keys.first]
              .replaceAll('File:', "")
              .replaceAll('\'', "")
              .replaceAll(' ', ""));
              return InkWell(
            onTap: () {
              dialogDraft(
            File(snapshot.data[index][snapshot.data[index].keys.first]
                .replaceAll('File:', "")
                .replaceAll('\'', "")
                .replaceAll(' ', "")),
            eC.dataListDraft[index]);
            },
            child: ZoomIn(
                            child: FadeInLeft(
                              child: Container(
                  height: 100,
                  child: Image.file(
                      File(snapshot.data[index][snapshot.data[index].keys.first]
                  .replaceAll('File:', "")
                  .replaceAll('\'', "")
                  .replaceAll(' ', "")),
                      fit: BoxFit.contain,
                  ),
                ),
              ),
            ) 
              );
            });
                }
                else{
                       return Center(
                         child: Lottie.asset('assets/lottie/no_data_found.json',
            width: MediaQuery.of(context).size.width * 0.75,
            height: MediaQuery.of(context).size.height * 0.65),
                       );
                }
              });
             
            } else {
              return Center(
                child: Lottie.asset('assets/lottie/no_data_found.json',
                      width: MediaQuery.of(context).size.width * 0.75,
                      height: MediaQuery.of(context).size.height * 0.65),
              );
            }
                },
              ),
                    ),
        ),
        Padding(
          padding:
              const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
          child: Text(
            title,
            style: GoogleFonts.abrilFatface(
                fontSize: 25.0, color: Colors.grey[800]),
          ),
        ),
        Expanded(
                    child: Padding(
                      padding:  EdgeInsets.only(left: 10.0),
                      child: FutureBuilder(
                future: eC.ListofFiles(),
                builder: (context, snapshot) {
            if (snapshot.hasData) {
              // print("snapshot.hasdata is   ${snapshot.hasData}");
              
                return Obx((){
                  if(snapshot.data.length != 0){
                      return GridView.builder(
                  // shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
            itemCount: eC.imgList.length,
            gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 7.5,
              mainAxisSpacing: 7.5,
              childAspectRatio: 1.2,
            ),
            itemBuilder: (context, index) {
              print(snapshot.data[index][snapshot.data[index].keys.first]
              .replaceAll('File:', "")
              .replaceAll('\'', "")
              .replaceAll(' ', ""));
              return InkWell(
            onTap: () {
              dialogMyProject(
            File(snapshot.data[index][snapshot.data[index].keys.first]
                .replaceAll('File:', "")
                .replaceAll('\'', "")
                .replaceAll(' ', "")),
            eC.dataList[index]);
            },
            child: index % 3 == 0 ? ZoomIn(
                            child: FadeInLeft(
                              child: Container(
                  height: 100,
                  child: Image.file(
                      File(snapshot.data[index][snapshot.data[index].keys.first]
                  .replaceAll('File:', "")
                  .replaceAll('\'', "")
                  .replaceAll(' ', "")),
                      fit: BoxFit.contain,
                  ),
                ),
              ),
            ) :
            index % 3 == 1 ?
            ZoomIn(
                            child: Container(
                height: 100,
                child: Image.file(
                  File(snapshot.data[index][snapshot.data[index].keys.first]
                .replaceAll('File:', "")
                .replaceAll('\'', "")
                .replaceAll(' ', "")),
                  fit: BoxFit.contain,
                ),
              ),
            ) :
            ZoomIn(
                            child: FadeInRight(
                              child: Container(
                  height: 100,
                  child: Image.file(
                      File(snapshot.data[index][snapshot.data[index].keys.first]
                  .replaceAll('File:', "")
                  .replaceAll('\'', "")
                  .replaceAll(' ', "")),
                      fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
              );
            });
                  }
                  else {
                return Center(
                  child: Lottie.asset('assets/lottie/no_data_found.json',
            width: MediaQuery.of(context).size.width * 0.75,
            height: MediaQuery.of(context).size.height * 0.65),
                );
              }
                });
               
              
            } else {
              return Center(
                child: Lottie.asset('assets/lottie/no_data_found.json',
                      width: MediaQuery.of(context).size.width * 0.75,
                      height: MediaQuery.of(context).size.height * 0.65),
              );
            }
                },
              ),
                    ),
        ),
      ],
    );
  }

  // ignore: missing_return
  Widget dialogDraft(io.File image, DraftTemplate dataPage) {
    
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (context) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Dialog(
              //this right here
              child: Container(
                child: Stack(
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Center(child: Image.file(image)),
                        // SizedBox(
                        //   height: 8,
                        // ),
                      ],
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: Padding(
                        // padding: EdgeInsets.all(20.0),
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          children: [
                            InkWell(
                              borderRadius: BorderRadius.circular(30.0),
                              onTap: () {
                                // eC.isEditTemplate.value = true;
                                // Conditions(dataPage);
                                DatabaseHelper.instance.deleteDraft(dataPage.draftId).then((value){
                                   eC.ListofFilesDraft();
                                   Navigator.pop(context);
                                });

                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        30.0)), //this right here
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Icon(Icons.delete),
                                ),
                                elevation: 5.0,
                              ),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            InkWell(
                              borderRadius: BorderRadius.circular(30.0),
                              onTap: () {
                                eC.isEditTemplate.value = true;
                                ConditionsDraft(dataPage);
                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        30.0)), //this right here
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Icon(Icons.edit),
                                ),
                                elevation: 5.0,
                              ),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            InkWell(
                              borderRadius: BorderRadius.circular(30.0),
                              onTap: () {
                                Share.shareFiles([image.path]);
                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        30.0)), //this right here
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Icon(Icons.share),
                                ),
                                elevation: 5.0,
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  // ignore: missing_return
  Widget dialogMyProject(io.File image, SavedTemplate dataPage) {
    
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (context) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Dialog(
              //this right here
              child: Container(
                child: Stack(
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Center(child: Image.file(image)),
                        // SizedBox(
                        //   height: 8,
                        // ),
                      ],
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: Padding(
                        // padding: EdgeInsets.all(20.0),
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          children: [
                            InkWell(
                              borderRadius: BorderRadius.circular(30.0),
                              onTap: () {
                                // eC.isEditTemplate.value = true;
                                // Conditions(dataPage);
                                DatabaseHelper.instance.delete(dataPage.id).then((value){
                                   eC.ListofFiles();
                                   Navigator.pop(context);
                                });

                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        30.0)), //this right here
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Icon(Icons.delete),
                                ),
                                elevation: 5.0,
                              ),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            InkWell(
                              borderRadius: BorderRadius.circular(30.0),
                              onTap: () {
                                eC.isEditTemplate.value = true;
                                ConditionsMyProject(dataPage);
                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        30.0)), //this right here
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Icon(Icons.edit),
                                ),
                                elevation: 5.0,
                              ),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            InkWell(
                              borderRadius: BorderRadius.circular(30.0),
                              onTap: () {
                                Share.shareFiles([image.path]);
                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        30.0)), //this right here
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Icon(Icons.share),
                                ),
                                elevation: 5.0,
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  // ignore: non_constant_identifier_names
  ConditionsDraft(DraftTemplate dataPage) {
    if (dataPage.draftTemplateId == 'fn_0') {
      Get.off(() => FunuralTemplate0(
            draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
            // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'fn_1') {
      Get.off(() => FunuralTemplate1(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'fn_2') {
      Get.off(() => FunuralTemplate2(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'fn_3') {
      Get.off(() => FunuralTemplate3(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'fn_4') {
      Get.off(() => FunuralTemplate4(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'fn_5') {
      Get.off(() => FunuralTemplate5(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'fn_6') {
      Get.off(() => FunuralTemplate6(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'fn_7') {
      Get.off(() => FunuralTemplate7(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'fn_8') {
      Get.off(() => FunuralTemplate8(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Cb_0') {
      Get.off(() => CelebrationTemplate0(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Cb_1') {
      Get.off(() => CelebrationTemplate1(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Cb_2') {
      Get.off(() => CelebrationTemplate2(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Cb_3') {
      Get.off(() => CelebrationTemplate3(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_00') {
      Get.off(() => CoolTemplate0(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_01') {
      Get.off(() => CoolTemplate1(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_02') {
      Get.off(() => CoolTemplate2(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_03') {
      Get.off(() => CoolTemplate3(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_04') {
      Get.off(() => CoolTemplate4(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_05') {
      Get.off(() => CoolTemplate5(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_06') {
      Get.off(() => CoolTemplate6(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_07') {
      Get.off(() => CoolTemplate7(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_08') {
      Get.off(() => CoolTemplate8(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_09') {
      Get.off(() => CoolTemplate9(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_10') {
      Get.off(() => CoolTemplate10(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_11') {
      Get.off(() => CoolTemplate11(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_12') {
      Get.off(() => CoolTemplate12(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_13') {
      Get.off(() => CoolTemplate13(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_14') {
      Get.off(() => CoolTemplate14(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_15') {
      Get.off(() => CoolTemplate15(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_16') {
      Get.off(() => CoolTemplate16(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_17') {
      Get.off(() => CoolTemplate17(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_18') {
      Get.off(() => CoolTemplate18(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_19') {
      Get.off(() => CoolTemplate19(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_20') {
      Get.off(() => CoolTemplate20(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_21') {
      Get.off(() => CoolTemplate21(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_22') {
      Get.off(() => CoolTemplate22(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_23') {
      Get.off(() => CoolTemplate23(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ct_24') {
      Get.off(() => CoolTemplate24(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ft_0') {
      Get.off(() => FestivalTemplate0(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ft_1') {
      Get.off(() => FestivalTemplate1(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ft_2') {
      Get.off(() => FestivalTemplate2(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ft_3') {
      Get.off(() => FestivalTemplate3(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ft_4') {
      Get.off(() => FestivalTemplate4(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Ft_5') {
      Get.off(() => FestivalTemplate5(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_00') {
      Get.off(() => NewTemplate0(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_01') {
      Get.off(() => NewTemplate1(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_02') {
      Get.off(() => NewTemplate2(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_03') {
      Get.off(() => NewTemplate3(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_04') {
      Get.off(() => NewTemplate4(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_05') {
      Get.off(() => NewTemplate5(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_06') {
      Get.off(() => NewTemplate6(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_07') {
      Get.off(() => NewTemplate7(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_08') {
      Get.off(() => NewTemplate8(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_09') {
      Get.off(() => NewTemplate9(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_10') {
      Get.off(() => NewTemplate10(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_11') {
      Get.off(() => NewTemplate11(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_12') {
      Get.off(() => NewTemplate12(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_13') {
      Get.off(() => NewTemplate13(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Nt_14') {
      Get.off(() => NewTemplate14(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_00') {
      Get.off(() => SimpleTemplate0(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_01') {
      Get.off(() => SimpleTemplate1(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_02') {
      Get.off(() => SimpleTemplate2(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_03') {
      Get.off(() => SimpleTemplate3(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_04') {
      Get.off(() => SimpleTemplate4(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_05') {
      Get.off(() => SimpleTemplate5(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_06') {
      Get.off(() => SimpleTemplate6(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_07') {
      Get.off(() => SimpleTemplate7(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_08') {
      Get.off(() => SimpleTemplate8(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_09') {
      Get.off(() => SimpleTemplate9(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_10') {
      Get.off(() => SimpleTemplate10(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_11') {
      Get.off(() => SimpleTemplate11(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_12') {
      Get.off(() => SimpleTemplate12(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_13') {
      Get.off(() => SimpleTemplate13(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_14') {
      Get.off(() => SimpleTemplate14(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_15') {
      Get.off(() => SimpleTemplate15(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_16') {
      Get.off(() => SimpleTemplate16(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_17') {
      Get.off(() => SimpleTemplate17(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_18') {
      Get.off(() => SimpleTemplate18(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.draftTemplateId == 'Si_19') {
      Get.off(() => SimpleTemplate19(
           draftJsonCode: dataPage.draftJsonCode,
            draftId: dataPage.draftId,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
             dataPotraid: dataPotraid,
          ));
    }
    
  }
 

  // ignore: non_constant_identifier_names
  ConditionsMyProject(SavedTemplate dataPage) {
    if (dataPage.templateId == 'fn_0') {
      Get.off(() => FunuralTemplate0(
            editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
            //// response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'fn_1') {
      Get.off(() => FunuralTemplate1(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'fn_2') {
      Get.off(() => FunuralTemplate2(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'fn_3') {
      Get.off(() => FunuralTemplate3(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'fn_4') {
      Get.off(() => FunuralTemplate4(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'fn_5') {
      Get.off(() => FunuralTemplate5(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'fn_6') {
      Get.off(() => FunuralTemplate6(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'fn_7') {
      Get.off(() => FunuralTemplate7(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'fn_8') {
      Get.off(() => FunuralTemplate8(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Cb_0') {
      Get.off(() => CelebrationTemplate0(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Cb_1') {
      Get.off(() => CelebrationTemplate1(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Cb_2') {
      Get.off(() => CelebrationTemplate2(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Cb_3') {
      Get.off(() => CelebrationTemplate3(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_00') {
      Get.off(() => CoolTemplate0(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_01') {
      Get.off(() => CoolTemplate1(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_02') {
      Get.off(() => CoolTemplate2(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_03') {
      Get.off(() => CoolTemplate3(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_04') {
      Get.off(() => CoolTemplate4(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_05') {
      Get.off(() => CoolTemplate5(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_06') {
      Get.off(() => CoolTemplate6(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_07') {
      Get.off(() => CoolTemplate7(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_08') {
      Get.off(() => CoolTemplate8(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_09') {
      Get.off(() => CoolTemplate9(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_10') {
      Get.off(() => CoolTemplate10(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_11') {
      Get.off(() => CoolTemplate11(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_12') {
      Get.off(() => CoolTemplate12(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_13') {
      Get.off(() => CoolTemplate13(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_14') {
      Get.off(() => CoolTemplate14(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_15') {
      Get.off(() => CoolTemplate15(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_16') {
      Get.off(() => CoolTemplate16(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_17') {
      Get.off(() => CoolTemplate17(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_18') {
      Get.off(() => CoolTemplate18(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_19') {
      Get.off(() => CoolTemplate19(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_20') {
      Get.off(() => CoolTemplate20(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_21') {
      Get.off(() => CoolTemplate21(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_22') {
      Get.off(() => CoolTemplate22(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_23') {
      Get.off(() => CoolTemplate23(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ct_24') {
      Get.off(() => CoolTemplate24(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ft_0') {
      Get.off(() => FestivalTemplate0(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ft_1') {
      Get.off(() => FestivalTemplate1(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ft_2') {
      Get.off(() => FestivalTemplate2(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ft_3') {
      Get.off(() => FestivalTemplate3(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ft_4') {
      Get.off(() => FestivalTemplate4(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Ft_5') {
      Get.off(() => FestivalTemplate5(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_00') {
      Get.off(() => NewTemplate0(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_01') {
      Get.off(() => NewTemplate1(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_02') {
      Get.off(() => NewTemplate2(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_03') {
      Get.off(() => NewTemplate3(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_04') {
      Get.off(() => NewTemplate4(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_05') {
      Get.off(() => NewTemplate5(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_06') {
      Get.off(() => NewTemplate6(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_07') {
      Get.off(() => NewTemplate7(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_08') {
      Get.off(() => NewTemplate8(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_09') {
      Get.off(() => NewTemplate9(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_10') {
      Get.off(() => NewTemplate10(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_11') {
      Get.off(() => NewTemplate11(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_12') {
      Get.off(() => NewTemplate12(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_13') {
      Get.off(() => NewTemplate13(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Nt_14') {
      Get.off(() => NewTemplate14(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_00') {
      Get.off(() => SimpleTemplate0(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_01') {
      Get.off(() => SimpleTemplate1(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_02') {
      Get.off(() => SimpleTemplate2(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_03') {
      Get.off(() => SimpleTemplate3(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_04') {
      Get.off(() => SimpleTemplate4(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_05') {
      Get.off(() => SimpleTemplate5(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_06') {
      Get.off(() => SimpleTemplate6(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_07') {
      Get.off(() => SimpleTemplate7(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_08') {
      Get.off(() => SimpleTemplate8(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_09') {
      Get.off(() => SimpleTemplate9(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_10') {
      Get.off(() => SimpleTemplate10(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_11') {
      Get.off(() => SimpleTemplate11(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_12') {
      Get.off(() => SimpleTemplate12(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_13') {
      Get.off(() => SimpleTemplate13(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_14') {
      Get.off(() => SimpleTemplate14(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_15') {
      Get.off(() => SimpleTemplate15(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_16') {
      Get.off(() => SimpleTemplate16(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_17') {
      Get.off(() => SimpleTemplate17(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_18') {
      Get.off(() => SimpleTemplate18(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    if (dataPage.templateId == 'Si_19') {
      Get.off(() => SimpleTemplate19(
           editjson: dataPage.jsonCode,
            editId: dataPage.id,
            width: MediaQuery.of(context).size.width -
                2 * (MediaQuery.of(context).size.width / 60),
           // response: jsonDecode(eC.respomse_json.value),
            dataPotraid: dataPotraid,
          ));
    }
    
  }
}

/*
// try {
    //   directory = (await getExternalStorageDirectory()).path;
    //   // setState(() {
    //   file = io.Directory("$directory")
    //       .listSync(); //use your folder name insted of resume.
    //   // });
    //   // print(file);
    //   file.forEach((element) {
    //     // print(element.path.split('/').last.split(".").last);
    //     if (element.path.split('/').last.split(".").last == "png" ||
    //         element.path.split('/').last.split(".").last == "jpg") {
    //       // print(element);

    //       imgList.add(Item(element));
    //     }
    //     var f = DateFormat('dd-MM-yyyy')
    //         .format(io.FileStat.statSync(element.path).modified);
    //     // print(f);
    //     dateTime.add(f);
    //   });
    //   return imgList;
    // } on Exception catch (e) {
    //   print(e);
    // }
    // // String fileName = directory.split('/').last;
    // // final stat = io.FileStat.statSync("test.dart");
    // // print(fileName);
    // // print(dateTime);
 */
