import 'package:flutter/material.dart';
import 'package:image_editing_flutter_app/components/lib/google_fonts.dart';
import 'package:image_editing_flutter_app/constant.dart';
import 'package:image_editing_flutter_app/models/radio_model.dart';

class RadioItem extends StatelessWidget {
  final RadioModel _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10.0),
        // margin: EdgeInsets.all(2.0),
        // height: 50.0,
        // width: 50.0,
        child: new Center(
    child:  FittedBox(
                child: Text(_item.buttonText,
      style: GoogleFonts.rye(
              color:
              _item.isSelected ? Colors.white : kPrimaryColor,
              //fontWeight: FontWeight.bold,
              fontSize: 10.0
      ),
          // style: new TextStyle(
          //     color:
          //     _item.isSelected ? Colors.white : kPrimaryColor,
          //     //fontWeight: FontWeight.bold,
          //     fontSize: 11.0),
              ),
    ),
        ),
        decoration: new BoxDecoration(
    color: _item.isSelected
        ? kPrimaryColor
        : Colors.transparent,
    border: new Border.all(
        width: 1.0,
        color: kPrimaryColor),
    borderRadius: const BorderRadius.all(const Radius.circular(5.0)),
        ),
      );
  }
}
