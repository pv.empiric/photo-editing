import 'dart:convert';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
// import 'package:image_editing_flutter_app/components/controller/apiController.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/components/textStyle.dart';
import 'package:image_editing_flutter_app/constant.dart';
import 'package:image_editing_flutter_app/models/drag_drop.dart';
import 'package:image_editing_flutter_app/models/radio_model.dart';
import 'package:image_editing_flutter_app/screens/collection/components/radio_item.dart';
import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate0/CelebrationTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate1/CelebrationTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate2/CelebrationTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate3/CelebrationTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate19/CoolTemplate19.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate20/CoolTemplate20.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate21/CoolTemplate21.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate5/CoolTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate0/FestivalTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate1/FestivalTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate2/FestivalTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate3/FestivalTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate4/FestivalTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate5/FestivalTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template0/FunuralTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template1/FunuralTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template2/FunuralTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template3/FunuralTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template4/FunuralTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template5/FunuralTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template6/FunuralTemplate6.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template7/FunuralTemplate7.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template8/FunuralTemplate8.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate0/SimpleTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate1/SimpleTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate10/SimpleTemplate10.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate11/SimpleTemplate11.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate12/SimpleTemplate12.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate13/SimpleTemplate13.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate14/SimpleTemplate14.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate15/SimpleTemplate15.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate16/SimpleTemplate16.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate17/SimpleTemplate17.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate18/SimpleTemplate18.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate19/SimpleTemplate19.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate2/SimpleTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate3/SimpleTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate4/SimpleTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate5/SimpleTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate6/SimpleTemplate6.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate7/SimpleTemplate7.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate8/SimpleTemplate8.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate9/SimpleTemplate9.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate0/CoolTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate1/CoolTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate2/CoolTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate3/CoolTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate4/CoolTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate6/CoolTemplate6.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate7/CoolTemplate7.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate8/CoolTemplate8.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate9/CoolTemplate9.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate10/CoolTemplate10.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate11/CoolTemplate11.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate12/CoolTemplate12.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate13/CoolTemplate13.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate14/CoolTemplate14.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate15/CoolTemplate15.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate16/CoolTemplate16.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate17/CoolTemplate17.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate18/CoolTemplate18.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate22/CoolTemplate22.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate23/CoolTemplate23.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate24/CoolTemplate24.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate0/NewTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate1/NewTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate2/NewTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate3/NewTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate4/NewTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate5/NewTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate6/NewTemplate6.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate7/NewTemplate7.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate8/NewTemplate8.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate9/NewTemplate9.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate10/NewTemplate10.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate11/NewTemplate11.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate12/NewTemplate12.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  // ApiController apiController = Get.put(ApiController());
  List<RadioModel> sampleData;
  var selectedIndex;

  @override
  void initState() {
    readJson();
    sampleData = <RadioModel>[];
    // ignore: todo
    // TODO: implement initState
    super.initState();
    sampleData.add(new RadioModel(true, 'Fashion'));
    sampleData.add(new RadioModel(false, 'Celebration'));
    sampleData.add(new RadioModel(false, 'Festival'));
    sampleData.add(new RadioModel(false, 'Simple'));
    sampleData.add(new RadioModel(false, 'Cool'));
    sampleData.add(new RadioModel(false, 'New'));
    selectedIndex = 0;
  }

  final List<String> items = <String>[
    "Item 1",
    "Item 2",
    "Item 3",
    "Item 4",
    "Item 5",
    "Item 6",
  ];

  var response;
  EditZoomController eC = Get.put(EditZoomController());
  // ignore: unused_element
  readJson() async {
    if (eC.respomse_json.value.length == 0) {
      http.Response data = await http.get('https://jsonkeeper.com/b/T97A');
      // final data = await rootBundle.loadString('assets/data.json');
      if (data.statusCode == 200) {
        setState(() {
          response = json.decode(data.body);
        });
      }
    }
    else{
      response = json.decode(eC.respomse_json.value);
    }

    // http.Response data1 = await http.get("http://165.227.248.244:3333/main/");
    // if (data1.statusCode == 200) {
    //   setState(() {
    //     apiResponse = json.decode(data1.body);
    //   });
    // }
  }

  var templateCategory;

  var dataPotraid;
  var funuralitemcount = 9;
  var celebrationitemcount = 4;
  var festivalitemcount = 6;
  var simpleitemcount = 20;
  var coolitemcount = 25;
  var newitemcount = 15;
  var temp;

  @override
  Widget build(BuildContext context) {
    if (response != null)
      dataPotraid = DragDrop(
        screenwidth: MediaQuery.of(context).size.width,
        screenheight: MediaQuery.of(context).size.height - 80,
        width: MediaQuery.of(context).size.width -
            2 * MediaQuery.of(context).size.width / 60,
        height: (MediaQuery.of(context).size.width -
                    2 * MediaQuery.of(context).size.width / 60) /
                double.parse(response["Templates"]["Main-Container"][0]
                    ["Aspect-ratio"]) -
            MediaQuery.of(context).size.height / 60,
      );

    return SafeArea(
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.vertical,
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                child: Text(
                  "Collections",
                  style: GoogleFonts.abrilFatface(fontSize: 25.0,color: Colors.grey[800]),
                ),
              ),
              Container(
                height: 50,
                // width: 80,
                margin: EdgeInsets.only(right: 10.0),
                child: ListView.builder(
                  physics: BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.all(5.0),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(5.0),
                        splashColor: kPrimaryColor,
                        onTap: () {
                          setState(() {
                            sampleData.forEach(
                                (element) => element.isSelected = false);
                            sampleData[index].isSelected = true;
                            selectedIndex = index;
                            if (selectedIndex == 0) {
                              setState(() {
                                temp = funuralitemcount;
                                funuralitemcount = 0;
                              });
                              Future.delayed(Duration(milliseconds: 50), () {
                                // funuralitemcount = temp;
                                setState(() {
                                  funuralitemcount = temp;
                                });
                              });
                            } else if (selectedIndex == 1) {
                              setState(() {
                                temp = celebrationitemcount;
                                celebrationitemcount = 0;
                              });
                              Future.delayed(Duration(milliseconds: 50), () {
                                // funuralitemcount = temp;
                                setState(() {
                                  celebrationitemcount = temp;
                                });
                              });
                            } else if (selectedIndex == 2) {
                              setState(() {
                                temp = festivalitemcount;
                                festivalitemcount = 0;
                              });
                              Future.delayed(Duration(milliseconds: 50), () {
                                // funuralitemcount = temp;
                                setState(() {
                                  festivalitemcount = temp;
                                });
                              });
                            } else if (selectedIndex == 3) {
                              setState(() {
                                temp = simpleitemcount;
                                simpleitemcount = 0;
                              });
                              Future.delayed(Duration(milliseconds: 50), () {
                                // funuralitemcount = temp;
                                setState(() {
                                  simpleitemcount = temp;
                                });
                              });
                            } else if (selectedIndex == 4) {
                              setState(() {
                                temp = coolitemcount;
                                coolitemcount = 0;
                              });
                              Future.delayed(Duration(milliseconds: 50), () {
                                // funuralitemcount = temp;
                                setState(() {
                                  coolitemcount = temp;
                                });
                              });
                            } else {
                              setState(() {
                                temp = newitemcount;
                                newitemcount = 0;
                              });
                              Future.delayed(Duration(milliseconds: 50), () {
                                // funuralitemcount = temp;
                                setState(() {
                                  newitemcount = temp;
                                });
                              });
                            }
                          });
                        },
                        child: Container(width: 80,child: RadioItem(sampleData[index],)),
                      ),
                    );
                  },
                  itemCount: sampleData.length,
                ),
              ),
              GridView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: selectedIndex == 0
                    ? funuralitemcount
                    : selectedIndex == 1
                        ? celebrationitemcount
                        : selectedIndex == 2
                            ? festivalitemcount
                            : selectedIndex == 3
                                ? simpleitemcount
                                : selectedIndex == 4
                                    ? coolitemcount
                                    : selectedIndex == 5
                                        ? newitemcount
                                        : 0,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3, childAspectRatio: 0.693),
                itemBuilder: (context, index) {
                  return AbsorbPointer(
                     absorbing: response == null ? true : false,
                                      child: InkWell(
                      onTap: () {
                        Navigator.push(context,
                            // ignore: missing_return
                            MaterialPageRoute(builder: (contex) {
                          if (selectedIndex == 0) {
                            if (index == 0) {
                              return FunuralTemplate0(
                                dataPotraid: dataPotraid,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                response: response,
                              );
                            }
                            if (index == 1) {
                              return FunuralTemplate1(
                                response: response,
                                // editjson: editjson,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                // dataPotraid: dataPotraid,
                                // response: response,
                              );
                            }

                            if (index == 2) {
                              return FunuralTemplate2(
                                //   dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 3) {
                              return FunuralTemplate3(
                                // dataPotraid: dataPotraid,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                response: response,
                              );
                            }
                            if (index == 4) {
                              return FunuralTemplate4(
                                // dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 5) {
                              return FunuralTemplate5(
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),

                                // dataPotraid: dataPotraid,
                                response: response,
                              );
                            }
                            if (index == 6) {
                              return FunuralTemplate6(
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                // dataPotraid: dataPotraid,
                                response: response,
                              );
                            }
                            if (index == 7) {
                              return FunuralTemplate7(
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                // dataPotraid: dataPotraid,
                                response: response,
                              );
                            }
                            if (index == 8) {
                              return FunuralTemplate8(
                                // dataPotraid: dataPotraid,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                response: response,
                              );
                            }
                          }
                          if (selectedIndex == 1) {
                            if (index == 0) {
                              return CelebrationTemplate0(
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                dataPotraid: dataPotraid,
                                response: response,
                              );
                            }
                            if (index == 1) {
                              return CelebrationTemplate1(
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                dataPotraid: dataPotraid,
                                response: response,
                              );
                            }
                            if (index == 2) {
                              return CelebrationTemplate2(
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                dataPotraid: dataPotraid,
                                response: response,
                              );
                            }
                            if (index == 3) {
                              return CelebrationTemplate3(
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                dataPotraid: dataPotraid,
                                response: response,
                              );
                            }
                          }
                          if (selectedIndex == 2) {
                            if (index == 0) {
                              return FestivalTemplate0(
                                dataPotraid: dataPotraid,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                response: response,
                              );
                            }
                            if (index == 1) {
                              return FestivalTemplate1(
                                dataPotraid: dataPotraid,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                response: response,
                              );
                            }
                            if (index == 2) {
                              return FestivalTemplate2(
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                dataPotraid: dataPotraid,
                                response: response,
                              );
                            }
                            if (index == 3) {
                              return FestivalTemplate3(
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                dataPotraid: dataPotraid,
                                response: response,
                              );
                            }
                            if (index == 4) {
                              return FestivalTemplate4(
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                dataPotraid: dataPotraid,
                                response: response,
                              );
                            }
                            if (index == 5) {
                              return FestivalTemplate5(
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                                dataPotraid: dataPotraid,
                                response: response,
                              );
                            }
                          }
                          if (selectedIndex == 3) {
                            if (index == 0) {
                              return SimpleTemplate0(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 1) {
                              return SimpleTemplate1(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 2) {
                              return SimpleTemplate2(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 3) {
                              return SimpleTemplate3(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 4) {
                              return SimpleTemplate4(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 5) {
                              return SimpleTemplate5(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 6) {
                              return SimpleTemplate6(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 7) {
                              return SimpleTemplate7(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 8) {
                              return SimpleTemplate8(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 9) {
                              return SimpleTemplate9(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 10) {
                              return SimpleTemplate10(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 11) {
                              return SimpleTemplate11(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 12) {
                              return SimpleTemplate12(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 13) {
                              return SimpleTemplate13(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 14) {
                              return SimpleTemplate14(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 15) {
                              return SimpleTemplate15(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 16) {
                              return SimpleTemplate16(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 17) {
                              return SimpleTemplate17(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 18) {
                              return SimpleTemplate18(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 19) {
                              return SimpleTemplate19(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                          }
                          if (selectedIndex == 4) {
                            if (index == 0) {
                              return CoolTemplate0(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 1) {
                              return CoolTemplate1(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 2) {
                              return CoolTemplate2(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 3) {
                              return CoolTemplate3(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 4) {
                              return CoolTemplate4(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 5) {
                              return CoolTemplate5(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 6) {
                              return CoolTemplate6(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 7) {
                              return CoolTemplate7(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 8) {
                              return CoolTemplate8(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 9) {
                              return CoolTemplate9(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 10) {
                              return CoolTemplate10(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 11) {
                              return CoolTemplate11(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 12) {
                              return CoolTemplate12(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 13) {
                              return CoolTemplate13(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 14) {
                              return CoolTemplate14(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 15) {
                              return CoolTemplate15(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 16) {
                              return CoolTemplate16(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 17) {
                              return CoolTemplate17(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 18) {
                              return CoolTemplate18(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 19) {
                              return CoolTemplate19(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 20) {
                              return CoolTemplate20(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 21) {
                              return CoolTemplate21(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 22) {
                              return CoolTemplate22(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 23) {
                              return CoolTemplate23(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 24) {
                              return CoolTemplate24(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                          }
                          if (selectedIndex == 5) {
                            if (index == 0) {
                              return NewTemplate0(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 1) {
                              return NewTemplate1(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 2) {
                              return NewTemplate2(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 3) {
                              return NewTemplate3(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 4) {
                              return NewTemplate4(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 5) {
                              return NewTemplate5(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 6) {
                              return NewTemplate6(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 7) {
                              return NewTemplate7(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 8) {
                              return NewTemplate8(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 9) {
                              return NewTemplate9(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 10) {
                              return NewTemplate10(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 11) {
                              return NewTemplate11(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                            if (index == 12) {
                              return NewTemplate12(
                                dataPotraid: dataPotraid,
                                response: response,
                                width: dataPotraid.screenwidth -
                                    2 * (dataPotraid.screenwidth / 60),
                              );
                            }
                          }
                        }));
                      },
                      child: FadeInLeft(
                        child: Card(
                          margin: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 10.0),
                          elevation: 5.0,
                          child: Container(
                            height: 50,
                            decoration: BoxDecoration(
                                // color: Colors.yellow,
                                borderRadius: BorderRadius.circular(5.0)),
                            child: ZoomIn(
                              child: Image.asset(
                                  "assets/Thumbimage/${selectedIndex == 0 ? "Fashion" : selectedIndex == 1 ? "Celebration" : selectedIndex == 2 ? "Festival" : selectedIndex == 3 ? "Simple" : selectedIndex == 4 ? "Cool" : selectedIndex == 5 ? "New" : ""}/$index.PNG"),
                            ),
                            // color: Colors.yellow,
                          ),
                        ),
                      ),
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
