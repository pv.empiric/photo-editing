import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/database_helper.dart';
import 'package:path_provider/path_provider.dart';

// ignore: non_constant_identifier_names
Widget GetFancyShimmerImage(
    {String url, Color baseColor, Color highlightColor, BoxFit boxFit, double height,double width}) {
      if(baseColor == null){
        baseColor = Colors.grey[300];
      }
      if(highlightColor == null){
        highlightColor = Colors.grey[100];
      }
      if(boxFit == null){
        boxFit = BoxFit.fill;
      }
  return FancyShimmerImage(
    imageUrl: url,
    shimmerHighlightColor: highlightColor,
    shimmerBaseColor: baseColor,
    boxFit: boxFit,
    height: height,
    width: width,
  );
}


 Future savefileDraft(fileImage, finalImage,state) async {
    // RenderRepaintBoundary boundary =
    // widget.myglobalKey.currentContext.findRenderObject();
    // Image image = await boundary.toImage(pixelRatio: 4.0);
    final directory = (await getExternalStorageDirectory()).path;
    ByteData byteData =
        await finalImage.toByteData(format: ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();
    print(fileImage.path.split('/').last);
    File imgFile =
        File("$directory/${fileImage.path.split('/').last.toString()}");
   
      Map<String, dynamic> row;
      state.setState(() {
        state.imagePath = imgFile;
        row = {
          DatabaseHelper.draft_templateId: state.templateKey,
          DatabaseHelper.draft_imagepath: state.imagePath.toString(),
          DatabaseHelper.draft_jsonCode: state.jsonCode1
        };
      });
      if(state.draftId == null){
      final id = await DatabaseHelper.instance.insertDraft(row);
      print("draft added at index  $id");
      }
      else{
        // ignore: unused_local_variable
        final id = await DatabaseHelper.instance.updateDraft({
          DatabaseHelper.draft_primaryId: state.draftId,
          DatabaseHelper.draft_templateId: state.templateKey,
          DatabaseHelper.draft_imagepath: state.imagePath.toString(),
          DatabaseHelper.draft_jsonCode: state.jsonCode1
        }).then((value) => print("value of draft update $value"));
      }
      await imgFile.writeAsBytes(pngBytes);    
    
 }
