import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:image_editing_flutter_app/models/radio_model.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/templates/Funural/Template0/FunuralTemplate0.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/radio_item.dart';

// ignore: must_be_immutable
class CustomRadioColor extends StatefulWidget {
  
  var state;
  List<RadioModel1> sampleData1;
  int selectedIndex;
  CustomRadioColor({this.state,this.sampleData1,this.selectedIndex});
  @override
  _CustomRadioColorState createState() => _CustomRadioColorState();
}

class _CustomRadioColorState extends State<CustomRadioColor> {


  // int textSelectedIndex = 0;
  // void _onTextItemTapped(int index) {
  //   setState(() {
  //     textSelectedIndex = index;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 40,
        width: double.infinity,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return InkWell(
                onTap: () {
                  setState(() {
                    widget.sampleData1
                        .forEach((element) => element.isSelected = false);
                    widget.sampleData1[index].isSelected = true;
                    // _onTextItemTapped(index);
                    // textSelectedIndex = index;
                    widget.state.setState(() {
                      // widget.bodyState.textColor = widget.sampleData1[index].color;
                      var hexcolor = widget.sampleData1[index].color.value.toRadixString(16);
                    print(hexcolor);  
                      widget.state.textList[widget.selectedIndex].textColor = widget.sampleData1[index].color;
                    });
                  });
                },
                child: Container(
                    width: 40,
                    height: 30,
                    child: RadioItem1(widget.sampleData1[index])));
          },
          itemCount: widget.sampleData1.length,
        ));
  }
}