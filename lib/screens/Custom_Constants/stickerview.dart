import 'package:flutter/material.dart';
import 'package:gesture_x_detector/gesture_x_detector.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
import 'package:image_editing_flutter_app/models/sticker_list_model.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/widgets.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/templates/Funural/Template0/FunuralTemplate0.dart';

import 'package:photo_view/photo_view.dart';

// ignore: must_be_immutable
class StickerView extends StatefulWidget {
  var state;
  List<StickerList> stickerList;
  bool isImageEditing;

  StickerView({
    this.stickerList,
    this.isImageEditing,
    this.state,
  });
  @override
  _StickerViewState createState() => _StickerViewState();
}

class _StickerViewState extends State<StickerView> {
  List<PhotoViewController> _controller = [];
  EditZoomController editZoomController = Get.put(EditZoomController());
  Offset _initPos;
  Offset _currentPos = Offset(0, 0);
  double _currentScale;
  double _currentRotation;
  var screen;

  @override
  void initState() {
    screen = Size(widget.state.datawidth, widget.state.dataheight);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    for (var i = 0; i < widget.state.stickerList.length; i++) {
      _controller.add(PhotoViewController());
    }
    // ignore: unused_local_variable
    var x = 0;
    return Stack(
      children: widget.state.stickerList.map<Widget>((value) {
        return XGestureDetector(
          onDoubleTap: (_) {
            if (editZoomController.stickerview.value == 0) {
              editZoomController.stickerview.value =
                  widget.state.stickerList.indexOf(value) + 1;
            } else {
              editZoomController.stickerview.value = 0;
            }
            setState(() {
              // value.xPosition += 0.01;
              // value.yPosition += 0.01;
              value.scale = 1.0;
              value.rotation = 0.0;
            });
          },
          child: Stack(
            children: [
              GestureDetector(
                onScaleStart: (details) {
                  if (value == null) return;
                  _initPos = details.focalPoint;
                  _currentPos = Offset(value.xPosition, value.yPosition);
                  _currentScale = value.scale;
                  _currentRotation = value.rotation;
                },
                onScaleUpdate: (details) {
                  if (value == null) return;
                  final delta = details.focalPoint - _initPos;
                  final left = (delta.dx / screen.width) + _currentPos.dx;
                  final top = (delta.dy / screen.height) + _currentPos.dy;

                  setState(() {
                    value.xPosition = Offset(left, top).dx;
                    value.yPosition = Offset(left, top).dy;
                    value.rotation = details.rotation + _currentRotation;
                    value.scale = details.scale * _currentScale;
                  });
                },
                child: Stack(
                  children: [
                    editZoomController.stickerview.value ==
                            widget.state.stickerList.indexOf(value) + 1
                        ? Positioned(
                            top: value.yPosition * screen.height - 45,
                            left:
                                (value.xPosition * screen.width + value.width) -
                                    35,
                            child: IconButton(
                                icon: Icon(
                                  Icons.check,
                                  color: Colors.white,
                                  size: 20.0,
                                ),
                                onPressed: () {
                                  editZoomController.stickerview.value = 0;
                                  setState(() {
                                    // value.xPosition += 0.01;
                                    // value.yPosition += 0.01;
                                  });
                                }),
                          )
                        : Container(),
                    editZoomController.stickerview.value ==
                            widget.state.stickerList.indexOf(value) + 1
                        ? Positioned(
                            top: value.yPosition * screen.height - 45,
                            left: value.xPosition * screen.width - 10,
                            child: IconButton(
                                icon: Icon(
                                  Icons.delete_outline_sharp,
                                  color: Colors.white,
                                  size: 20.0,
                                ),
                                onPressed: () {
                                  // state.containerList.remove(val);
                                  editZoomController.stickerview.value = 0;
                                  widget.state.stickerList.remove(value);

                                  setState(() {
                                    // value.xPosition += 0.01;
                                    // value.yPosition += 0.01;
                                  });
                                }),
                          )
                        : Container(),
                    Positioned(
                      left: value.xPosition * screen.width,
                      top: value.yPosition * screen.height,
                      child: Transform.scale(
                        scale: value.scale,
                        child: Transform.rotate(
                          angle: value.rotation,
                          child: Container(
                            height: value.height,
                            width: value.width,
                            child: FittedBox(
                              fit: BoxFit.fill,
                              child: Listener(
                                onPointerDown: (details) {
                                    // if (_inAction) return;
                                    // _inAction = true;
                                    // _activeItem = val;
                                    _initPos = details.position;
                                    _currentPos =
                                        Offset(value.xPosition, value.yPosition);
                                    _currentScale = value.scale;
                                    _currentRotation = value.rotation;
                                  },
                                  onPointerUp: (details) {
                                    // _inAction = false;
                                  },
                                  child: GetFancyShimmerImage(
                                    url: value.name
                                  ),
                                  // child: Image.network(value.name),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      }).toList(),
    );
  }

  // ignore: non_constant_identifier_names
  StreamBuilder<PhotoViewControllerValue> Data(i) {
    return StreamBuilder(
      stream: _controller[i].outputStateStream,
      // ignore: missing_return
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Container(
            child: Text("."),
          );
        else {
          widget.state.stickerList[i].scale = snapshot.data.scale;
          widget.state.stickerList[i].rotation = snapshot.data.rotation;
        }
        return Opacity(
          opacity: 0,
          child: Container(),
        );
      },
    );
  }
}

/*

editZoomController.stickerview.value ==
                      widget.state.stickerList.indexOf(value) + 1
                  ? Positioned(
                      top: value.yPosition - 45,
                      left: value.xPosition + value.width - 30,
                      child: IconButton(
                          icon: Icon(
                            Icons.check,
                            color: Colors.white,
                            size: 20.0,
                          ),
                          onPressed: () {
                            editZoomController.stickerview.value = 0;
                            setState(() {
                              value.xPosition += 0.01;
                              value.yPosition += 0.01;
                            });
                          }),
                    )
                  : Container(),
              editZoomController.stickerview.value ==
                      widget.state.stickerList.indexOf(value) + 1
                  ? Positioned(
                      top: value.yPosition - 45,
                      left: value.xPosition - 10,
                      child: IconButton(
                          icon: Icon(
                            Icons.delete_outline_sharp,
                            color: Colors.white,
                            size: 20.0,
                          ),
                          onPressed: () {
                            // state.containerList.remove(val);
                            editZoomController.stickerview.value = 0;
                            widget.state.stickerList.remove(value);

                            setState(() {
                              value.xPosition += 0.01;
                              value.yPosition += 0.01;
                            });
                          }),
                    )
                  : Container(),
              Positioned(
                left: value.xPosition,
                top: value.yPosition,
                child: XGestureDetector(
                  onMoveUpdate: (detail) {
                    setState(() {
                      value.xPosition += detail.delta.dx;
                      value.yPosition += detail.delta.dy;
                      // "top will be :- ${value.yPosition / widget.state.dataheight}");
                    });
                  },
                  child: Container(
                    height: value.height,
                    width: value.width,
                    child: PhotoView.customChild(
                      controller:
                          _controller[widget.state.stickerList.indexOf(value)],
                      backgroundDecoration: BoxDecoration(
                        color: Colors.transparent,
                      ),
                      child: Image.network(value.name),
                      enableRotation: true,
                      minScale: PhotoViewComputedScale.contained * 0.3,
                      maxScale: PhotoViewComputedScale.covered * 5,
                    ),
                  ),
                ),
              ),
              if (x <= widget.state.stickerList.length) Data(x++),
 */
