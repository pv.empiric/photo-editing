import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_editing_flutter_app/components/textStyle.dart';
import 'package:image_editing_flutter_app/models/radio_model.dart';


class RadioItem extends StatelessWidget {
  final RadioModel _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: new EdgeInsets.all(7.0),
      child: new Container(
        padding: EdgeInsets.all(10.0),
        // height: 50.0,
        // width: 50.0,
        child: new Center(
          child: new Text(_item.buttonText,
              style: photoEditBottomTextStyle(_item.isSelected),
        ),
      ),
      )
    );
  }
}

class RadioItem1 extends StatelessWidget {
  final RadioModel1 _item;
  RadioItem1(this._item);
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: new EdgeInsets.all(5.0),
        child: new Container(
          padding: EdgeInsets.all(7.0),
          height: 20,
          width: 20,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
            color:_item.color,
          ),
          child: _item.isSelected ? SvgPicture.asset("assets/images/tick.svg"):SizedBox(),

        )
    );
  }
}
