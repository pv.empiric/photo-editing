import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/templates/Funural/Template0/FunuralTemplate0.dart';


// ignore: must_be_immutable
class OpacitySlider extends StatefulWidget {
  var state;
  int selectedIndex;
  OpacitySlider({this.state,this.selectedIndex});

  @override
  _OpacitySliderState createState() => _OpacitySliderState();
}

class _OpacitySliderState extends State<OpacitySlider> {

  double _opacityColorSliderPosition = 0;
  double opacity = 0.0;
  _colorOpacityChangeHandler(double position) {
    //handle out of bounds positions
    if (position > MediaQuery.of(context).size.width) {
      position = MediaQuery.of(context).size.width;
    }
    if (position < 0) {
      position = 0;
    } else if (position > 0) {
      // print(-100 * ((position - 1.0) / 1.0));
    }
    setState(() {
      _opacityColorSliderPosition = position;
      double myopacity = ((position * 100) / MediaQuery.of(context).size.width) / 100;
      // opacity = myopacity;
      widget.state.setState(() {
        widget.state.textOpacity = 1-myopacity;
        widget.state.textList[widget.selectedIndex].textOpacity = 1-myopacity;
      });
      // double countopacity = (position * 100) / SizeConfig.screenWidth;
      // print("-------------------------${1- opacity}-------------------------------");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Center(
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onHorizontalDragStart: (DragStartDetails details) {
              _colorOpacityChangeHandler(details.localPosition.dx);
            },
            onHorizontalDragUpdate: (DragUpdateDetails details) {
              _colorOpacityChangeHandler(details.localPosition.dx);
            },
            onTapDown: (TapDownDetails details) {
              _colorOpacityChangeHandler(details.localPosition.dx);
            },
            child: Padding(
              padding: EdgeInsets.only(top: 9),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 8,
                decoration: BoxDecoration(
                  border: Border.all(width: 2, color: Colors.transparent),
                  borderRadius: BorderRadius.circular(15),
                  gradient:
                  LinearGradient(colors: [Colors.black, Colors.white]),
                ),
                child: CustomPaint(
                  painter: _OpacitySliderIndicatorPainter(
                      _opacityColorSliderPosition),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }


}

class _OpacitySliderIndicatorPainter extends CustomPainter {
  final double position;
  _OpacitySliderIndicatorPainter(this.position);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawCircle(
        Offset(position, size.height / 2),
        11,
        Paint()..color = Colors.black45.withOpacity(0.2));
    canvas.drawCircle(Offset(position, size.height / 2),
        10, Paint()..color = Colors.white);
  }

  @override
  bool shouldRepaint(_OpacitySliderIndicatorPainter old) {
    return true;
  }
}