import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/templates/Funural/Template0/FunuralTemplate0.dart';
// ignore: must_be_immutable
class FontFamilyPage extends StatefulWidget {
  var state;


  final data;

  FontFamilyPage({Key key, this.data,this.state}) : super(key: key);

  @override
  _FontFamilyPageState createState() => _FontFamilyPageState();
}

class _FontFamilyPageState extends State<FontFamilyPage> {
  @override
  Widget build(BuildContext context) {
    List fonts =  widget.data;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: SvgPicture.asset("assets/images/backarrow.svg"),
          onPressed: (){
            Navigator.pop(context,true);
          },
        ),

        // actions: [
        //   GestureDetector(
        //     onTap: (){},
        //     child: Padding(
        //       padding: EdgeInsets.all(SizeConfig.defaultSize*1),
        //       child: Center(
        //         child: Text("Done",style: TextStyle(
        //           color: Colors.black
        //         ),),
        //       ),
        //     ),
        //   )
        // ],

      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
        itemCount: fonts.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: (){
              widget.state.setState(() {
                widget.state.textStyleIndex = index;
              });
              Navigator.pop(context,true);
              // print(fonts[index]());
            },
            child: Card(
              child: Center(
                  child: Text(
                      "Hello",
                    style: fonts[index](),
                  )
              ),
            ),
          );
        },
      ),
    );
  }
}