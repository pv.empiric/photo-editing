import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/templates/Funural/Template0/FunuralTemplate0.dart';

// ignore: must_be_immutable
class GridColors extends StatefulWidget {
 
  var state;
  int selectedIndex;

  final data;

  GridColors({Key key, this.data,this.state,this.selectedIndex}) : super(key: key);

  @override
  _GridColorsState createState() => _GridColorsState();
}

class _GridColorsState extends State<GridColors> {

  @override
  Widget build(BuildContext context) {
    List colors =  widget.data;
    // print(bodyState);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: SvgPicture.asset("assets/images/backarrow.svg"),
          onPressed: (){
            Navigator.pop(context,true);
          },
        ),
        actions: [
          GestureDetector(
            onTap: (){},
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Center(
                child: Text("Done",style: TextStyle(
                    color: Colors.black
                ),),
              ),
            ),
          )
        ],
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
        itemCount: colors.length,
        itemBuilder: (context, index) {
          // print(widget.data[index].isSelected);
          return GestureDetector(
            onTap: (){
              widget.state.setState(() {
                widget.state.textColor = widget.data[index].color as Color;
                widget.state.textList[widget.selectedIndex].textColor = widget.data[index].color as Color;
              });
              setState(() {
                widget.data
                    .forEach((element) => element.isSelected = false);
                widget.data[index].isSelected = true;
              });
              // widget.data[index].isSelected = true;
              Navigator.pop(context,true);
              // print(fonts[index]());
            },
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)
              ),
              color: widget.data[index].color,
              child: Center(
                child:widget.data[index].isSelected ?  SvgPicture.asset("assets/images/tick.svg") : SizedBox(),
              ),
            ),
          );
        },
      ),
    );
  }
}
