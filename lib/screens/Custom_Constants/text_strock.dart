import 'package:flutter/material.dart';
import 'package:image_editing_flutter_app/components/title_text.dart';




import './strock_slider.dart';

// ignore: must_be_immutable
class TextStrock extends StatefulWidget {
  var state;
  // Template0State template0state;
  // Template2State template2state;
  // Template3State template3state;
  // Template4State template4state;
  // Template5State template5state;
  // Template6State template6state;
  // BodyState bodyState;
  int selectedIndex;

  TextStrock({
    this.state,
    // this.template0state,
    // this.template2state,
    // this.template3state,
    // this.template4state,
    // this.template5state,
    // this.template6state,
    // this.bodyState,
    this.selectedIndex
      });

  @override
  _TextStrockState createState() => _TextStrockState();
}

class _TextStrockState extends State<TextStrock> {


  double _strockColorSliderPosition = 0;
  Color _currentStrockColor = Color.fromARGB(0, 0, 0, 0);

  final List<Color> _strockColors = [
    Color.fromARGB(255, 0, 0, 0),
    Color.fromARGB(255, 255, 255, 255),
    Color.fromARGB(255, 255, 0, 0),
    Color.fromARGB(255, 255, 128, 0),
    Color.fromARGB(255, 255, 255, 0),
    Color.fromARGB(255, 128, 255, 0),
    Color.fromARGB(255, 0, 255, 0),
    Color.fromARGB(255, 0, 255, 128),
    Color.fromARGB(255, 0, 255, 255),
    Color.fromARGB(255, 0, 128, 255),
    Color.fromARGB(255, 0, 0, 255),
    Color.fromARGB(255, 127, 0, 255),
    Color.fromARGB(255, 255, 0, 255),
    Color.fromARGB(255, 255, 0, 127),
    Color.fromARGB(255, 128, 128, 128),
  ];

  List textAlign = [
  ];

  @override
  void initState() {
    // if (widget.bodyState != null) {
    //   state = widget.bodyState;
    // } else if (widget.template0state != null) {
    //   state = widget.template0state;
    // } else if (widget.template2state != null){
    //   state = widget.template2state;
    // } else if (widget.template3state != null){
    //   state = widget.template3state;
    // } else if (widget.template4state != null){
    //   state = widget.template4state;
    // } else if (widget.template5state != null){
    //   state = widget.template5state;
    // } else if (widget.template6state != null){
    //   state = widget.template6state;
    // }
    super.initState();
  }
  
    
  

  

  @override
  Widget build(BuildContext context) {
    return Container(
      child:
      Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left:10.0),
            child: TitleText(titleName: "STROCK",),
          ),
          StrockSlider(widget.state,widget.selectedIndex),
          Padding(
            padding: EdgeInsets.only(left:10.0),
            child: TitleText(titleName: "COLOR",),
          ),
          Center(
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onHorizontalDragStart: (DragStartDetails details) {
                _strockcolorChangeHandler(details.localPosition.dx);
              },
              onHorizontalDragUpdate: (DragUpdateDetails details) {
                _strockcolorChangeHandler(details.localPosition.dx);
              },
              onTapDown: (TapDownDetails details) {
                _strockcolorChangeHandler(details.localPosition.dx);
              },
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 8,
                  decoration: BoxDecoration(
                    border: Border.all(width: 2, color: Colors.transparent),
                    borderRadius: BorderRadius.circular(15),
                    gradient: LinearGradient(colors: _strockColors),
                  ),
                  child: CustomPaint(
                    painter: _SliderIndicatorPainter(_strockColorSliderPosition),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }


  _strockcolorChangeHandler(double position) {
    //handle out of bounds positions
    if (position >  MediaQuery.of(context).size.width) {
      position =  MediaQuery.of(context).size.width;
    }
    if (position < 0) {
      position = 0;
    }
    setState(() {
    _strockColorSliderPosition = position;
    _currentStrockColor =
        _calculatesSliderSelectedColor(_strockColorSliderPosition);
    // finalColor = _currentColor;
      widget.state.setState(() {
        widget.state.textStrockColor = _currentStrockColor;
        widget.state.textList[widget.selectedIndex].strockColor = _currentStrockColor;
      });
    });
  }

  Color _calculatesSliderSelectedColor(double position) {
    //determine color
    double positionInColorArray =
    (position / MediaQuery.of(context).size.width * (_strockColors.length - 1));
    int index = positionInColorArray.truncate();
    double remainder = positionInColorArray - index;
    if (remainder == 0.0) {
      _currentStrockColor = _strockColors[index];
    } else {
      //calculate new color
      int redValue = _strockColors[index].red == _strockColors[index + 1].red
          ? _strockColors[index].red
          : (_strockColors[index].red +
          (_strockColors[index + 1].red - _strockColors[index].red) * remainder)
          .round();
      int greenValue = _strockColors[index].green == _strockColors[index + 1].green
          ? _strockColors[index].green
          : (_strockColors[index].green +
          (_strockColors[index + 1].green - _strockColors[index].green) * remainder)
          .round();
      int blueValue = _strockColors[index].blue == _strockColors[index + 1].blue
          ? _strockColors[index].blue
          : (_strockColors[index].blue +
          (_strockColors[index + 1].blue - _strockColors[index].blue) * remainder)
          .round();
      _currentStrockColor =
          Color.fromARGB(255, redValue, greenValue, blueValue);
    }
    return _currentStrockColor;
  }
}

class _SliderIndicatorPainter extends CustomPainter {
  final double position;
  _SliderIndicatorPainter(this.position);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawCircle(
        Offset(position, size.height / 2),
        11,
        Paint()..color = Colors.black45.withOpacity(0.2));
    canvas.drawCircle(Offset(position, size.height / 2),
       10, Paint()..color = Colors.white);
  }

  @override
  bool shouldRepaint(_SliderIndicatorPainter old) {
    return true;
  }
}