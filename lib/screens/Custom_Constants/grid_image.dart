import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_editing_flutter_app/models/radio_model.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/font_family.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/grid_colors.dart';


// ignore: must_be_immutable
class GridImage extends StatelessWidget {
  final data;
  var state;
  int selectedIndex;
  GridImage({Key key, this.data,this.state,this.selectedIndex}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    var fontList = <dynamic>[];
    var colorList = <RadioModel1>[];

    return GestureDetector(
      onTap: () {
        // print(data.runtimeType);
        if(data.runtimeType == fontList.runtimeType){
          // print("This is font list");
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return FontFamilyPage(
              data: data,
              state: state,
            );
          }));
        }else if(data.runtimeType == colorList.runtimeType){
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return GridColors(
              data: data,
              state: state,
              selectedIndex: selectedIndex,
            );
          }));

        }
        
      },
      child: Container(
        height: 30,
        width: 30,
        padding: EdgeInsets.all(5.0),
        // margin: EdgeInsets.all(defaultSize * 1),
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(color: Colors.black45, blurRadius: 3),
            ],
            borderRadius: BorderRadius.circular(50.0)),
        child: Center(
          child: SvgPicture.asset("assets/images/grid.svg"),
        ),
      ),
    );
  }
}