import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/SizeConfig.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/grid_image.dart';
import 'package:image_editing_flutter_app/components/title_text.dart';
import 'package:image_editing_flutter_app/models/text_align.dart';




// ignore: must_be_immutable
class EditText extends StatefulWidget {
  List fontStyleList;
  var state;
  final onScreenText;
  int myWidgetIndex;
  // ImageEditorState imageEditorState;

  EditText(
      {this.fontStyleList,
      this.state,
      this.onScreenText,
      this.myWidgetIndex,
         
      });
  @override
  _EditTextState createState() => _EditTextState();
}

class _EditTextState extends State<EditText> {
  List textAlign = [
    FinalTextAlign(
        textAlign: TextAlign.start,
        textAlignImage: "assets/textalignimages/alignLeft.svg",
        textDirection: TextDirection.ltr),
    FinalTextAlign(
        textAlign: TextAlign.center,
        textAlignImage: "assets/textalignimages/alignCenter.svg"),
    FinalTextAlign(
        // textAlign: TextAlign.end,
        textAlignImage: "assets/textalignimages/alignRight.svg",
        textDirection: TextDirection.rtl),
    FinalTextAlign(
        textAlign: TextAlign.justify,
        textAlignImage: "assets/textalignimages/alignLeftWithJustify.svg",
        textDirection: TextDirection.ltr),
    FinalTextAlign(
      textAlign: TextAlign.center,
      textAlignImage: "assets/textalignimages/alignCenterWithJustify.svg",
      // textDirection: TextDirection.values
    ),
    FinalTextAlign(
        textAlign: TextAlign.justify,
        textAlignImage: "assets/textalignimages/alignRightWithJustify.svg",
        textDirection: TextDirection.rtl),
    FinalTextAlign(
        textAlign: TextAlign.justify,
        textAlignImage: "assets/textalignimages/allJustify.svg")
  ];

  int textAlignIndex = 0;
  int fontSelectedIndex;

  int index = 0;

  

  @override
  Widget build(BuildContext context) {
    if (widget.myWidgetIndex != 0) {
      setState(() {
        index = widget.myWidgetIndex;
        // print("Edit Text ${widget.myWidgetIndex}");
      });
    } else {
      index = 0;
    }

    return Column(
        // mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TitleText(
                    titleName: "TEXT",
                  ),
                  GridImage(
                    data: widget.fontStyleList,
                    state: widget.state,
                  )
                ],
              )),
          Container(
              height: MediaQuery.of(context).size.height / 6,
              child: CupertinoPicker(
                itemExtent: 30,
                // key: key,
                scrollController: FixedExtentScrollController(
                  initialItem: index,
                ),
                useMagnifier: true,
                children: widget.fontStyleList.map((value) {
                  return Center(
                    child: Text(
                      "This is demo text",
                      // style: value(),
                      style: value(), 
                      textAlign: TextAlign.center,
                      
                    ),
                  );
                }).toList(),
                squeeze: 1.0,
                onSelectedItemChanged: (int index) {
                 widget.state.setState(() {
                    widget.state.textStyleIndex = index;
                    print("new index for font style will be ${widget.state.textStyleIndex}");
                     
                    // index = widget.bodyState.textStyleIndex;
                  });
                },
              ))
        ]);
  }
}
