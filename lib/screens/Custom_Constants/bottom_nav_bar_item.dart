import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/SizeConfig.dart';

class BottomNavBarItem extends StatelessWidget {
  final String image;
  final String title;
  final Function onPress;
  BottomNavBarItem({this.image,this.title,this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Column(
        children: [
          SvgPicture.asset(image),
          Text(title,
            style: TextStyle(
              fontSize: 10
          ),)
        ],
      ),
    );
  }
}
