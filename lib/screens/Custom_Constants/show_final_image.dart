import 'dart:async';
// ignore: unused_import
import 'dart:convert';
import 'dart:io';
import 'dart:math' as math;
import 'dart:typed_data';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
import 'package:path_provider/path_provider.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/database_helper.dart';

// ignore: must_be_immutable
class ShowImage extends StatefulWidget {
  File image;
  final image1;
  var state;
  ShowImage({this.image, this.image1, this.state});
  @override
  ShowImageState createState() => ShowImageState();
}

class ShowImageState extends State<ShowImage>
    with SingleTickerProviderStateMixin {
  AnimationController controller;

  Animation animation;

  double beginAnim = 0.0;
  double endAnim = 1.0;

  // ignore: unused_field
  Timer _timer;
  // ignore: unused_field
  int _start = 10;
  double end = 0;
  EditZoomController editZoomController = Get.put(EditZoomController());
  @override
  void initState() {
  
    // Timer(Duration(seconds: 5), (){
    //   print("Image closed");
    // });
    controller =
        AnimationController(duration: const Duration(seconds: 5), vsync: this);
    animation = Tween(begin: beginAnim, end: endAnim).animate(controller)
      ..addListener(() {
        setState(() {
          if (controller.value == endAnim) {
            Navigator.pop(context);
            editZoomController.isfinalimageProcess.value = false;
          }
          
          // Change here any Animation object value.
        });
      });
    // startTimer();
    super.initState();
  }

  @override
  void dispose() {
    // ignore: todo
    // TODO: implement dispose
    // _timer.cancel();
    controller.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    controller.forward();
    return Scaffold(
      backgroundColor: Colors.black,
      body: WillPopScope(
        // ignore: missing_return
        onWillPop: (){
          editZoomController.isfinalimageProcess.value = false;
          Navigator.pop(context);
        },
              child: SafeArea(
          child: GestureDetector(
            onPanUpdate: (details) {
              controller.stop();
            },
            onPanEnd: (details) {
              controller.forward();
              // editZoomController.isfinalimageProcess.value = false;
            },
            onLongPressStart: (details) {
              controller.stop();
            },
            onLongPress: () {
              // print("long pressed");
              // _timer.cancel();
              controller.stop();
            },
            onSecondaryLongPressStart: (details) {
              print("Started");
            },
            onLongPressEnd: (details) {
              // print("long pressed end");
              // startTimer();
              controller.forward();
              // controller.isAnimating;
            },
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // Icon(Icons.arrow_back, color: Colors.white),
                      IconButton(onPressed: (){
                        // controller.dispose();
                        Navigator.pop(context);
                        editZoomController.isfinalimageProcess.value = false;
                      }, icon: Icon(Icons.arrow_back, color: Colors.white,)),
                      IconButton(
                        splashColor: Colors.transparent,
                        onPressed: () async {
                          controller.stop();
                          savefile(widget.image, widget.image1,widget.state).then((value){
                            editZoomController.ListofFiles();
                          });
                        },
                        icon: Text(
                          "Save",
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                      // InkWell(
                      //   onTap: () async {
                      //     controller.stop();
                      //     _savefile();
                      //   },
                      //   child: Text(
                      //     "Save",
                      //     style: TextStyle(color: Colors.white),
                      //   ),
                      // )
                    ],
                  ),
                  Expanded(
                    child: Center(
                      child: Container(
                        child: Image.file(widget.image),
                      ),
                    ),
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: LinearProgressIndicator(
                      backgroundColor: Color(0XFF707070),
                      semanticsValue: "Hello",
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                      value: animation.value,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
    
  }

 Future savefile(fileImage, finalImage,state) async {
   editZoomController.isfinalimageProcess.value = false;
    // RenderRepaintBoundary boundary =
    // widget.myglobalKey.currentContext.findRenderObject();
    // Image image = await boundary.toImage(pixelRatio: 4.0);
    final directory = (await getExternalStorageDirectory()).path;
    ByteData byteData =
        await finalImage.toByteData(format: ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();
    print(fileImage.path.split('/').last);
    File imgFile =
        File("$directory/${fileImage.path.split('/').last.toString()}");

    await imgFile.writeAsBytes(pngBytes);

    if(state.editId == null && state.draftId == null){
      Map<String, dynamic> row;
      setState(() {
        state.imagePath = imgFile;
        row = {
          DatabaseHelper.templateid: state.templateKey,
          DatabaseHelper.imagepath: state.imagePath.toString(),
          DatabaseHelper.jsonCode: state.jsonCode1
        };
        state.isSaved = true;
        // print(DatabaseHelper.instance.queryAllRows());
      });
      await DatabaseHelper.instance.insert(row).then((value) => print(value));
    }
    else if(state.editId != null){
      // ignore: unused_local_variable
      // 
      state.setState(() {
          state.imagePath = imgFile;
      });
      // ignore: unused_local_variable
      final id = await DatabaseHelper.instance.update({
          DatabaseHelper.primaryId: state.editId,
          DatabaseHelper.templateid: state.templateKey,
          DatabaseHelper.imagepath: state.imagePath.toString(),
          DatabaseHelper.jsonCode: state.jsonCode1
        }).then((value) => print("value is $value"));
       
    }
    else if(state.draftId != null){
      Map<String, dynamic> row;
      setState(() {
        state.imagePath = imgFile;
        row = {
          DatabaseHelper.templateid: state.templateKey,
          DatabaseHelper.imagepath: state.imagePath.toString(),
          DatabaseHelper.jsonCode: state.jsonCode1
        };
        
        // print(DatabaseHelper.instance.queryAllRows());
      });
      await DatabaseHelper.instance.insert(row).then((value) => print(value));
      await DatabaseHelper.instance.deleteDraft(state.draftId).then((value){
        editZoomController.ListofFilesDraft();
      });
      
    }

    
    // if (myproject) {
    //   Map<String, dynamic> row;
    //   setState(() {
    //     widget.state.imagePath = imgFile;
    //     row = {
    //       DatabaseHelper.templateid: state.templateKey,
    //       DatabaseHelper.imagepath: state.imagePath.toString(),
    //       DatabaseHelper.jsonCode: state.jsonCode1
    //     };
    //     // print(DatabaseHelper.instance.queryAllRows());
    //   });
    //   if (widget.state.editId == null) {
    //     final id = await DatabaseHelper.instance.insert(row);
    //     print('inserted row id: $id');
    //   } else {
    //     // ignore: unused_local_variable
    //     final id = await DatabaseHelper.instance.update({
    //       DatabaseHelper.primaryId: state.editId,
    //       DatabaseHelper.templateid: state.templateKey,
    //       DatabaseHelper.imagepath: state.imagePath.toString(),
    //       DatabaseHelper.jsonCode: state.jsonCode1
    //     }).then((value) => print("value is $value"));
    //     // print('updated row id = $id');
    //   }
    // }
    // if(draft){
    //   Map<String, dynamic> row;
    //   state.setState(() {
    //     state.imagePath = imgFile;
    //     row = {
    //       DatabaseHelper.draft_templateId: state.templateKey,
    //       DatabaseHelper.draft_imagepath: state.imagePath.toString(),
    //       DatabaseHelper.draft_jsonCode: state.jsonCode1
    //     };
    //   });
    //   if(state.draftId == null){
    //   final id = await DatabaseHelper.instance.insertDraft(row);
    //   print("draft added at index  $id");
    //   }
    //   else{
    //     final id = await DatabaseHelper.instance.updateDraft({
    //       DatabaseHelper.draft_primaryId: state.draftId,
    //       DatabaseHelper.draft_templateId: state.templateKey,
    //       DatabaseHelper.draft_imagepath: state.imagePath.toString(),
    //       DatabaseHelper.draft_jsonCode: state.jsonCode1
    //     }).then((value) => print("value of draft update $value"));
    //   }
      

    // //   final data = await DatabaseHelper.instance.queryAllRowsDraft();
    // // data.forEach((element) {
    // //   print(data.length);
    // //   print(jsonDecode(jsonEncode(element).replaceAll('\\"', '"').replaceAll('\"[{', '[{').replaceAll('}]\"}', '}]}').replaceAll('\"{ "', '{"').replaceAll('}"}', '}}')));
    // // });
    // }

    
    await imgFile.writeAsBytes(pngBytes);
    Fluttertoast.showToast(
        msg: "Image Saved Successfully",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black45.withOpacity(0.5),
        textColor: Colors.white,
        fontSize: 16.0);
    
    controller.forward();
  }
}

class CustomTimerPainter extends CustomPainter {
  CustomTimerPainter({
    this.animation,
    this.backgroundColor,
    this.color,
  }) : super(repaint: animation);

  final Animation<double> animation;
  final Color backgroundColor, color;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = backgroundColor
      ..strokeWidth = 10.0
      ..strokeCap = StrokeCap.butt
      ..style = PaintingStyle.stroke;

    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);
    paint.color = color;
    double progress = (1.0 - animation.value) * 2 * math.pi;
    canvas.drawArc(Offset.zero & size, math.pi * 1.5, -progress, false, paint);
  }

  @override
  bool shouldRepaint(CustomTimerPainter old) {
    return animation.value != old.animation.value ||
        color != old.color ||
        backgroundColor != old.backgroundColor;
  }
}
