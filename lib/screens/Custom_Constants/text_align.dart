import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_editing_flutter_app/models/text_align.dart';



// ignore: must_be_immutable
class SetTextAlign extends StatefulWidget {
  var state;
  // Template0State template0state;
  // Template2State template2state;
  // Template3State template3state;
  // Template4State template4state;
  // Template5State template5state;
  // Template6State template6state;
  // BodyState bodyState;
  int selectedTextIndex;
  List textStyleList;
  SetTextAlign(
      {
        this.state,
      //   this.template0state,
      // this.template2state,
      // this.template3state,
      // this.template4state,
      // this.template6state,
      // this.template5state,
      // this.bodyState,
      this.selectedTextIndex,
      this.textStyleList});

  @override
  _SetTextAlignState createState() => _SetTextAlignState();
}

class _SetTextAlignState extends State<SetTextAlign> {
  List textAlign = [
    FinalTextAlign(
        textAlign: TextAlign.start,
        textAlignImage: "assets/textalignimages/alignLeft.svg",
        textDirection: TextDirection.ltr),
    FinalTextAlign(
        textAlign: TextAlign.center,
        textAlignImage: "assets/textalignimages/alignCenter.svg"),
    FinalTextAlign(
        // textAlign: TextAlign.end,
        textAlignImage: "assets/textalignimages/alignRight.svg",
        textDirection: TextDirection.rtl),
    FinalTextAlign(
        textAlign: TextAlign.justify,
        textAlignImage: "assets/textalignimages/alignLeftWithJustify.svg",
        textDirection: TextDirection.ltr),
    FinalTextAlign(
      textAlign: TextAlign.center,
      textAlignImage: "assets/textalignimages/alignCenterWithJustify.svg",
      // textDirection: TextDirection.values
    ),
    FinalTextAlign(
        textAlign: TextAlign.justify,
        textAlignImage: "assets/textalignimages/alignRightWithJustify.svg",
        textDirection: TextDirection.rtl),
    FinalTextAlign(
        textAlign: TextAlign.justify,
        textAlignImage: "assets/textalignimages/allJustify.svg")
  ];
  
  @override
  void initState() {
    // if (widget.bodyState != null) {
    //   state = widget.bodyState;
    // } else if (widget.template0state != null) {
    //   state = widget.template0state;
    // } else if (widget.template2state != null) {
    //   state = widget.template2state;
    // } else if (widget.template3state != null) {
    //   state = widget.template3state;
    // } else if (widget.template4state != null) {
    //   state = widget.template4state;
    // } else if (widget.template5state != null) {
    //   state = widget.template5state;
    // } else if (widget.template6state != null){
    //   state = widget.template6state;
    // }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // print("Text align index is : $selectedTextIndex");
    return Container(
      height: 100,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: textAlign.length,
        itemBuilder: (context, index) {
          // print();
          return Container(
            height: 80,
            width: 80,
            child: Card(
              margin: EdgeInsets.all(10),
              color: Colors.white,
              child: IconButton(
                onPressed: () {
                  // ignore: invalid_use_of_protected_member
                  widget.state.setState(() {
                    widget.state.textAlignIndex = index;
                    widget.state.textList[widget.selectedTextIndex]
                        .finalTextAlignIndex = index;
                  });
                  // setState(() {
                  //   textAlignIndex = index;
                  // });
                },
                icon: SvgPicture.asset(
                  textAlign[index].textAlignImage.toString(),
                  color: Colors.black,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
