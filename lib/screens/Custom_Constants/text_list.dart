import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:flutter_svg/svg.dart';
// ignore: unused_import
import 'package:gesture_x_detector/gesture_x_detector.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/components/stickerScalingDot.dart';
import 'package:image_editing_flutter_app/models/text_align.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/Custom_Constants/color_picker.dart';

// ignore: unused_import
import 'package:image_editing_flutter_app/screens/Custom_Constants/EditText.dart';
// import 'package:image_editing_flutter_app/screens/Custom_Constants/textlistnewcode.dart';
import 'package:photo_view/photo_view.dart';

// class EditableItem {
//   Offset position = Offset(0.1, 0.1);
//   double scale = 1.0;
//   double rotation = 0.0;
//   // ItemType type;
//   var value;
// }

// ignore: must_be_immutable
class TextList extends StatefulWidget {
  List textList;
  var state;

  bool isImageEditing;
  bool isTextEditing;
  List fontStyleList;
  int finalTextStyleIndex;
  TextList(
      {this.state,
      this.textList,
      this.isImageEditing,
      this.isTextEditing,
      this.fontStyleList,
      this.finalTextStyleIndex});

  @override
  _TextListState createState() => _TextListState();
}

class _TextListState extends State<TextList> {
  // ignore: unused_field
  List<TextEditingController> _editingController = [];
  // List<PhotoViewController> _photoviewController = [];
  // List<PhotoViewScaleStateController> scaleStateController = [];
  String tamplateText;
  // double finalAngle = 0.0;
  // double oldAngle = 0.0;
  // double upsetAngle = 0.0;
  // ignore: unused_field
  // double _imageHeightBaseScaleFactor = 1.0;
  // ignore: unused_field
  // double _imageWidthBaseScaleFactor = 1.0;
  // double previousAngle;
  // double previousHeight = 0.0;
  // double previousWidth = 0.0;
  // double stackScale;
  // double width = 100;
  bool isediting;

  // double temprotation;
  // double tempXposition;
  // double tempYposition;
  // double tempHeightscale;
  // double tempWidthscale;
  // double tempScale;
  EditZoomController ec = Get.put(EditZoomController());
  // EditableItem _activeItem;
  Offset _initPos;
  Offset _currentPos = Offset(0, 0);
  double _currentScale;
  double _currentRotation;
  // bool _inAction = true;
  var screen;
  @override
  void initState() {
    // _editingController = <TextEditingController>[];
    screen = Size(widget.state.datawidth, widget.state.dataheight);

    super.initState();
    // setData();
  }

  List textAlign = [
    FinalTextAlign(
        textAlign: TextAlign.start,
        textAlignImage: "assets/textalignimages/alignLeft.svg",
        textDirection: TextDirection.ltr),
    FinalTextAlign(
        textAlign: TextAlign.center,
        textAlignImage: "assets/textalignimages/alignCenter.svg"),
    FinalTextAlign(
        // textAlign: TextAlign.end,
        textAlignImage: "assets/textalignimages/alignRight.svg",
        textDirection: TextDirection.rtl),
    FinalTextAlign(
        textAlign: TextAlign.justify,
        textAlignImage: "assets/textalignimages/alignLeftWithJustify.svg",
        textDirection: TextDirection.ltr),
    FinalTextAlign(
      textAlign: TextAlign.justify,
      textAlignImage: "assets/textalignimages/alignCenterWithJustify.svg",
      // textDirection: TextDirection
    ),
    FinalTextAlign(
        textAlign: TextAlign.justify,
        textAlignImage: "assets/textalignimages/alignRightWithJustify.svg",
        textDirection: TextDirection.rtl),
    FinalTextAlign(
        textAlign: TextAlign.justify,
        textAlignImage: "assets/textalignimages/allJustify.svg")
  ];

  int editingIndex = null; // ignore: avoid_init_to_null

  void onScaleState(PhotoViewScaleState scaleState) {
    print(scaleState);
  }

  // void setData() {
  //   print(widget.textList.length);
  // }

  Widget build(BuildContext context) {
    for (var i = 0; i < widget.textList.length; i++) {
      _editingController.add(TextEditingController());

      // _photoviewController.add(PhotoViewController());
      // scaleStateController.add(PhotoViewScaleStateController()
      //   ..outputScaleStateStream.listen(onScaleState));
    }
    if (widget.textList.length != 0) {
      for (var i = 0; i < widget.textList.length; i++) {
        // _photoviewController[i].scale = widget.textList[i].scaleFactor;
        // _photoviewController[i].rotation = widget.textList[i].rotationAngle;
      }
    }
    if (editingIndex != null && ec.callDecoratedfun.value) {
      decoratedText();
    }
    // var x = 0;

    return Obx(() {
      return Stack(
        children: widget.textList.map((val) {
          return XGestureDetector(
            onTap: (_) {
              // print(widget.textList[widget.textList.indexOf(val)].text);
              editingIndex = widget.textList.indexOf(val);
              print(editingIndex);
              ec.isOpenTextbar.value = true;
              ec.isOpenWidget.value = true;
              ec.isOpenSticker.value = false;
              ec.callDecoratedfun.value = true;
              widget.state.setState(() {
                widget.state.textStyleEditingIndex =
                    widget.textList.indexOf(val);
                // widget.state.isTextediting = true;
                widget.state.textStyleIndex =
                    widget.textList[editingIndex].textStyleIndex;
                EditText().myWidgetIndex = widget.state.textStyleIndex;
                widget.state.myWidget = TextColorPicker(
                  state: widget.state,
                  width: MediaQuery.of(context).size.width,
                  selectedTextIndex: widget.state.textStyleEditingIndex,
                );
              });
            },
            onDoubleTap: (_) {
              // _editingController[widget.textList.indexOf(val)].text =
              //     widget.textList[widget.textList.indexOf(val)].text;
              ec.callDecoratedfun.value = true;
              if (ec.textediting.value == 0) {
                ec.textediting.value = widget.textList.indexOf(val) + 1;
              } else {
                ec.textediting.value = 0;
              }
            },
            child: Stack(
              children: [
                ec.textediting.value != widget.textList.indexOf(val) + 1
                    // ?
                    ? GestureDetector(
                        onScaleStart: (details) {
                          if (val == null) return;

                          _initPos = details.focalPoint;
                          _currentPos = Offset(val.xPosition, val.yPosition);
                          _currentScale = val.scaleFactor;
                          _currentRotation = val.rotationAngle;
                        },
                        onScaleUpdate: (details) {
                          if (val == null) return;
                          final delta = details.focalPoint - _initPos;
                          final left =
                              (delta.dx / screen.width) + _currentPos.dx;
                          final top =
                              (delta.dy / screen.height) + _currentPos.dy;

                          setState(() {
                            val.xPosition = Offset(left, top).dx;
                            val.yPosition = Offset(left, top).dy;
                            val.rotationAngle =
                                details.rotation + _currentRotation;
                            val.scaleFactor = details.scale * _currentScale;
                          });
                        },
                        child: Stack(
                          children: [
                            // Positioned(
                            //   top: 350,
                            //   left: 50,
                            //   // child: Text("dsjfhsa"),
                            // ),
                            Positioned(
                              left: val.xPosition * screen.width,
                              top: val.yPosition * screen.height,
                              // left: _currentPos.dx,
                              // top: _currentPos.dy,
                              
                                child: Transform.scale(
                                  scale: val.scaleFactor,
                                  
                                  child: Transform.rotate(
                                    angle: val.rotationAngle,
                                    child: Container(
                                      height: val.height,
                                      width: val.width,
                                      child: FittedBox(
                                        fit: BoxFit.fill,
                                      child: Listener(
                                        
                                        onPointerDown: (details) {
                                          // if (_inAction) return;
                                          // _inAction = true;
                                          // _activeItem = val;
                                          _initPos = details.position;
                                          _currentPos =
                                              Offset(val.xPosition, val.yPosition);
                                          _currentScale = val.scaleFactor;
                                          _currentRotation = val.rotationAngle;
                                        },
                                        onPointerUp: (details) {
                                          // _inAction = false;
                                        },
                                        child: Stack(
                                          children: [
                                            Text(
                                              val.text,

                                              style: val.textStyle,
                                              textAlign:
                                                  textAlign[val.finalTextAlignIndex]
                                                      .textAlign,
                                              textDirection:
                                                  textAlign[val.finalTextAlignIndex]
                                                      .textDirection,
                                              overflow: TextOverflow.fade,
                                            ),
                                            Text(
                                              val.text,
                                              style: val.textStyle2,
                                              textAlign:
                                                  textAlign[val.finalTextAlignIndex]
                                                      .textAlign,
                                              textDirection:
                                                  textAlign[val.finalTextAlignIndex]
                                                      .textDirection,
                                              overflow: TextOverflow.fade,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                            
                          ],
                        ))
                    : Stack(
                        children: [
                          Container(
                            height: widget.state.dataheight,
                            width: widget.state.datawidth,
                            decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.8),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  left: widget.state.datawidth * 0.35,
                                  top: widget.state.dataheight * 0.25,
                                  child: Container(
                                    height: 125,
                                    width: widget.state.datawidth * 0.50,
                                    child: Center(
                                      child: TextFormField(
                                        expands: true,
                                        maxLines: null,
                                        minLines: null,
                                        controller: _editingController[
                                            widget.textList.indexOf(val)],
                                        decoration: InputDecoration(
                                            border: InputBorder.none,
                                            hintText: "Enter Text"),
                                        keyboardType: TextInputType.multiline,
                                        onChanged: (value) {
                                          setState(() {
                                            val.text = value;
                                          });
                                        },
                                        autofocus: true,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            left: 15,
                            top: 15,
                            child: Container(
                              height: 30,
                              width: 30,
                              child: IconButton(
                                iconSize: 30.0,
                                icon: Icon(
                                  Icons.delete,
                                  color: Colors.black,
                                ),
                                onPressed: () {
                                  // print("Editing index is " + "${widget.textList.indexOf(val)}");
                                  // editingIndex = widget.textList.indexOf(val) - 1;
                                  ec.callDecoratedfun.value = false;
                                  widget.textList.remove(val);
                                  ec.isOpenTextbar.value = false;
                                  ec.isOpenWidget.value = false;
                                  widget.state.setState(() {
                                    widget.state.myWidget = null;     
                                  });
                                  ec.textediting.value = 0;
                                },
                              ),
                            ),
                          ),
                          Positioned(
                            right: 15,
                            top: 15,
                            child: Container(
                              height: 30,
                              width: 30,
                              child: IconButton(
                                iconSize: 30.0,
                                icon: Icon(
                                  Icons.check,
                                  color: Colors.black,
                                ),
                                onPressed: () {
                                  // decoratedText();
                                  ec.textediting.value = 0;
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                // if (x <= widget.textList.length - 1) Data(x++),
              ],
            ),
          );
        }).toList(),
      );
    });
  }

  
  // ignore: missing_return
  Widget decoratedText() {
    // if (widget.isTextEditing) {
    // print("editing index" + "$editingIndex");
    // print(widget.textList.length);
    if (editingIndex >= 0) {
      // widget.textList[editingIndex].textStyleIndex = widget.finalTextStyleIndex;
      widget.textList[editingIndex].textStyleIndex =
          widget.finalTextStyleIndex;

      widget.textList[editingIndex].textStyle = widget
              .fontStyleList[widget.state.textStyleIndex](
          foreground: Paint()
            ..style = PaintingStyle.stroke
            ..strokeMiterLimit = 10
            ..strokeWidth =
                widget.state.textList[editingIndex].strockValue /
                    10
            ..strokeJoin = StrokeJoin.miter
            ..strokeCap = StrokeCap.square
            ..color = widget.textList[editingIndex].strockColor
                .withOpacity(
                    widget.textList[editingIndex].textOpacity),
          fontSize: 14.0);

      widget.textList[editingIndex].textStyle2 =
          widget.fontStyleList[widget.state.textStyleIndex](
        color: widget.textList[editingIndex].textColor
            .withOpacity(
                widget.textList[editingIndex].textOpacity),
        fontSize: 14.0,
      );
      // }
    }
    // }
  }
  // ignore: missing_return
  // static decoratedText1(state,index) {
  //   // if (widget.isTextEditing) {
  //   // print("editing index" + "$editingIndex");
  //   // print(widget.textList.length);
  //     // widget.textList[editingIndex].textStyleIndex = widget.finalTextStyleIndex;
  //     state.textList[index].textStyleIndex =
  //         state.finalTextStyleIndex;

  //     state.textList[index].textStyle = state
  //             .fontStyleList[state.textStyleIndex](
  //         foreground: Paint()
  //           ..style = PaintingStyle.stroke
  //           ..strokeMiterLimit = 10
  //           ..strokeWidth =
  //               state.textList[index].strockValue /
  //                   10
  //           ..strokeJoin = StrokeJoin.miter
  //           ..strokeCap = StrokeCap.square
  //           ..color = state.textList[index].strockColor
  //               .withOpacity(
  //                   state.textList[index].textOpacity),
  //         fontSize: 14.0);

  //     state.textList[index].textStyle2 =
  //         state.fontStyleList[state.textStyleIndex](
  //       color: state.textList[index].textColor
  //           .withOpacity(
  //               state.textList[index].textOpacity),
  //       fontSize: 14.0,
  //     );
  //     // }
  //   }
    // }
  }



/* 
  Positioned(
                        top: val.yPosition,
                        left: val.xPosition,
                        child: XGestureDetector(
                          onMoveUpdate: (detail) {
                            setState(() {
                              val.xPosition += detail.delta.dx;
                              val.yPosition += detail.delta.dy;
                            });
                          },
                          child: Container(
                            height: val.height,
                            width: val.width,
                            child: PhotoView.customChild(
                              scaleStateController: scaleStateController[
                                  widget.textList.indexOf(val)],
                              controller: _photoviewController[
                                  widget.textList.indexOf(val)],
                              enableRotation: true,
                              backgroundDecoration:
                                  BoxDecoration(color: Colors.red),
                              child: Container(

                                // decoration:  BoxDecoration(
                                //   border: ec.textediting.value != 0 ? Border.all(color: Colors.black, width: 1.0) : Border(),
                                // ),
                                child: FittedBox(
                                  child: Center(
                                    child: Stack(
                                      children: [
                                        Text(
                                          val.text,
                                          style: val.textStyle,
                                          textAlign:
                                              textAlign[val.finalTextAlignIndex]
                                                  .textAlign,
                                          textDirection:
                                              textAlign[val.finalTextAlignIndex]
                                                  .textDirection,
                                          overflow: TextOverflow.fade,
                                        ),
                                        Text(
                                          val.text,
                                          style: val.textStyle2,
                                          textAlign:
                                              textAlign[val.finalTextAlignIndex]
                                                  .textAlign,
                                          textDirection:
                                              textAlign[val.finalTextAlignIndex]
                                                  .textDirection,
                                          overflow: TextOverflow.fade,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        // ),
                      )
*/
