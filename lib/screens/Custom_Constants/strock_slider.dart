import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/templates/Funural/Template0/FunuralTemplate0.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/templates/Funural/Template2/FunuralTemplate2.dart';
// ignore: must_be_immutable
class StrockSlider extends StatefulWidget {
  var state;
  int selectedIndex;
  StrockSlider(this.state,this.selectedIndex);

  @override
  _StrockSliderState createState() => _StrockSliderState();
}

class _StrockSliderState extends State<StrockSlider> {

  // ignore: unused_field
  double _currentSliderValue = 0;
  @override
  Widget build(BuildContext context) {
    return SliderTheme(
      data: SliderTheme.of(context).copyWith(
        activeTrackColor: Colors.black,
        inactiveTrackColor: Color(0XFF707070),
        thumbShape: RoundSliderThumbShape(enabledThumbRadius: 0.0),
        thumbColor: Colors.black,
        activeTickMarkColor: Colors.black,
        inactiveTickMarkColor: Colors.black,
        tickMarkShape: RoundSliderTickMarkShape(),
        valueIndicatorShape: PaddleSliderValueIndicatorShape(),
        valueIndicatorColor: Colors.black,
        valueIndicatorTextStyle: TextStyle(
          color: Colors.white,
        ),
      ),
      child: Slider(
        min: 0,
        max: 100,
        divisions: 1000,
        label: widget.state.strockSliderValue.toStringAsFixed(1),
        value: widget.state.strockSliderValue,
        onChanged: (value) {
          setState(() {
            // _currentSliderValue = value;
            // print(value);
            widget.state.setState(() {
              widget.state.strockSliderValue = value;
              widget.state.textList[widget.selectedIndex].strockValue = value;
            });
          }
          );
        },
      ),
    );
  }
}
