import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
import 'package:image_editing_flutter_app/components/title_text.dart';
import 'package:image_editing_flutter_app/models/sticker.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/models/sticker_category.dart';
import 'package:image_editing_flutter_app/models/sticker_list_model.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/widgets.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/templates/Funural/Template0/FunuralTemplate0.dart';

// ignore: must_be_immutable
class StickerPage extends StatefulWidget {
  var state;
  // Template2State template2state;
  // Template0State template0state;
  // Template3State template3state;
  // Template4State template4state;
  // Template5State template5state;
  // Template6State template6state;
  // BodyState bodyState;

  StickerPage({
    //   this.template2state,
    // this.template5state,
    // this.template0state,
    // this.bodyState,
    // this.template6state,
    // this.template3state,
    // this.template4state
    this.state,
  });

  @override
  _StickerPageState createState() => _StickerPageState();
}

class _StickerPageState extends State<StickerPage>
    with TickerProviderStateMixin {
  // double startPos = -1.0;
  // double endPos = 0.0;
  // double startPos1 = -1.0;
  // double endPos1 = 0.0;

  var x;
  EditZoomController ec = Get.put(EditZoomController());
  @override
  void initState() {
    // if (widget.bodyState != null) {
    //   state = widget.bodyState;
    // } else if (widget.template0state != null) {
    //   state = widget.template0state;
    // } else if (widget.template2state != null) {
    //   state = widget.template2state;
    // } else if (widget.template3state != null) {
    //   state = widget.template3state;
    // } else if (widget.template4state != null) {
    //   state = widget.template4state;
    // } else if (widget.template5state != null) {
    //   state = widget.template5state;
    // } else if (widget.template6state != null){
    //   state = widget.template6state;
    // }
    // x = widget.state.response["Templates"]["sticker"];
    // for (int i = 0; i < 10; i++) {
    //   _stickers[i].stickerImage = x[i]["stickerImage"];
    // }
    //  print(widget.response);
    super.initState();
  }

  var _stickers = [
    Stickers(
      stickerName: "smily",
      stickerCategory: "normal",

      stickerImage: "https://dbdzm869oupei.cloudfront.net/img/sticker/preview/20084.png"
    ),
    Stickers(
      stickerName: "laughing",
      stickerCategory: "normal",
      stickerImage: "https://rlv.zcache.com/crying_laugh_emoji_sticker-reef23b8064174b0a8165a992e1c628fd_0ugmp_8byvr_704.jpg",
    ),
    Stickers(
      stickerName: "subway",
      stickerCategory: "normal",
       stickerImage: "https://ih1.redbubble.net/image.1070709536.5138/st,small,507x507-pad,600x600,f8f8f8.jpg",
    ),
    Stickers(
      stickerName: "random",
      stickerCategory: "normal",
      stickerImage: "https://cdn.shopify.com/s/files/1/2657/4310/products/092-sticker-pack-of-15-toronto-collective_78850285-4337-4268-a2a7-ebd833080f00_1200x.jpg?v=1576129047",
    ),
    Stickers(
      stickerName: "smily",
      stickerCategory: "normal",
        stickerImage: "https://img.pixers.pics/pho_wat(s3:700/FO/19/34/44/08/700_FO19344408_c8b2b8d8903cec57731f4f6087077e99.jpg,700,700,cms:2018/10/5bd1b6b8d04b8_220x50-watermark.png,over,480,650,jpg)/stickers-happy-winking-smiley.jpg.jpg",
    ),
    Stickers(
      stickerName: "smily",
      stickerCategory: "normal",
      stickerImage: "https://dbdzm869oupei.cloudfront.net/img/sticker/preview/20084.png"
    ),
    Stickers(
      stickerName: "laughing",
      stickerCategory: "normal",
      stickerImage: "https://rlv.zcache.com/crying_laugh_emoji_sticker-reef23b8064174b0a8165a992e1c628fd_0ugmp_8byvr_704.jpg",
    ),
    Stickers(
      stickerName: "subway",
      stickerCategory: "normal",
      stickerImage: "https://ih1.redbubble.net/image.1070709536.5138/st,small,507x507-pad,600x600,f8f8f8.jpg",
    ),
    Stickers(
      stickerName: "random",
      stickerCategory: "normal",
        stickerImage: "https://cdn.shopify.com/s/files/1/2657/4310/products/092-sticker-pack-of-15-toronto-collective_78850285-4337-4268-a2a7-ebd833080f00_1200x.jpg?v=1576129047",
    ),
    Stickers(
      stickerName: "smily",
      stickerCategory: "normal",
      stickerImage: "https://img.pixers.pics/pho_wat(s3:700/FO/19/34/44/08/700_FO19344408_c8b2b8d8903cec57731f4f6087077e99.jpg,700,700,cms:2018/10/5bd1b6b8d04b8_220x50-watermark.png,over,480,650,jpg)/stickers-happy-winking-smiley.jpg.jpg",
    ),
  ];

  List<Stickers> _searchList = [];
  bool searching = false;
  int count = 0;
  // ignore: unused_element
  _onChanged(String value) {
//    _albums.clear();
    setState(() {
//      print(value);
      if (value.length != 0) {
        count = 0;
//        widget.following.clear();
        searching = true;
        _searchList.clear();
        fetchDetails(value);
      } else if (value.length == 0) {
        searching = false;
      }
    });
  }

  // ignore: missing_return
  Future<List> fetchDetails(String value) async {
    try {
      // print(value);
      _stickers.forEach((p) {
        if (p.stickerName.toLowerCase().contains(value)) {
          _searchList.add(p);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  var _stickerCategory = [
    // StickerCategory(
    //     stickerCategoryImage: "assets/images/clock.svg",
    //     categoryIsSelected: false),
    // StickerCategory(stickerCategoryName: "love", categoryIsSelected: true),
    // StickerCategory(
    //     stickerCategoryName: "Thank you", categoryIsSelected: false),
    // StickerCategory(stickerCategoryName: "Happy", categoryIsSelected: false),
    // StickerCategory(stickerCategoryName: "Smiley", categoryIsSelected: false),
    // StickerCategory(stickerCategoryName: "Laugh", categoryIsSelected: false),
    // StickerCategory(stickerCategoryName: "Rip", categoryIsSelected: false),
    // StickerCategory(stickerCategoryName: "Baby", categoryIsSelected: false),
    // StickerCategory(stickerCategoryName: "Cute", categoryIsSelected: false),
  ];

  void _onHorizontalDrag(DragEndDetails details) {
    if (details.primaryVelocity == 0)
      return; // user have just tapped on screen (no dragging)

    if (details.primaryVelocity.compareTo(0) == -1) {
      // print("current index $_currentIndex");
      if (_currentIndex < _stickerCategory.length - 1) {
        setState(() {
          // print(_currentIndex > _stickerCategory.length);
          //   print('dragged from left');
          _currentIndex++;
          _stickerCategory
              .forEach((element) => element.categoryIsSelected = false);
          _stickerCategory[_currentIndex].categoryIsSelected = true;
        });
      }
    } else {
      // print('dragged from right');
      if (_currentIndex > 0) {
        // print('dragged from right');
        setState(() {
          // print('dragged from right');
          _currentIndex--;

          _stickerCategory
              .forEach((element) => element.categoryIsSelected = false);
          _stickerCategory[_currentIndex].categoryIsSelected = true;
        });
      }
    }
  }

  int _currentIndex = 0;

  // ignore: unused_field
  TextEditingController _searchSticker = new TextEditingController();

  bool isSearching = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 4.8,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 3.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TitleText(
                    titleName: "STICKER",
                  ),
                  // isSearching
                  //     ? TweenAnimationBuilder(
                  //         tween: Tween<Offset>(
                  //             begin: Offset(1, 0), end: Offset(0, 0)),
                  //         duration: Duration(seconds: 2),
                  //         curve: Curves.fastLinearToSlowEaseIn,
                  //         builder: (context, offset, child) {
                  //           return FractionalTranslation(
                  //             translation: offset,
                  //             child: Container(
                  //               child: Center(
                  //                 child: child,
                  //               ),
                  //             ),
                  //           );
                  //         },
                  //         child: Card(
                  //           shape: RoundedRectangleBorder(
                  //               borderRadius: BorderRadius.circular(5.0)),
                  //           child: Row(
                  //             children: [
                  //               GestureDetector(
                  //                   onTap: () {
                  //                     setState(() {
                  //                       isSearching = !isSearching;
                  //                     });
                  //                   },
                  //                   child: Padding(
                  //                     padding: EdgeInsets.only(
                  //                         left: 8.0, right: 8.0),
                  //                     child: SvgPicture.asset(
                  //                       "assets/images/search.svg",
                  //                       height: 18.0,
                  //                       width: 18.0,
                  //                     ),
                  //                   )),
                  //               SizedBox(
                  //                 height: 32,
                  //                 width: 200,
                  //                 child: TextField(
                  //                   controller: _searchSticker,
                  //                   onChanged: (value) {
                  //                     setState(() {
                  //                       _onChanged(value);
                  //                     });
                  //                   },
                  //                   decoration: InputDecoration(
                  //                       contentPadding: EdgeInsets.only(
                  //                           top: 0.0, bottom: 12.0),
                  //                       border: InputBorder.none,
                  //                       focusedBorder: InputBorder.none,
                  //                       enabledBorder: InputBorder.none,
                  //                       errorBorder: InputBorder.none,
                  //                       disabledBorder: InputBorder.none,
                  //                       hintText: "Search...",
                  //                       hintStyle: TextStyle(
                  //                           color: Color(0XFFA9A9A9))),
                  //                 ),
                  //               ),
                  //             ],
                  //           ),
                  //         ))
                  //     : Container(
                  //         child: GestureDetector(
                  //           onTap: () {
                  //             setState(() {
                  //               isSearching = !isSearching;
                  //             });
                  //             // print("tapped");
                  //           },
                  //           child: SizedBox(
                  //             height: 40.0,
                  //             width: 40.0,
                  //             child: TweenAnimationBuilder(
                  //               tween: Tween<Offset>(
                  //                   begin: Offset(-1, 0), end: Offset(0, 0)),
                  //               duration: Duration(seconds: 2),
                  //               curve: Curves.fastLinearToSlowEaseIn,
                  //               builder: (context, offset, child) {
                  //                 return FractionalTranslation(
                  //                   translation: offset,
                  //                   child: child,
                  //                 );
                  //               },
                  //               child: Card(
                  //                   child: Padding(
                  //                 padding: const EdgeInsets.all(7.0),
                  //                 child: SvgPicture.asset(
                  //                   "assets/images/search.svg",
                  //                   height: 10.0,
                  //                   width: 10.0,
                  //                 ),
                  //               )),
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                ],
              ),
            ),
          ),
          // Container(
          //   height: 20,
          //   child: ListView.builder(
          //     itemCount: _stickerCategory.length,
          //     // shrinkWrap: true,
          //     scrollDirection: Axis.horizontal,
          //     itemBuilder: (context, index) {
          //       // print(_stickerCategory[index].categoryIsSelected);
          //       return InkWell(
          //         onTap: () {
          //           setState(() {
          //             _currentIndex = index;
          //             _stickerCategory.forEach(
          //                 (element) => element.categoryIsSelected = false);
          //             _stickerCategory[index].categoryIsSelected = true;
          //           });
          //         },
          //         child: Padding(
          //           padding: EdgeInsets.only(left: 14, right: 14),
          //           child: _stickerCategory[index].stickerCategoryImage == null
          //               ? Text(
          //                   _stickerCategory[index].stickerCategoryName,
          //                   style: TextStyle(
          //                       color:
          //                           _stickerCategory[index].categoryIsSelected
          //                               ? Colors.black
          //                               : Color(0XFFA9A9A9)),
          //                 )
          //               : SvgPicture.asset(
          //                   _stickerCategory[index].stickerCategoryImage,
          //                   color: _stickerCategory[index].categoryIsSelected
          //                       ? Colors.black
          //                       : Color(0XFFA9A9A9),
          //                 ),
          //         ),
          //       );
          //     },
          //   ),
          // ),
          Expanded(
            child: GestureDetector(
              onHorizontalDragEnd: (details) {
                _onHorizontalDrag(details);
              },
              child: searching ? _listitem(_searchList) : _listitem(_stickers),
            ),
          ),
        ],
      ),
    );
  }

  Widget _listitem(List finallist) {
    // final RenderBox renderBoxRed =
    //     state.flexibleKey.currentContext.findRenderObject();
    // double x = MediaQuery.of(context).size.height - renderBoxRed.size.height;
    // double height = renderBoxRed.size.height + 6;
    // double width = renderBoxRed.size.width + 6;

    return GestureDetector(
        onHorizontalDragEnd: (details) {
          _onHorizontalDrag(details);
        },
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 6, crossAxisSpacing: 5, mainAxisSpacing: 5),
          itemCount: finallist.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                // print(_stickers[index].stickerImage);
                //  print(state.response["Templates"]["Funaral"][0]["sticker"][0]);
                widget.state.setState(() {
                  widget.state.sticker = finallist[index].stickerImage;
                  widget.state.stickerlist2.add(widget.state.sticker);
                  widget.state.stickerList.add(StickerList(
                    name: finallist[index].stickerImage,
                    xPosition: 0.50,
                    yPosition: 0.50,
                    // yPosition1: height - x,
                    // xPosition1: width - x,
                    width: 60.0,
                    height: 60.0,
                    rotation: 0.0,
                    scale: 1.0,
                  ));
                  //  ec.stickerview.value = widget.state.stickerList.length;
                });
              },
              child: Container(
                child: Center(
                  child: GetFancyShimmerImage(
                    url: finallist[index].stickerImage,
                    height: 50,
                    width: 50,
                  ),
                ),
              ),
            );
          },
        ));
  }
}
