import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:flutter_svg/svg.dart';
import 'package:image_editing_flutter_app/components/title_text.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/models/Background_category.dart';
import 'package:image_editing_flutter_app/models/Background_model.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/widgets.dart';


// ignore: must_be_immutable
class BackgroundPage extends StatefulWidget {
  // Template2State template2state;
  // Template0State template0state;
  // Template3State template3state;
  // Template4State template4state;
  // Template5State template5state;
  // Template6State template6state;
  // BodyState bodyState;
  var state;
  BackgroundPage({
    // this.template2state,
    // this.template5state,
    // this.bodyState,
    // this.template0state,
    // this.template3state,
    // this.template4state,
    // this.template6state,
    this.state
  });
  @override
  _BackgroundPageState createState() => _BackgroundPageState();
}

class _BackgroundPageState extends State<BackgroundPage> {
 

  List<BackGroundPhoto> _searchList = [];
  bool searching = false;
  int count = 0;
  // ignore: unused_element
  _onChanged(String value) {
//    _albums.clear();
    setState(() {
//      print(value);
      if (value.length != 0) {
        count = 0;
//        widget.following.clear();
        searching = true;
        _searchList.clear();
        fetchDetails(value);
      } else if (value.length == 0) {
        searching = false;
      }
    });
  }

  // ignore: missing_return
  Future<List> fetchDetails(String value) async {
    try {
      // print(value);
      _backgroundPhoto.forEach((p) {
        if (p.photoName.toLowerCase().contains(value)) {
          _searchList.add(p);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    // if (widget.bodyState != null) {
    //   state = widget.bodyState;
    // } else if (widget.template0state != null) {
    //   state = widget.template0state;
    // } else if (widget.template2state != null) {
    //   state = widget.template2state;
    // } else if (widget.template3state != null) {
    //   state = widget.template3state;
    // } else if (widget.template4state != null) {
    //   state = widget.template4state;
    // } else if (widget.template5state != null) {
    //   state = widget.template5state;
    // } else if (widget.template6state != null){
    //   state = widget.template6state;
    // }
    // for (int i = 0; i < 15; i++) {
    //   _backgroundPhoto[i].backgroundImage =
    //       widget.state.response["Templates"]["Background"][i]["BackgroundImage"];
    // }
    super.initState();
  }

  var _backgroundCategory = [
    // BackgroundCategory(
    //     backgroundCategoryImage: "assets/images/clock.svg",
    //     categoryIsSelected: true),
    // BackgroundCategory(
    //     backgroundCategoryName: "cat1", categoryIsSelected: true),
    // BackgroundCategory(
    //     backgroundCategoryName: "cat2", categoryIsSelected: false),
    // BackgroundCategory(
    //     backgroundCategoryName: "cat3", categoryIsSelected: false),
    // BackgroundCategory(
    //     backgroundCategoryName: "cat4", categoryIsSelected: false),
    // BackgroundCategory(
    //     backgroundCategoryName: "cat5", categoryIsSelected: false),
    // BackgroundCategory(
    //     backgroundCategoryName: "cat6", categoryIsSelected: false),
    // BackgroundCategory(
    //     backgroundCategoryName: "cat7", categoryIsSelected: false),
    // BackgroundCategory(
    //     backgroundCategoryName: "cat8", categoryIsSelected: false),
    // BackgroundCategory(
    //     backgroundCategoryName: "cat9", categoryIsSelected: false),
  ];

  int _currentIndex = 0;
  void _onHorizontalDrag(DragEndDetails details) {
    if (details.primaryVelocity == 0)
      return; // user have just tapped on screen (no dragging)

    if (details.primaryVelocity.compareTo(0) == -1) {
      // print("current index $_currentIndex");
      if (_currentIndex < _backgroundCategory.length - 1) {
        setState(() {
          // print(_currentIndex > _stickerCategory.length);
          //   print('dragged from left');
          _currentIndex++;
          _backgroundCategory
              .forEach((element) => element.categoryIsSelected = false);
          _backgroundCategory[_currentIndex].categoryIsSelected = true;
        });
      }
    } else {
      // print('dragged from right');
      if (_currentIndex > 0) {
        // print('dragged from right');
        setState(() {
          // print('dragged from right');
          _currentIndex--;

          _backgroundCategory
              .forEach((element) => element.categoryIsSelected = false);
          _backgroundCategory[_currentIndex].categoryIsSelected = true;
        });
      }
    }
  }

  var _backgroundPhoto = [
    BackGroundPhoto(
      photoName: "1",
      backgroundImage: "https://i.pinimg.com/originals/af/8d/63/af8d63a477078732b79ff9d9fc60873f.jpg",
      imageCatagory: "Blank",
    ),
    BackGroundPhoto(
      photoName: "2",
      imageCatagory: "Blank",
      backgroundImage: "https://i.pinimg.com/originals/2e/c6/b5/2ec6b5e14fe0cba0cb0aa5d2caeeccc6.jpg",
    ),
    BackGroundPhoto(
      photoName: "3",
      imageCatagory: "Blank",
      backgroundImage: "https://img.buzzfeed.com/buzzfeed-static/static/2018-04/13/12/asset/buzzfeed-prod-web-03/sub-buzz-8755-1523636113-1.jpg?output-quality=auto&output-format=auto&downsize=640:*",
    ),
    BackGroundPhoto(
      photoName: "4",
      imageCatagory: "Blank",
      backgroundImage: "https://wallpaperaccess.com/full/1076238.jpg",
    ),
    BackGroundPhoto(
      photoName: "5",
      imageCatagory: "Blank",
      backgroundImage: "https://images2.imgbox.com/8d/b2/DnMiCIiu_o.png",
    ),
    BackGroundPhoto(
      photoName: "6",
      imageCatagory: "Blank",
      backgroundImage: "https://archziner.com/wp-content/uploads/2019/03/spring-flowers-wallpaper-pink-blooming-flowers-phone-wallpaper-blue-background.jpg",
    ),
    BackGroundPhoto(
      photoName: "7",
      imageCatagory: "Blank",
      backgroundImage: "https://wallpapersflix.com/cool/wp-content/uploads/2020/09/Cool-Phone-Background-HD.jpg",
    ),
    BackGroundPhoto(
      photoName: "8",
      imageCatagory: "Blank",
      backgroundImage: "https://cdn.hipwallpaper.com/m/6/38/TvP8gC.jpg",
    ),
    BackGroundPhoto(
      photoName: "9",
      imageCatagory: "Blank",
      backgroundImage: "https://cdn.hipwallpaper.com/m/88/3/aj6hsL.jpg",
    ),
    BackGroundPhoto(
      photoName: "10",
      imageCatagory: "Blank",
      backgroundImage: "https://cdn.hipwallpaper.com/m/57/21/isNc7k.jpg",
    ),
    BackGroundPhoto(
      photoName: "11",
      imageCatagory: "Blank",
      backgroundImage: "https://cdn.hipwallpaper.com/m/32/66/bd3RPU.jpg",
    ),
    BackGroundPhoto(
      photoName: "12",
      imageCatagory: "Blank",
      backgroundImage: "https://previews.123rf.com/images/noravector/noravector1707/noravector170700056/81641024-cream-colored-blank-concrete-wall-for-texture-background-.jpg",
    ),
    BackGroundPhoto(
      photoName: "13",
      imageCatagory: "Blank",
      backgroundImage: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRieQh8YmpN_nOoF794RZ8TAO5ZOKZVTbzJpg&usqp=CAU",
    ),
    BackGroundPhoto(
      photoName: "14",
      imageCatagory: "Blank",
      backgroundImage: "https://i.pinimg.com/originals/91/8e/df/918edf562fd4e30b3b0a566bf402651f.jpg",
    ),
    BackGroundPhoto(
      photoName: "15",
      imageCatagory: "Blank",
      backgroundImage: "https://i.pinimg.com/originals/45/ee/fe/45eefecc477df47593b361a04b2d4cfa.jpg",
    ),
  ];

  // ignore: unused_field
  TextEditingController _searchSticker = new TextEditingController();

  bool isSearching = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 4.8,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 3.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TitleText(
                    titleName: "BACKGROUND",
                  ),
                  // isSearching
                  //     ? TweenAnimationBuilder(
                  //         tween: Tween<Offset>(
                  //             begin: Offset(1, 0), end: Offset(0, 0)),
                  //         duration: Duration(seconds: 2),
                  //         curve: Curves.fastLinearToSlowEaseIn,
                  //         builder: (context, offset, child) {
                  //           return FractionalTranslation(
                  //             translation: offset,
                  //             child: Container(
                  //               child: Center(
                  //                 child: child,
                  //               ),
                  //             ),
                  //           );
                  //         },
                  //         child: Card(
                  //           shape: RoundedRectangleBorder(
                  //               borderRadius: BorderRadius.circular(5.0)),
                  //           child: Row(
                  //             children: [
                  //               GestureDetector(
                  //                   onTap: () {
                  //                     setState(() {
                  //                       isSearching = !isSearching;
                  //                     });
                  //                   },
                  //                   child: Padding(
                  //                     padding: EdgeInsets.only(
                  //                         left: 8.0, right: 8.0),
                  //                     child: SvgPicture.asset(
                  //                       "assets/images/search.svg",
                  //                       height: 18.0,
                  //                       width: 18.0,
                  //                     ),
                  //                   )),
                  //               SizedBox(
                  //                 height: 32,
                  //                 width: 200,
                  //                 child: TextField(
                  //                   controller: _searchSticker,
                  //                   onChanged: (value) {
                  //                     setState(() {
                  //                       _onChanged(value);
                  //                     });
                  //                   },
                  //                   decoration: InputDecoration(
                  //                       contentPadding: EdgeInsets.only(
                  //                           top: 0.0, bottom: 12.0),
                  //                       border: InputBorder.none,
                  //                       focusedBorder: InputBorder.none,
                  //                       enabledBorder: InputBorder.none,
                  //                       errorBorder: InputBorder.none,
                  //                       disabledBorder: InputBorder.none,
                  //                       hintText: "Search...",
                  //                       hintStyle: TextStyle(
                  //                           color: Color(0XFFA9A9A9))),
                  //                 ),
                  //               ),
                  //             ],
                  //           ),
                  //         ))
                  //     : Container(
                  //         child: GestureDetector(
                  //           onTap: () {
                  //             setState(() {
                  //               isSearching = !isSearching;
                  //             });
                  //             // print("tapped");
                  //           },
                  //           child: SizedBox(
                  //             height: 40.0,
                  //             width: 40.0,
                  //             child: TweenAnimationBuilder(
                  //               tween: Tween<Offset>(
                  //                   begin: Offset(-1, 0), end: Offset(0, 0)),
                  //               duration: Duration(seconds: 2),
                  //               curve: Curves.fastLinearToSlowEaseIn,
                  //               builder: (context, offset, child) {
                  //                 return FractionalTranslation(
                  //                   translation: offset,
                  //                   child: child,
                  //                 );
                  //               },
                  //               child: Card(
                  //                   child: Padding(
                  //                 padding: const EdgeInsets.all(7.0),
                  //                 child: SvgPicture.asset(
                  //                   "assets/images/search.svg",
                  //                   height: 10.0,
                  //                   width: 10.0,
                  //                 ),
                  //               )),
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                ],
              ),
            ),
          ),
          // Container(
          //   height: 20,
          //   child: ListView.builder(
          //     itemCount: _backgroundCategory.length,
          //     // shrinkWrap: true,
          //     scrollDirection: Axis.horizontal,
          //     itemBuilder: (context, index) {
          //       // print(_stickerCategory[index].categoryIsSelected);
          //       return InkWell(
          //         onTap: () {
          //           setState(() {
          //             _currentIndex = index;
          //             _backgroundCategory.forEach(
          //                 (element) => element.categoryIsSelected = false);
          //             _backgroundCategory[index].categoryIsSelected = true;
          //           });
          //         },
          //         child: Padding(
          //           padding: EdgeInsets.only(left: 14, right: 14),
          //           child: _backgroundCategory[index].backgroundCategoryImage ==
          //                   null
          //               ? Text(
          //                   _backgroundCategory[index].backgroundCategoryName,
          //                   style: TextStyle(
          //                       color: _backgroundCategory[index]
          //                               .categoryIsSelected
          //                           ? Colors.black
          //                           : Color(0XFFA9A9A9)),
          //                 )
          //               : SvgPicture.asset(
          //                   _backgroundCategory[index].backgroundCategoryImage,
          //                   color: _backgroundCategory[index].categoryIsSelected
          //                       ? Colors.black
          //                       : Color(0XFFA9A9A9),
          //                 ),
          //         ),
          //       );
          //     },
          //   ),
          // ),
          Expanded(
            child: GestureDetector(
              onHorizontalDragEnd: (details) {
                _onHorizontalDrag(details);
              },
              child: searching
                  ? _listitem(_searchList)
                  : _listitem(_backgroundPhoto),
            ),
          ),
        ],
      ),
    );
  }

  Widget _listitem(List finallist) {
    // final RenderBox renderBoxRed =
    //     widget.template2state.flexibleKey.currentContext.findRenderObject();
    // double x = MediaQuery.of(context).size.height - renderBoxRed.size.height;
    // double height = renderBoxRed.size.height + 6;
    // double width = renderBoxRed.size.width + 6;

    return GestureDetector(
        onHorizontalDragEnd: (details) {
          _onHorizontalDrag(details);
        },
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 6, crossAxisSpacing: 5, mainAxisSpacing: 5),
          itemCount: finallist.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                widget.state.setState(() {
                  widget.state.background_image = finallist[index].backgroundImage;
                });
              },
              child: Container(
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(width: 0.1,color: Colors.black)
                    ),
                                      child: GetFancyShimmerImage(
                      url: finallist[index].backgroundImage,
                      // boxFit: BoxFit.contain
                      height: 50,
                      width: 30,
                      boxFit: BoxFit.cover
                    ),
                  ),
                ),
              ),
            );
          },
        ));
  }
}
