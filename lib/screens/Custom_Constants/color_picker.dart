import 'package:flutter/material.dart';
import 'package:image_editing_flutter_app/components/title_text.dart';
import 'package:image_editing_flutter_app/models/radio_model.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/text_opacity_change.dart';

import 'package:image_editing_flutter_app/screens/Custom_Constants/grid_image.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/colorlist.dart';


// import 'package:image_editing_flutter_app/screens/templates/text_opacity_change.dart';

// ignore: must_be_immutable
class TextColorPicker extends StatefulWidget {
  final double width;
  var state;
  // Template0State template0state;
  // Template2State template2state;
  // Template3State template3state;
  // Template4State template4state;
  // Template5State template5state;
  // Template6State template6state;
  // BodyState bodyState;
  final int selectedTextIndex;
  TextColorPicker(
      {Key key,
      this.width,
      this.state,
      // this.template0state,
      // this.template2state,
      // this.bodyState,
      // this.template3state,
      // this.template6state,
      // this.template4state,
      // this.template5state,
      this.selectedTextIndex})
      : super(key: key);

  @override
  _ColorPickerState createState() => _ColorPickerState();
}

class _ColorPickerState extends State<TextColorPicker> {
  
  double _colorSliderPosition = 0;
  Color _currentColor = Color.fromARGB(255, 0, 0, 0);
  List<RadioModel1> sampleData1 = <RadioModel1>[];

  final List<Color> _colors = [
    Color.fromARGB(255, 0, 0, 0),
    Color.fromARGB(255, 255, 255, 255),
    Color.fromARGB(255, 255, 0, 0),
    Color.fromARGB(255, 255, 128, 0),
    Color.fromARGB(255, 255, 255, 0),
    Color.fromARGB(255, 128, 255, 0),
    Color.fromARGB(255, 0, 255, 0),
    Color.fromARGB(255, 0, 255, 128),
    Color.fromARGB(255, 0, 255, 255),
    Color.fromARGB(255, 0, 128, 255),
    Color.fromARGB(255, 0, 0, 255),
    Color.fromARGB(255, 127, 0, 255),
    Color.fromARGB(255, 255, 0, 255),
    Color.fromARGB(255, 255, 0, 127),
    Color.fromARGB(255, 128, 128, 128),
  ];

  @override
  initState() {
    sampleData1.add(new RadioModel1(true, Color.fromARGB(255, 0, 0, 0)));
    sampleData1.add(new RadioModel1(false,Color.fromARGB(255, 255, 255, 255)));
    sampleData1.add(new RadioModel1(false, Color.fromARGB(255, 255, 0, 0)));
    sampleData1.add(new RadioModel1(false, Color.fromARGB(255, 255, 128, 0)));
    sampleData1.add(new RadioModel1(false, Color.fromARGB(255, 255, 255, 0)));
    sampleData1.add(new RadioModel1(false, Color.fromARGB(255, 128, 255, 0)));
    sampleData1.add(new RadioModel1(false, Color.fromARGB(255, 0, 255, 0)));
    sampleData1.add(new RadioModel1(false, Color.fromARGB(255, 0, 255, 128)));
    sampleData1.add(new RadioModel1(false,  Color.fromARGB(255, 0, 255, 255)));
    sampleData1.add(new RadioModel1(false, Color.fromARGB(255, 0, 128, 255)));
    sampleData1.add(new RadioModel1(false, Color.fromARGB(255, 0, 0, 255)));
    sampleData1.add(new RadioModel1(false, Color.fromARGB(255, 127, 0, 255)));
    sampleData1.add(new RadioModel1(false, Color.fromARGB(255, 255, 0, 255)));
    sampleData1.add(new RadioModel1(false, Color.fromARGB(255, 127, 0, 127)));
    sampleData1.add(new RadioModel1(false, Color.fromARGB(255, 128, 128, 128)));
    super.initState();
    _currentColor = _calculateSelectedColor(_colorSliderPosition);

    // if (widget.bodyState != null) {
    //   state = widget.bodyState;
    // } else if (widget.template0state != null) {
    //   state = widget.template0state;
    // } else if (widget.template2state != null){
    //   state = widget.template2state;
    // }
    // else if(widget.template3state != null){
    //   state = widget.template3state;
    // }
    // else if (widget.template4state != null){
    //   state = widget.template4state;
    // }
    // else if (widget.template5state != null){
    //   state = widget.template5state;
    // }
    // else if (widget.template6state != null){
    //   state = widget.template6state;
    // }
  }

  _colorChangeHandler(double position) {
    //handle out of bounds positions
    if (position > widget.width) {
      position = widget.width;
    }
    if (position < 0) {
      position = 0;
    }
    // print("New pos: $position");
    setState(() {
      _colorSliderPosition = position;
      _currentColor = _calculateSelectedColor(_colorSliderPosition);
    widget.state.setState(() {
        widget.state.textColor = _currentColor;
       widget.state.textList[widget.selectedTextIndex].textColor =
            _currentColor;
          
      });

      // print(_currentColor);
      // holder.setValue(position);
      // imageEditorState.setState(() {
      //   imageEditorState.textColor = _currentColor;
      // });
    });
  }

  Color _calculateSelectedColor(double position) {
    //determine color
    double positionInColorArray =
        (position / widget.width * (_colors.length - 1));
    // print(widget.width);
    // print(positionInColorArray);
    int index = positionInColorArray.truncate();
    // print(index);
    double remainder = positionInColorArray - index;
    if (remainder == 0.0) {
      _currentColor = _colors[index];
    } else {
      //calculate new color
      int redValue = _colors[index].red == _colors[index + 1].red
          ? _colors[index].red
          : (_colors[index].red +
                  (_colors[index + 1].red - _colors[index].red) * remainder)
              .round();
      int greenValue = _colors[index].green == _colors[index + 1].green
          ? _colors[index].green
          : (_colors[index].green +
                  (_colors[index + 1].green - _colors[index].green) * remainder)
              .round();
      int blueValue = _colors[index].blue == _colors[index + 1].blue
          ? _colors[index].blue
          : (_colors[index].blue +
                  (_colors[index + 1].blue - _colors[index].blue) * remainder)
              .round();
      _currentColor = Color.fromARGB(255, redValue, greenValue, blueValue);
    }
    return _currentColor;
  }

  @override
  Widget build(BuildContext context) {
    // print(_currentColor);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 14),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TitleText(
                titleName: "COLOR",
              ),
              GridImage(
                data: sampleData1,
                state: widget.state,
                selectedIndex: widget.selectedTextIndex,
              )
            ],
          ),
          Center(
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onHorizontalDragStart: (DragStartDetails details) {
                // print("_-------------------------STARTED DRAG");
                _colorChangeHandler(details.localPosition.dx);
              },
              onHorizontalDragUpdate: (DragUpdateDetails details) {
                _colorChangeHandler(details.localPosition.dx);
              },
              onTapDown: (TapDownDetails details) {
                _colorChangeHandler(details.localPosition.dx);
              },
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 9),
                child: Container(
                  width: widget.width,
                  height: 8,
                  decoration: BoxDecoration(
                    border: Border.all(width: 2, color: Colors.transparent),
                    borderRadius: BorderRadius.circular(15),
                    gradient: LinearGradient(colors: _colors),
                  ),
                  child: CustomPaint(
                    painter: _SliderIndicatorPainter(_colorSliderPosition),
                  ),
                ),
              ),
            ),
          ),
          CustomRadioColor(
           state: widget.state,
            sampleData1: sampleData1,
            selectedIndex: widget.selectedTextIndex,
          ),
          Padding(
            padding: EdgeInsets.only(top: 6),
            child: TitleText(
              titleName: "OPACITY",
            ),
          ),
          OpacitySlider(
            state: widget.state,
            selectedIndex: widget.selectedTextIndex,
          )
        ],
      ),
    );
  }
}

class _SliderIndicatorPainter extends CustomPainter {
  final double position;
  _SliderIndicatorPainter(this.position);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawCircle(Offset(position, size.height / 2), 11,
        Paint()..color = Colors.black45.withOpacity(0.2));
    canvas.drawCircle(
        Offset(position, size.height / 2), 10, Paint()..color = Colors.white);
  }

  @override
  bool shouldRepaint(_SliderIndicatorPainter old) {
    return true;
  }
}
