import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

// import 'package:dynamic_widget/dynamic_widget/basic/dynamic_widget_json_exportor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
import 'package:image_editing_flutter_app/components/textStyle.dart';
import 'package:image_editing_flutter_app/components/transitions.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/Custom_Constants/like_refresh.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/show_final_image.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/widgets.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:ui' as ui;
import '../../main.dart';

// ignore: must_be_immutable
class Appbardata extends StatefulWidget {
  var state;
  
  Appbardata({
    this.state,
  
  });
  @override
  AppbardataState createState() => AppbardataState();
}

class AppbardataState extends State<Appbardata> {
  // var state;
  String mainJsondata;
  EditZoomController ec = Get.put(EditZoomController());
  // ignore: missing_return
  Future<bool> _onBackPressed() {
    widget.state.isTextediting = false;
    if (ec.isOpenSticker.value == false &&
        ec.isOpenTextbar.value == false &&
        ec.isOpenWidget.value == false &&
        ec.isOpenBackground.value == false &&
        widget.state.isTextediting == false) {
      onBackPressed();
    }
    if (ec.isOpenSticker.value == true) {
      setState(() {
        ec.isOpenSticker.value = false;
        ec.isOpenWidget.value = false;
      });
    }
    if (ec.isOpenBackground.value == true) {
      setState(() {
        ec.isOpenWidget.value = false;
        ec.isOpenBackground.value = false;
      });
    }
    if (ec.isOpenTextbar.value == true) {
      setState(() {
        ec.isOpenTextbar.value = false;
        ec.isOpenWidget.value = false;
        widget.state.isTextediting = false;
      });
    }
    if (widget.state.isTextediting == true) {
      setState(() {
        widget.state.isTextediting = false;
      });
    }

    // if (popScreen == true) {
    //   // print("popped screen");
    //   // Navigator.pop(context);
    // }
  }

  // ignore: missing_return
  Widget onBackPressed() {
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            child: Container(
              height: 300,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    height: 100,
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.circular(30.0),
                        ),
                    ),
                     child: Center(
                      child: Obx((){
                        return ec.isdraftLoading.value ? CircularProgressIndicator(color: Colors.white,):Icon(
                        Icons.exit_to_app_rounded,
                        size: 50,
                        color: Colors.white,
                      );
                      })
                    ),
                  ),
                  Container(
                    height: 130,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Alert!",
                            style: TextStyle(
                                fontSize: 28, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Are you sure, you want to Exit?",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                (widget.state.editId == null && widget.state.draftId == null) ? !widget.state.isSaved ?
                       Container(
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                ElevatedButton(
                                  onPressed: () {
                                    ec.isdraftLoading.value = true;
                                    saveAsDraft();
                                  },
                                  child: Text("Save as draft"),
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.blue[500],
                                    shadowColor: Colors.grey,
                                    onPrimary: Colors.white,
                                  ),
                                ),
                                Row(
                                  children: [
                                    ElevatedButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: Text("No")),
                                    SizedBox(
                                      width: 7.5,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          ec.innerPhotoview.value = 1;
                                          ec.textediting.value = 0;
                                          ec.stickerview.value = 1;
                                          ec.ListofFiles();
                                          ec.ListofFilesDraft();
                                          ec.isEditTemplate.value = false;
                                          //Function called
                                          Navigator.of(context).pop();
                                          Navigator.pop(context);
                                        },
                                        child: Text("Yes"))
                                  ],
                                )
                              ],
                            ),
                          ),
                        )
                      : Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("No")),
                              ElevatedButton(
                                  onPressed: () {
                                    ec.innerPhotoview.value = 1;
                                    ec.textediting.value = 0;
                                    ec.stickerview.value = 1;
                                    ec.ListofFiles();
                                    ec.isEditTemplate.value = false;
                                    //Function called
                                    Navigator.of(context).pop();
                                    Navigator.pop(context);
                                  },
                                  child: Text("Yes"))
                            ],
                          ),
                        ) :
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("No")),
                              ElevatedButton(
                                  onPressed: () {
                                    ec.innerPhotoview.value = 1;
                                    ec.textediting.value = 0;
                                    ec.stickerview.value = 1;
                                    ec.ListofFiles();
                                    ec.isEditTemplate.value = false;
                                    //Function called
                                    Navigator.of(context).pop();
                                    Navigator.pop(context);
                                  },
                                  child: Text("Yes"))
                            ],
                          ),
                        ) 
                ],
              ),
            ),
          );
        });
  }

 Future saveAsDraft() async{
   bool value = true;
   widget.state.containerList.forEach((element) {
     if(element.containerimage == null){
       value = false;
     }
   });
    // ec.isdraft.value = true;
    if(value){
    widget.state.appbardataState.export(this);
    await widget.state.appbardataState.takeScreenshot(this,true);
    print(widget.state.fileImage);
    print(widget.state.finalimage);
    savefileDraft(widget.state.fileImage, widget.state.finalimage,this).then((value){
      Navigator.of(context).pop();
    Navigator.pop(context);
    ec.ListofFilesDraft();
    });    
    }
    else{
      ec.isdraftLoading.value = false;
      Navigator.pop(context);
      Fluttertoast.showToast(msg: "Add image to save in draft");
    }
  }
  @override
  void initState() {
    super.initState();
  }

  File imageFile;
  Random rng = new Random();
  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: IconButton(
        icon: SvgPicture.asset("assets/images/backarrow.svg"),
        onPressed: () {
          // setState(() {
            // ec.isOpenTextbar.value = false;
            _onBackPressed();
          // });
        },
      ),
      title: GestureDetector(
          onTap: () {
            Navigator.pop(context);
            Navigator.pushAndRemoveUntil(
                context, FadeRoute(page: MyHomePage()), (route) => false);
          },
          child: Text(
            "Dismiss Project",
            style: appTextStyleWithoutBold(fontSize: 11, textColor: Colors.red),
          )),
      centerTitle: true,
      actions: [
        // Container(
        //   margin: EdgeInsets.only(top: 8.0),
        //   padding: EdgeInsets.zero,
        //   height: 40,
        //   width: 40,
        //   child: IconButton(
        //       icon: Icon(
        //         Icons.swap_vert,
        //         color: Colors.black,
        //       ),
        //       onPressed:(){
        //         export(widget.state);
        //       }
        // ),),
        // LikeAndRefresh(),
        Container(
          margin: EdgeInsets.only(top: 8.0, right: 5.0),
          padding: EdgeInsets.zero,
          height: 40,
          width: 40,
          child: IconButton(
            icon: Obx((){
              return ec.isfinalimageProcess.value ? CupertinoActivityIndicator(animating: true,radius: 10.0,) : SvgPicture.asset(
              "assets/images/eye.svg",
              height: 20,
              width: 20,
            );
            }),
            onPressed: () async {
              
              export(widget.state);
              var val = false;
              widget.state.containerList.forEach((element) {
                if (element.containerimage == null) {
                  val = false;
                } else {
                  val = true;
                }
              });
              if (widget.state.myglobalKey.currentWidget == null || !val) {
                widget.state.myglobalkey1.currentState
                    .showSnackBar(snackBarForImage);
              } else {
                ec.isfinalimageProcess.value = true;
                setState(() {
                  // widget.state.isImageEditing = false;
                  // widget.state.isOpenSticker = false;
                  ec.isOpenSticker.value = false;
                  widget.state.isImageEditing = false;
                });
                await takeScreenshot(widget.state,false);
                await new Future.delayed(const Duration(seconds: 1)).then((value){
                   Navigator.push(
                    context,
                    FadeRoute(
                        page: ShowImage(
                      image: imageFile,
                      image1: newimage,
                      state: widget.state,
                    )));
                });
                
                
              }
              // print(ImageEditorState().imageFile);
            },
          ),
        ),
      ],
    );
  }

  final snackBarForImage = SnackBar(
    content: Text('Please select image!!'),
  );
  ui.Image newimage;
  Future takeScreenshot(state,draft) async {

    // Directory appDocDirectory = await getApplicationDocumentsDirectory();
    // print(_imageEditorwidget.state.globalKey.currentContext.findRenderObject());

    RenderRepaintBoundary boundary =
        state.myglobalKey.currentContext.findRenderObject();
    ui.Image image = await boundary.toImage(pixelRatio: 4.0);
    
    final directory = (await getApplicationDocumentsDirectory()).path;
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();
    File imgFile = new File('$directory/screenshot${rng.nextInt(100000)}.png');
    state.setState(() {
      newimage = image;
      imageFile = imgFile;
      // widget.state.imagePath = imgFile;
    });
    if(draft){
      state.setState(() {
        state.fileImage = imageFile;
        state.finalimage = image;
      });
    }
    imgFile.writeAsBytes(pngBytes);
  }

  export(state) {

    // var getState;
    List datacontainer = [];
    List datasticker = [];
    String databackground;
    List datatextlist = [];
    for (int i = 0; i < state.containerList.length; i++) {
      datacontainer.add(state.containerList[i]);
    }
    for (int i = 0; i < state.stickerList.length; i++) {
      datasticker.add(state.stickerList[i]);
    }
    databackground = state.background_image.toString();
    for (int i = 0; i < state.textList.length; i++) {
      datatextlist.add(state.textList[i]);
    }

    String maincontainer = '{"aspectRatio" : "${state.dataAspectRatio}","height" : "${state.dataheight.toStringAsFixed(3)}","width" : "${state.datawidth.toStringAsFixed(3)}","marginVerticle" : "60","marginHorizontal" : "60"}';

    String backgroundJson = '{"Background" : "$databackground"}';
    // String mainContainerJson = '{ "height" : "${datamaincontainer.height}",  "width" : "${datamaincontainer.width}",  ""                }';
    String datacontainerJson = "";
    // '{"width" : "${datacontainer[0].width}", "height" : "${datacontainer[0].height}", "initial-left" : "${datacontainer[0].left}",  "initial-top" : "${datacontainer[0].top}",  "xposition" : "${datacontainer[0].xposition}",  "yposition" : "${datacontainer[0].yposition}", "containerImage" : "${datacontainer[0].containerimage}", "scale" : "${datacontainer[0].scale}", "rotation" : "${datacontainer[0].rotation}"}';
    for (int i = 0; i < datacontainer.length; i++) {
      datacontainerJson = datacontainerJson +
          '{"width" : "${datacontainer[i].width.toStringAsFixed(3)}", "height" : "${datacontainer[i].height.toStringAsFixed(3)}",   "xposition" : "${datacontainer[i].xposition.toStringAsFixed(3)}",  "yposition" : "${datacontainer[i].yposition.toStringAsFixed(3)}", "containerImage" : "${datacontainer[i].containerimage}", "scale" : "${datacontainer[i].scale}", "rotation" : "${datacontainer[i].rotation}"}';
      if (i != datacontainer.length - 1) {
        datacontainerJson = datacontainerJson + ',';
      }
    }
    String datastickerJson = "";

    for (int i = 0; i < datasticker.length; i++) {
      datastickerJson = datastickerJson +
          '{"name" : "${datasticker[i].name}", "width" : "${datasticker[i].width}", "height" : "${datasticker[i].height}",  "xposition" : "${datasticker[i].xPosition.toStringAsFixed(3)}",  "yposition" : "${datasticker[i].yPosition.toStringAsFixed(3)}", "scale" : "${datasticker[i].scale}", "rotation" : "${datasticker[i].rotation}"}';
      if (i != datasticker.length - 1) {
        datastickerJson = datastickerJson + ',';
      }
    }
    String datatextlistJson = "";

    for (int i = 0; i < datatextlist.length; i++) {
      datatextlistJson = datatextlistJson +
          '{"text" : "${datatextlist[i].text}", "xPosition" : "${datatextlist[i].xPosition.toStringAsFixed(3)}",  "yposition" : "${datatextlist[i].yPosition.toStringAsFixed(3)}", "height" : "${datatextlist[i].height}","width" : "${datatextlist[i].width}","textStyleIndex" : "${datatextlist[i].textStyleIndex}","finalTextAlignIndex" : "${datatextlist[i].finalTextAlignIndex}","textColor" : "${datatextlist[i].textColor}","strockColor" : "${datatextlist[i].strockColor}","strockValue" : "${datatextlist[i].strockValue}","textOpacity" : "${datatextlist[i].textOpacity}","rotationAngle" : "${datatextlist[i].rotationAngle}","scaleFactor" : "${datatextlist[i].scaleFactor}"}';
      if (i != datatextlist.length - 1) {
        datatextlistJson = datatextlistJson + ',';
      }
    }
     mainJsondata =
        '{ "mainContainer": $maincontainer, "Sticker": [$datastickerJson] , "BackGround": $backgroundJson, "containers" : [$datacontainerJson], "Textlist": [$datatextlistJson] }';
    // print();
    state.setState(() {
      state.jsonCode1 = mainJsondata;
    }); 
    print(mainJsondata);
    
  
  }
}

// // ignore: must_be_immutable
// class Extra extends StatefulWidget {
//   String json;
//   Extra({this.json});
//   @override
//   _ExtraState createState() => _ExtraState();
// }

// class _ExtraState extends State<Extra> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Json file "),
//       ),
//       body: SingleChildScrollView(
//         child: Center(
//           child: Column(
//             children: [
//               Container(
//                 child: SelectableText(widget.json),
//               ),
//               SizedBox(
//                 height: 200,
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
