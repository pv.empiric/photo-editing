// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:gesture_x_detector/gesture_x_detector.dart';
// import 'package:image_editing_flutter_app/components/stickerScalingDot.dart';
// import 'package:image_editing_flutter_app/models/text_align.dart';
// import 'package:image_editing_flutter_app/screens/Custom_Constants/color_picker.dart';

// import 'package:image_editing_flutter_app/screens/Custom_Constants/EditText.dart';


// // ignore: must_be_immutable
// class TextList extends StatefulWidget {
//   List textList;
//   var state;
//   // Template0State template0state;
//   // Template2State template2state;
//   // Template3State template3state;
//   // Template4State template4state;
//   // Template5State template5state;
//   // Template6State template6state;
//   // BodyState bodyState;
//   bool isImageEditing;
//   bool isTextEditing;
//   List fontStyleList;
//   int finalTextStyleIndex;
//   TextList(
//       {
//         this.state,
//         this.textList,
//       // this.template0state,
//       // this.template3state,
//       // this.template2state,
//       // this.template4state,
//       // this.bodyState,
//       // this.template5state,
//       // this.template6state,
//       this.isImageEditing,
//       this.isTextEditing,
//       this.fontStyleList,
//       this.finalTextStyleIndex});

//   @override
//   _TextListState createState() => _TextListState();
// }

// class _TextListState extends State<TextList> {
//   TextEditingController _editingController;
//   String tamplateText;
//   double finalAngle = 0.0;
//   double oldAngle = 0.0;
//   double upsetAngle = 0.0;
//   double _imageHeightBaseScaleFactor = 1.0;
//   double _imageWidthBaseScaleFactor = 1.0;
//   double previousAngle;
//   double previousHeight = 0.0;
//   double previousWidth = 0.0;
//   double stackScale;
//   double width = 100;
//   bool isediting;
  
//   double temprotation;
//   double tempXposition;
//   double tempYposition;
//   double tempHeightscale;
//   double tempWidthscale;
//   double tempScale;

//   @override
//   void initState() {
//     _editingController = TextEditingController(text: tamplateText);
//     // if (widget.bodyState != null) {
//     //   state = widget.bodyState;
//     // } else if (widget.template0state != null) {
//     //   state = widget.template0state;
//     // } else if (widget.template2state != null) {
//     //   state = widget.template2state;
//     // } else if (widget.template3state != null) {
//     //   state = widget.template3state;
//     // } else if (widget.template4state != null) {
//     //   state = widget.template4state;
//     // } else if (widget.template5state != null) {
//     //   state = widget.template5state;
//     // } else if (widget.template6state != null) {
//     //   state = widget.template6state;
//     // }
//     super.initState();
//   }

//   List textAlign = [
//     FinalTextAlign(
//         textAlign: TextAlign.start,
//         textAlignImage: "assets/textalignimages/alignLeft.svg",
//         textDirection: TextDirection.ltr),
//     FinalTextAlign(
//         textAlign: TextAlign.center,
//         textAlignImage: "assets/textalignimages/alignCenter.svg"),
//     FinalTextAlign(
//         // textAlign: TextAlign.end,
//         textAlignImage: "assets/textalignimages/alignRight.svg",
//         textDirection: TextDirection.rtl),
//     FinalTextAlign(
//         textAlign: TextAlign.justify,
//         textAlignImage: "assets/textalignimages/alignLeftWithJustify.svg",
//         textDirection: TextDirection.ltr),
//     FinalTextAlign(
//       textAlign: TextAlign.justify,
//       textAlignImage: "assets/textalignimages/alignCenterWithJustify.svg",
//       // textDirection: TextDirection
//     ),
//     FinalTextAlign(
//         textAlign: TextAlign.justify,
//         textAlignImage: "assets/textalignimages/alignRightWithJustify.svg",
//         textDirection: TextDirection.rtl),
//     FinalTextAlign(
//         textAlign: TextAlign.justify,
//         textAlignImage: "assets/textalignimages/allJustify.svg")
//   ];

//   int editingIndex = null; // ignore: avoid_init_to_null
//   double xPosition = 100;
//   double yPosition = 50;
//   double _baseScaleFactor = 1.0;
//   num finalHeight;
//   num finalWidth;

//   var yOffset = 400.0;
//   var xOffset = 50.0;

//   @override
//   Widget build(BuildContext context) {
//     if (editingIndex != null) {
//       decoratedText();
//     }
//     // print(widget.size);

//     return Stack(
//       children: widget.textList.map((textValue) {
//         // ignore: unused_local_variable
//         //     final RenderBox renderBox = widget.state.flexibleKey.currentContext.findRenderObject();
//         // print(renderBox.size);

//         return Positioned(
//             left: textValue.xPosition,
//             top: textValue.yPosition,
//             // bottom: textValue.xPosition1,
//             // right: textValue.yPosition1,
//             child: Transform.rotate(
//               angle: textValue.rotationAngle,
//               child: Container(
//                 child: LayoutBuilder(
//                   builder: (context, constraints) {
//                     Offset centerOfGestureDetector = Offset(
//                         textValue.finalHeight / 2, textValue.finalHeight / 2);
//                     return Container(
//                       child: Stack(
//                         children: [
//                           XGestureDetector(
                           
//                             onMoveStart: (value) {
//                               widget.state.setState(() {
//                                 widget.state.isTextediting = true;
//                               });
//                             },
//                             onMoveUpdate: (value) {
//                               setState(() {
//                                 textValue.xPosition += value.delta.dx;
//                                 textValue.yPosition += value.delta.dy;
//                                 tempXposition = textValue.xPosition;
//                                 tempYposition = textValue.yPosition;
//                                 var x = textValue.xPosition / widget.state.datawidth;
//                                 var y = textValue.yPosition / widget.state.dataheight;

//                                 print("new left for text field will be:-  $x");
//                                 print("new top for text field will be :- $y");
//                               });
//                             },
//                             // onMoveStart: (pointer, localPos, position) {
//                             //   widget.bodywidget.state.setState(() {
//                             //     widget.bodywidget.state.isTextEditing = true;
//                             //   });
//                             // },
//                             // onMoveUpdate: (localPos, position, localDelta, delta) {
//                             //   setState(() {
//                             //     textValue.xPosition += delta.dx;
//                             //     textValue.yPosition += delta.dy;
//                             //   });
//                             // },
//                             // onMoveEnd: (pointer, localPosition, position) {
//                             // },
//                             child: GestureDetector(
//                               behavior: HitTestBehavior.opaque,
//                               onScaleStart: (initialFocusPoint) {
//                                 setState(() {
//                                   _baseScaleFactor = textValue.scaleFactor;
//                                   _imageHeightBaseScaleFactor =
//                                       textValue.heightScale;
//                                   _imageWidthBaseScaleFactor =
//                                       textValue.widthScale;
//                                   previousAngle = textValue.rotationAngle;
                                  
//                                 });
//                               },
//                               onScaleUpdate: (details) {
//                                 // print(details);
//                                 setState(() {
//                                   textValue.scaleFactor =
//                                       _baseScaleFactor * details.scale;
//                                   tempScale = textValue.scaleFactor;
//                                   textValue.rotationAngle =
//                                       previousAngle + details.rotation;
//                                   temprotation = textValue.rotationAngle;
                                  
//                                   // print(details.scale);
//                                   textValue.heightScale =
//                                       _imageHeightBaseScaleFactor *
//                                           details.scale;
//                                   textValue.widthScale =
//                                       _imageWidthBaseScaleFactor *
//                                           details.scale;
//                                   textValue.finalHeight =
//                                       (textValue.heightScale *
//                                           textValue.height);
//                                   textValue.finalWidth =
//                                       (textValue.widthScale * textValue.width);
//                                 });
//                                 // print(finalAngle.abs());
//                               },
//                               onScaleEnd: (details) {
//                                 previousAngle = 0.0;
//                               },
//                                onDoubleTap: (){
//                                  setState(() {
//                                    widget.state.isTextediting = true;
//                                  });
//                                },
//                               onTap: () {
//                                 editingIndex =
//                                     widget.textList.indexOf(textValue);

//                                 widget.state.setState(() {
//                                   widget.state.textStyleEditingIndex =
//                                       widget.textList.indexOf(textValue);
//                                   widget.state.isTextediting = true;
//                                   widget.state.textStyleIndex = widget
//                                       .textList[editingIndex].textStyleIndex;
//                                   widget.state.isOpenTextbar = true;
//                                   widget.state.isOpenWidget = true;
//                                   widget.state.isOpenSticker = false;
//                                   // widget.state.myWidget = EditText(
//                                   //   fontStyleList: widget.fontStyleList,
//                                   //   state: state,
//                                   //   myWidgetIndex: widget.state.textStyleIndex,
//                                   // );
//                                   EditText().myWidgetIndex = widget.state.textStyleIndex;
//                                   widget.state.myWidget = TextColorPicker(
//                                     state: widget.state,
//                                     width: MediaQuery.of(context).size.width,
//                                     selectedTextIndex: widget.state.textStyleEditingIndex,
//                                   );
//                                 });
//                               },
//                               child: Container(
//                                 padding: EdgeInsets.symmetric(
//                                     horizontal: 20.0, vertical: 10.0),
//                                 margin: EdgeInsets.all(6.0),
//                                 decoration: BoxDecoration(
//                                   shape: BoxShape.rectangle,
//                                   border: Border.symmetric(
//                                       vertical: widget.isTextEditing
//                                           ? BorderSide(
//                                               width: 1.0, color: Colors.white)
//                                           : BorderSide.none,
//                                       horizontal: widget.isTextEditing
//                                           ? BorderSide(
//                                               width: 1.0, color: Colors.white)
//                                           : BorderSide.none),
//                                 ),
//                                 child: Center(
//                                   child: textValue.isEditing
//                                       ? Container(
//                                           height: 300,
//                                           width: 300,
//                                           child: TextFormField(
//                                             keyboardType:
//                                                 TextInputType.multiline,
//                                             maxLines: null,
//                                             minLines: null,
//                                             expands: true,
//                                             controller: _editingController,
//                                             decoration: InputDecoration(
//                                                 border: InputBorder.none),
//                                             onChanged: (newValue) {
//                                               setState(() {
//                                                 textValue.text = newValue;
//                                                 // textValue.isEditing = false;
//                                               });
//                                             },
//                                             autofocus: true,
//                                           ),
//                                         )
//                                       : Container(
//                                           width: textValue.finalWidth.abs(),
//                                           height: textValue.finalHeight.abs(),
//                                           margin: EdgeInsets.only(
//                                               left: 15.0,
//                                               right: 15.0,
//                                               top: 15.0,
//                                               bottom: 15.0),
//                                           // padding:EdgeInsets.only(left:15.0,right:15.0,top:50.0,bottom: 50.0),
//                                           child: Center(
//                                             child: Stack(
//                                               children: [
//                                                 Text(
//                                                   textValue.text,
//                                                   style: textValue.textStyle,
//                                                   textAlign: textAlign[textValue
//                                                           .finalTextAlignIndex]
//                                                       .textAlign,
//                                                   textDirection: textAlign[
//                                                           textValue
//                                                               .finalTextAlignIndex]
//                                                       .textDirection,
//                                                   textScaleFactor:
//                                                       textValue.scaleFactor,
//                                                   // softWrap: false,
//                                                   overflow: TextOverflow.fade,
//                                                 ),
//                                                 Text(
//                                                   textValue.text,
//                                                   style: textValue.textStyle2,
//                                                   textAlign: textAlign[textValue
//                                                           .finalTextAlignIndex]
//                                                       .textAlign,
//                                                   textDirection: textAlign[
//                                                           textValue
//                                                               .finalTextAlignIndex]
//                                                       .textDirection,
//                                                   textScaleFactor:
//                                                       textValue.scaleFactor,
//                                                   // softWrap: false,
//                                                   overflow: TextOverflow.fade,
//                                                 ),
//                                               ],
//                                             ),
//                                           ),
//                                         ),
//                                 ),
//                               ),
//                             ),
//                           ),

//                           // textValue.isEditing
//                           //     ? Positioned(
//                           //         top: 10,
//                           //         right: 30,
//                           //         child: IconButton(
//                           //           icon: Icon(Icons.check, color: Colors.green[900],size: 30.0,),
//                           //           onPressed: () {
//                           //             setState(() {
//                           //               textValue.isEditing = false;
//                           //               textValue.rotationAngle = temprotation;
//                           //               textValue.scaleFactor = tempScale;
//                           //               textValue.xPosition = tempXposition;
//                           //               textValue.yPosition = tempYposition;
//                           //               widget.state.isTextediting = true;
//                           //               if (textValue.text == null) {
//                           //                 widget.textList.remove(textValue);
//                           //               }
//                           //             });
//                           //           },
//                           //         ),
//                           //       )
//                           //     : Container(),

//                           // //upper dot of text edit container
//                           // widget.isTextEditing
//                           //     ? Positioned(
//                           //         left: 30 + textValue.finalWidth.abs() / 2,
//                           //         child: Container(
//                           //           child: GestureDetector(
//                           //             onPanStart: (details) {
//                           //               previousHeight = textValue.finalHeight;
//                           //               previousWidth = textValue.yPosition;
//                           //             },
//                           //             onPanUpdate: (details) {
//                           //               setState(() {
//                           //                 textValue.finalHeight =
//                           //                     previousHeight -
//                           //                         details.localPosition.dy;
//                           //                 textValue.height =
//                           //                     textValue.finalHeight;
//                           //                 textValue.yPosition = previousWidth +
//                           //                     details.localPosition.dy;

//                           //                 textValue.heightScale = 1.0;
//                           //                 var x = textValue.finalHeight +
//                           //                     previousHeight;
//                           //                 print("New height will be $x");
//                           //               });
//                           //             },
//                           //             onPanEnd: (details) {
//                           //               previousHeight = 0.0;
//                           //             },
//                           //             child: Container(
//                           //               // margin: EdgeInsets.all(5.0),
//                           //               child: StickerScalingDot(
//                           //                 color: Colors.white,
//                           //                 size: 12,
//                           //               ),
//                           //             ),
//                           //           ),
//                           //         ),
//                           //       )
//                           //     : SizedBox(),

//                           // //Bottom dot of text edit container
//                           // widget.isTextEditing
//                           //     ? Positioned(
//                           //         bottom: 0,
//                           //         left: 30 + textValue.finalWidth.abs() / 2,
//                           //         child: GestureDetector(
//                           //           onPanStart: (details) {
//                           //             previousHeight = textValue.finalHeight;
//                           //           },
//                           //           onPanUpdate: (details) {
//                           //             setState(() {
//                           //               // textValue.heightScale = previousHeight + details.localPosition.dy;
//                           //               textValue.finalHeight = previousHeight +
//                           //                   details.localPosition.dy;
//                           //               textValue.height =
//                           //                   textValue.finalHeight;
//                           //               textValue.heightScale = 1.0;
//                           //             });
//                           //           },
//                           //           onPanEnd: (details) {
//                           //             previousHeight = 0.0;
//                           //           },
//                           //           child: StickerScalingDot(
//                           //             color: Colors.white,
//                           //             size: 12,
//                           //           ),
//                           //         ),
//                           //       )
//                           //     : SizedBox(),

//                           // //right dot of text edit container
//                           // widget.isTextEditing
//                           //     ? Positioned(
//                           //         right: 0,
//                           //         top: 30 + textValue.finalHeight.abs() / 2,
//                           //         child: GestureDetector(
//                           //           onPanStart: (details) {
//                           //             previousHeight = textValue.finalWidth;
//                           //           },
//                           //           onPanUpdate: (details) {
//                           //             setState(() {
//                           //               // textValue.widthScale = previousHeight + details.localPosition.dx;
//                           //               textValue.finalWidth = previousHeight +
//                           //                   details.localPosition.dx;
//                           //               textValue.width = textValue.finalWidth;
//                           //               textValue.widthScale = 1.0;
//                           //               print(textValue.width);
//                           //             });
//                           //           },
//                           //           onPanEnd: (details) {
//                           //             previousHeight = 0.0;
//                           //           },
//                           //           child: StickerScalingDot(
//                           //             color: Colors.white,
//                           //             size: 12,
//                           //           ),
//                           //         ),
//                           //       )
//                           //     : SizedBox(),

//                           // //left dot of text edit container
//                           // widget.isTextEditing
//                           //     ? Positioned(
//                           //         top: 30 + textValue.finalHeight.abs() / 2,
//                           //         child: GestureDetector(
//                           //           onPanStart: (details) {
//                           //             previousHeight = textValue.finalWidth;
//                           //             previousWidth = textValue.xPosition;
//                           //           },
//                           //           onPanUpdate: (details) {
//                           //             setState(() {
//                           //               textValue.finalWidth = previousHeight -
//                           //                   details.localPosition.dx;
//                           //               textValue.width = textValue.finalWidth;
//                           //               //  print(previousWidth + details.localPosition.dx);
//                           //               textValue.xPosition = previousWidth +
//                           //                   details.localPosition.dx;
//                           //               textValue.widthScale = 1.0;
//                           //               print(textValue.width);
//                           //             });

//                           //             // print(details);
//                           //           },
//                           //           onPanEnd: (details) {
//                           //             previousHeight = 0.0;
//                           //           },
//                           //           child: StickerScalingDot(
//                           //             color: Colors.white,
//                           //             size: 12,
//                           //           ),
//                           //         ),
//                           //       )
//                           //     : SizedBox(),

//                           // //Edit Image Text
//                           // widget.isTextEditing
//                           //     ? Positioned(
//                           //         top: 0,
//                           //         left: 0,
//                           //         child: GestureDetector(
//                           //           onTap: () {
//                           //             _editingController.text = textValue.text;
//                           //             setState(() {
//                           //               textValue.isEditing = true;
//                           //               textValue.rotationAngle = 0.0;
//                           //               textValue.scaleFactor = 1.0;
//                           //               textValue.xPosition = 50.0;
//                           //               textValue.yPosition = 100.0;
//                           //               widget.state.isTextediting = false;
//                           //                   // !textValue.isEditing;
//                           //             });
//                           //           },
//                           //           onPanEnd: (details) {
//                           //             previousHeight = 0.0;
//                           //           },
//                           //           child: Container(
//                           //             padding: EdgeInsets.all(5.0),
//                           //             decoration: BoxDecoration(
//                           //               boxShadow: [
//                           //                 BoxShadow(
//                           //                     color: Colors.black,
//                           //                     blurRadius: 2,
//                           //                     offset: Offset(1, 1))
//                           //               ],
//                           //               shape: BoxShape.circle,
//                           //               color: Colors.white,
//                           //               // borderRadius: BorderRadius.circular(20.0)
//                           //             ),
//                           //             child: Icon(
//                           //               textValue.isEditing
//                           //                   ? Icons.spellcheck
//                           //                   : Icons.edit,
//                           //               size: 12.0,
//                           //             ),
//                           //           ),
//                           //         ),
//                           //       )
//                           //     : SizedBox(),

//                           // //this is for delete text from image
//                           // widget.isTextEditing
//                           //     ? Positioned(
//                           //         top: 0,
//                           //         right: 0,
//                           //         child: GestureDetector(
//                           //           onTap: () {
//                           //             setState(() {
//                           //               widget.textList.remove(textValue);
//                           //               if (widget.textList.length == 0) {
//                           //                 widget.state.setState(() {
//                           //                   widget..state.isOpenWidget = false;
//                           //                   widget.state.isOpenTextbar = false;
//                           //                   widget.state.isTextediting = false;
//                           //                 });
//                           //               }
//                           //               editingIndex = null;
//                           //             });
//                           //           },
//                           //           onPanEnd: (details) {
//                           //             previousHeight = 0.0;
//                           //           },
//                           //           child: Container(
//                           //             padding: EdgeInsets.all(5.0),
//                           //             decoration: BoxDecoration(
//                           //               boxShadow: [
//                           //                 BoxShadow(
//                           //                     color: Colors.black,
//                           //                     blurRadius: 2,
//                           //                     offset: Offset(1, 1))
//                           //               ],
//                           //               shape: BoxShape.circle,
//                           //               color: Colors.white,
//                           //               // borderRadius: BorderRadius.circular(20.0)
//                           //             ),
//                           //             child: SvgPicture.asset(
//                           //               "assets/images/cancel.svg",
//                           //               color: Colors.black,
//                           //               height: 10,
//                           //               width: 10,
//                           //             ),
//                           //           ),
//                           //         ),
//                           //       )
//                           //     : SizedBox(),

//                           // widget.isTextEditing ? Positioned(

//                           //   child: IconButton(
//                           //     tooltip: "Edit Text",
//                           //     icon: Container(
//                           //       margin:
//                           //         EdgeInsets.only(
//                           //           bottom: 10,
//                           //           right: 10
//                           //         ),
//                           //       child: Icon(Icons.mode_edit)),
//                           //     onPressed: () {
//                           //       print("Edit");
//                           //       setState(() {
//                           //         _editingController.text = textValue.text;
//                           //         textValue.isEditing = true;
//                           //       });
//                           //     },
//                           //   ),
//                           // ) : SizedBox(),

//                           //rotate Sticker
//                           widget.isTextEditing
//                               ? Positioned(
//                                   bottom: 0,
//                                   left: 0,
//                                   child: GestureDetector(
//                                     onPanStart: (details) {
//                                       final touchPositionFromCenter =
//                                           details.localPosition -
//                                               centerOfGestureDetector;
//                                       upsetAngle = textValue.rotationAngle -
//                                           touchPositionFromCenter.direction;
//                                     },
//                                     onPanUpdate: (details) {
//                                       final touchPositionFromCenter =
//                                           details.localPosition -
//                                               centerOfGestureDetector;
//                                       setState(() {
//                                         textValue.rotationAngle =
//                                             touchPositionFromCenter.direction +
//                                                 upsetAngle;
//                                       });
//                                     },
//                                     onPanEnd: (details) {
//                                       textValue.rotationAngle =
//                                           textValue.rotationAngle.abs();
//                                       print(
//                                           "New rotation angle will be:- ${textValue.rotationAngle}");
//                                       previousAngle = 0.0;
//                                     },
//                                     child: Container(
//                                       padding: EdgeInsets.all(5.0),
//                                       decoration: BoxDecoration(
//                                         boxShadow: [
//                                           BoxShadow(
//                                               color: Colors.black,
//                                               blurRadius: 2,
//                                               offset: Offset(1, 1))
//                                         ],
//                                         shape: BoxShape.circle,
//                                         color: Colors.white,
//                                         // borderRadius: BorderRadius.circular(20.0)
//                                       ),
//                                       child: Icon(
//                                         Icons.rotate_right,
//                                         size: 12,
//                                       ),
//                                     ),
//                                   ),
//                                 )
//                               : SizedBox(),
//                         ],
//                       ),
//                     );
//                   },
//                 ),
//               ),
//             ));
//       }).toList(),
//     );
//   }

//   @override
//   void dispose() {
//     _editingController.dispose();
//     super.dispose();
//   }

//   // ignore: missing_return
//   Widget decoratedText() {
//     if (widget.isTextEditing) {
//       if (editingIndex >= 0) {
//         // widget.textList[editingIndex].textStyleIndex = widget.finalTextStyleIndex;
//         widget.textList[editingIndex].textStyleIndex =
//             widget.finalTextStyleIndex;

//         widget.textList[editingIndex].textStyle =
//             widget.fontStyleList[widget.state.textStyleIndex](
//                 foreground: Paint()
//                   ..style = PaintingStyle.stroke
//                   ..strokeMiterLimit = 10
//                   ..strokeWidth = widget.state.textList[editingIndex].strockValue / 10
//                   ..strokeJoin = StrokeJoin.miter
//                   ..strokeCap = StrokeCap.square
//                   ..color = widget.textList[editingIndex].strockColor
//                       .withOpacity(widget.textList[editingIndex].textOpacity),
//                 fontSize: 14.0);

//         widget.textList[editingIndex].textStyle2 =
//             widget.fontStyleList[widget.state.textStyleIndex](
//                 color: widget.textList[editingIndex].textColor
//                     .withOpacity(widget.textList[editingIndex].textOpacity),
//                 fontSize: 14.0);
//         // }
//       }
//     }
//   }
// }
