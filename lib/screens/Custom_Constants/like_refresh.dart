import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/SizeConfig.dart';

class LikeAndRefresh extends StatefulWidget {
  @override
  _LikeAndRefreshState createState() => _LikeAndRefreshState();
}

class _LikeAndRefreshState extends State<LikeAndRefresh> {
  bool isLiked = false;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
          onTap: (){
            setState(() {
              isLiked = !isLiked;
            });
          },
          child: Container(
            height: 40,
            width: 40,
            // padding: EdgeInsets.all(5.0),
            margin: EdgeInsets.only(top: 8.0),
            // decoration: BoxDecoration(
            //     color: Colors.white,
            //     boxShadow: [
            //       BoxShadow(
            //           color: Colors.black45,
            //           blurRadius: 3
            //       ),
            //     ],
            //     borderRadius: BorderRadius.circular(50.0)
            // ),
            child: Center(
              child: isLiked ? Icon(Icons.favorite,color: Colors.red,):Icon(Icons.favorite_border,color: Colors.black,),

            ),
          ),
        ),
        // Container(
        //   height: defaultSize * 3,
        //   width: defaultSize * 3,
        //   padding: EdgeInsets.all(5.0),
        //   margin: EdgeInsets.all(defaultSize * 1),
        //   decoration: BoxDecoration(
        //       color: Colors.white,
        //       boxShadow: [
        //         BoxShadow(
        //             color: Colors.black45,
        //             blurRadius: 3
        //         ),
        //       ],
        //       borderRadius: BorderRadius.circular(50.0)
        //   ),
        //   child: Center(
        //     child: SvgPicture.asset("assets/images/undo.svg"),
        //   ),
        // )
      ],
    );
  }
  }

