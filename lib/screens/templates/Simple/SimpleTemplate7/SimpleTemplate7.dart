// ignore: unused_import
import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_editing_flutter_app/SizeConfig.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
import 'package:image_editing_flutter_app/models/draft_template_model.dart';
import 'package:image_editing_flutter_app/models/drag_drop.dart';
import 'package:flutter/services.dart';
import 'package:image_editing_flutter_app/models/radio_model.dart';
import 'package:image_editing_flutter_app/models/saved_template_model.dart';
import 'package:image_editing_flutter_app/models/sticker_list_model.dart';
import 'package:image_editing_flutter_app/models/text_list_model.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/Appbar.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/show_final_image.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/widgets.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/text_style_list.dart';
import 'package:image_editing_flutter_app/screens/collection/components/radio_item.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/bottom_nav_bar_item.dart';
// import 'package:image_editing_flutter_app/screens/Custom_Constants/like_refresh.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate7/mainImageBody.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/EditText.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/color_picker.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/text_strock.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/text_align.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/StickerPage.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/BackgroundPage.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_editing_flutter_app/models/ContainerList.dart';

// ignore: must_be_immutable
class SimpleTemplate7 extends StatefulWidget {
  final DragDrop dataPotraid;
  //final DragDrop dataLandscape;
  var response;
  JsonCode editjson;
  DraftJsonCode draftJsonCode;
  var draftId;
  var editId;
  var height;
  var width;
  File image1;
  File image2;

//  final bool checkOrientation;
  SimpleTemplate7(
      {Key key,
      this.dataPotraid,
      this.image1,
      this.image2,
      this.response,
      this.editjson,
      this.draftJsonCode,
      this.editId,
      this.draftId,
      this.height,
      this.width
      /* this.checkOrientation*/
      })
      : super(key: key);

  // static SimpleTemplate7State of(BuildContext context) =>
  //     //   ignore: deprecated_member_use
  //     context.ancestorStateOfType(const TypeMatcher<SimpleTemplate7State>());
  @override
  SimpleTemplate7State createState() =>
      SimpleTemplate7State(editingimage1: image1, editingimage2: image2);
}

class SimpleTemplate7State extends State<SimpleTemplate7> {
  File editingimage1;
  File image1;
  File image2;
  File editingimage2;
  bool imageselected1 = false;
  bool imageselected2 = false;
  bool isImageEditing = false;
  var adminJson;

  SimpleTemplate7State({this.editingimage1, this.editingimage2});

  final GlobalKey myglobalKey = new GlobalKey();
  final GlobalKey<ScaffoldState> myglobalkey1 = new GlobalKey<ScaffoldState>();
  final GlobalKey flexibleKey = new GlobalKey();
  AppbardataState appbardataState = AppbardataState();
  ShowImageState showImageState = ShowImageState();

//  DragDrop data;

  bool imageSelected = false;
  // ignore: unused_field
  int _currentIndex = 0;
  var response;
  bool isTextediting = false;
  // bool ec.isOpenWidget.value = false;
  double textOpacity = 1.0;
  // bool ec.isOpenSticker.value = false;
  // bool ec.isOpenBackground.value = false;
  // bool ec.isOpenTextbar.value = false;
  bool isImageSaveLoading = false;
  String sticker = "assets/stickers/sticker_3.png";
  List<String> stickerlist2 = [];
  // List<StickerList> stickerList = List<StickerList>();
  Color textColor = Color.fromARGB(255, 0, 0, 0);
  int textStyleIndex;
  double strockSliderValue = 0.0;
  int textAlignIndex = 0;
  int textIndex = 0;
  List fontStyleList = [];
  Color textStrockColor = Color.fromARGB(255, 0, 0, 0);
  Random rng = new Random();
  Widget myWidget;
  File imageFile;
  // List<TextListModel> textList = List<TextListModel>();
  // List<RadioModel> sampleData = new List<RadioModel>();
  // ignore: non_constant_identifier_names
  String background_image;
  // ignore: avoid_init_to_null
  int textStyleEditingIndex = null;
  // List<ContainerList> containerList = List<ContainerList>();
  List<StickerList> stickerList = <StickerList>[];
  List<TextListModel> textList = <TextListModel>[];
  List<RadioModel> sampleData = <RadioModel>[];
  List<ContainerList> containerList = <ContainerList>[];
  var editId;
  var draftId;
  EditZoomController ec = Get.put(EditZoomController());
  var datawidth;
  var dataheight;
   var dataleft;
  File fileImage;
  var finalimage;
  var datatop;
  var dataAspectRatio;
  var dataposition1;
  var templateKey = 'Si_07';
  var jsonCode1 = "";
  var isSaved = false;
  File imagePath;
var editJson;

  @override
  void initState() {
   super.initState();
    response = widget.response;
    editJson = widget.editjson;
    if(widget.editjson != null){
      editJson = widget.editjson;
    }
    if(widget.draftJsonCode != null){
      editJson = widget.draftJsonCode;
    }
    GoogleFonts.asMap().forEach((key, value) {
      fontStyleList.add(value);
    });
   sampleData.add(new RadioModel(true, 'Color'));
    sampleData.add(new RadioModel(false, 'Font Style'));

    sampleData.add(new RadioModel(false, 'Strock'));
    sampleData.add(new RadioModel(false, 'Align'));
   editId = widget.editId;
    draftId = widget.draftId;
    if(ec.isdraftLoading.value == true){
      ec.isdraftLoading.value = false;
    }

    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
        datawidth = widget.width;
    dataAspectRatio = 0.70;
    dataheight = datawidth / dataAspectRatio;

    if (ec.isEditTemplate.value) {
      background_image = editJson.backGround.background;
      editJson.containers.forEach((element) {
        containerList.add(ContainerList(
          height: double.parse(element.height),
          width: double.parse(element.width),
          // ignore: unrelated_type_equality_checks
          containerimage: File(element.containerImage
              .replaceAll('File:', "")
              .replaceAll('\'', "")
              .replaceAll(' ', "")) == "null" ? null : File(element.containerImage
              .replaceAll('File:', "")
              .replaceAll('\'', "")
              .replaceAll(' ', "")),
          scale: double.parse(element.scale),
          xposition: double.parse(element.xposition),
          yposition: double.parse(element.yposition),
          rotation: double.parse(element.rotation),
        ));
      });
      ec.callDecoratedfun.value = true;

      editJson.textlist.forEach((element) {
        textList.add(TextListModel(
          text: element.text,
          finalTextAlignIndex: int.parse(element.finalTextAlignIndex),
          height: double.parse(element.height),
          width: double.parse(element.width),
          rotationAngle: double.parse(element.rotationAngle),
          scaleFactor: double.parse(element.scaleFactor),
          textStyleIndex: int.parse(element.textStyleIndex),
          strockValue: double.parse(element.strockValue),
          textOpacity: double.parse(element.textOpacity),
          strockColor: Color(int.parse(element.strockColor
              .replaceAll('Color(', '')
              .replaceAll(')', '')
              .replaceAll('Materialprimary value: ', ''))),
          textColor: Color(int.parse(element.textColor
              .replaceAll('Color(', '')
              .replaceAll(')', '')
              .replaceAll('Materialprimary value: ', ''))),
          xPosition: double.parse(element.xPosition),
          yPosition: double.parse(element.yposition),
          textStyle: textstyleList[int.parse(element.textStyleIndex)](
              color: Color(int.parse(element.textColor
                  .replaceAll('Color(', '')
                  .replaceAll(')', '')
                  .replaceAll('Materialprimary value: ', '')))),
          textStyle2: textstyleList[int.parse(element.textStyleIndex)](
              color: Color(int.parse(element.textColor
                  .replaceAll('Color(', '')
                  .replaceAll(')', '')
                  .replaceAll('Materialprimary value: ', '')))),
        ));
      });

      editJson.sticker.forEach((element) {
        stickerList.add(StickerList(
          height: double.parse(element.height),
          width: double.parse(element.width),
          xPosition: double.parse(element.xposition),
          yPosition: double.parse(element.yposition),
          name: element.name,
          rotation: double.parse(element.rotation),
          scale: double.parse(element.scale),
        ));
      });
    }else{
    
    background_image = "https://wallpaperaccess.com/full/1076238.jpg";
   
    
    
    ec.callDecoratedfun.value = true;
    containerList.add(ContainerList(
      height: dataheight * 0.80,
      width: datawidth * 0.80,
      left: dataleft,
      xposition: 0,
      yposition: 0.10,
      top: datatop,
      scale: 1.0,
      rotation: 0.0,
    ));
  }
  }

  TextStyle textStyle = GoogleFonts.aclonica(fontSize: 14);

  TextStyle textStyle1 = GoogleFonts.aclonica(fontSize: 14);

  void addText() {
    ec.callDecoratedfun.value = true;
    setState(() {
      textList.add(TextListModel(
        text: "Double tap to edit",
        xPosition: 0.50,
        yPosition: 0.50,
        finalTextAlignIndex: 1,
        height: 30,
        width: 80,
        rotationAngle: 0.0,
        scaleFactor: 2.0,
        textStyle: GoogleFonts.concertOne(),
        textStyle2: GoogleFonts.concertOne(),
        textStyleIndex: 0,
        textColor: Colors.black,
        strockColor: Colors.black,
        strockValue: 0,
        textOpacity: 1,
      ));
      ec.textediting.value = textList.length;
    });
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        resizeToAvoidBottomInset: false,
        key: myglobalkey1,
        appBar: (ec.isOpenBackground.value ||
                ec.isOpenTextbar.value ||
                ec.isOpenSticker.value ||
                ec.isOpenWidget.value ||
                isTextediting)
            ? PreferredSize(
                child: Container(
                  color: Colors.transparent,
                ),
                preferredSize: Size.fromHeight(0.0))
            : PreferredSize(
                child: Appbardata(
                  state: this,
                ),
                preferredSize: Size.fromHeight(50.0)),
        body:WillPopScope(
                child: isImageSaveLoading
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : SizeConfig.orientation == Orientation.portrait
                        ? editingBody()
                        : editingBody(),
                onWillPop: _onBackPressed),
      );
    });
  }

  // ignore: missing_return
  Future<bool> _onBackPressed() {
    isTextediting = false;
    if (ec.isOpenSticker.value == false &&
        ec.isOpenTextbar.value == false &&
        ec.isOpenWidget.value == false &&
        ec.isOpenBackground.value == false &&
        isTextediting == false) {
      onBackPressed();
    }
    if (ec.isOpenSticker.value == true) {
      setState(() {
        ec.isOpenSticker.value = false;
        ec.isOpenWidget.value = false;
      });
    }
    if (ec.isOpenBackground.value == true) {
      setState(() {
        ec.isOpenWidget.value = false;
        ec.isOpenBackground.value = false;
      });
    }
    if (ec.isOpenTextbar.value == true) {
      setState(() {
        ec.isOpenTextbar.value = false;
        ec.isOpenWidget.value = false;
        isTextediting = false;
      });
    }
    if (isTextediting == true) {
      setState(() {
        isTextediting = false;
      });
    }

    // if (popScreen == true) {
    //   // print("popped screen");
    //   // Navigator.pop(context);
    // }
  }

// ignore: missing_return
  Widget onBackPressed() {
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            child: Container(
              height: 300,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    height: 100,
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.circular(30.0),
                        ),
                    ),
                     child: Center(
                      child: Obx((){
                        return ec.isdraftLoading.value ? CircularProgressIndicator(color: Colors.white,):Icon(
                        Icons.exit_to_app_rounded,
                        size: 50,
                        color: Colors.white,
                      );
                      })
                    ),
                  ),
                  Container(
                    height: 130,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Alert!",
                            style: TextStyle(
                                fontSize: 28, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Are you sure, you want to Exit?",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                (editId == null && draftId == null) ? !isSaved ?
                       Container(
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                ElevatedButton(
                                  onPressed: () {
                                    ec.isdraftLoading.value = true;
                                    saveAsDraft();
                                  },
                                  child: Text("Save as draft"),
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.blue[500],
                                    shadowColor: Colors.grey,
                                    onPrimary: Colors.white,
                                  ),
                                ),
                                Row(
                                  children: [
                                    ElevatedButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: Text("No")),
                                    SizedBox(
                                      width: 7.5,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          ec.innerPhotoview.value = 1;
                                          ec.textediting.value = 0;
                                          ec.stickerview.value = 1;
                                          ec.ListofFiles();
                                          ec.ListofFilesDraft();
                                          ec.isEditTemplate.value = false;
                                          //Function called
                                          Navigator.of(context).pop();
                                          Navigator.pop(context);
                                        },
                                        child: Text("Yes"))
                                  ],
                                )
                              ],
                            ),
                          ),
                        )
                      : Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("No")),
                              ElevatedButton(
                                  onPressed: () {
                                    ec.innerPhotoview.value = 1;
                                    ec.textediting.value = 0;
                                    ec.stickerview.value = 1;
                                    ec.ListofFiles();
                                    ec.isEditTemplate.value = false;
                                    //Function called
                                    Navigator.of(context).pop();
                                    Navigator.pop(context);
                                  },
                                  child: Text("Yes"))
                            ],
                          ),
                        ) :
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("No")),
                              ElevatedButton(
                                  onPressed: () {
                                    ec.innerPhotoview.value = 1;
                                    ec.textediting.value = 0;
                                    ec.stickerview.value = 1;
                                    ec.ListofFiles();
                                    ec.isEditTemplate.value = false;
                                    //Function called
                                    Navigator.of(context).pop();
                                    Navigator.pop(context);
                                  },
                                  child: Text("Yes"))
                            ],
                          ),
                        ) 
                ],
              ),
            ),
          );
        });
  }

 Future saveAsDraft() async{
   bool value = true;
   containerList.forEach((element) {
     if(element.containerimage == null){
       value = false;
     }
   });
    // ec.isdraft.value = true;
    if(value){
    appbardataState.export(this);
    await appbardataState.takeScreenshot(this,true);
    print(fileImage);
    print(finalimage);
    savefileDraft(fileImage, finalimage,this).then((value){
      Navigator.of(context).pop();
    Navigator.pop(context);
    ec.ListofFilesDraft();
    });    
    }
    else{
      ec.isdraftLoading.value = false;
      Navigator.pop(context);
      Fluttertoast.showToast(msg: "Add image to save in draft");
    }
  }

  Widget editingBody() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          flex: 4,
          key: flexibleKey,
          child: MainImageEditingBody(
             dataPotraid: widget.dataPotraid,
            state: this,
            // image1: editingimage1 != null ? editingimage1 : widget.image1,
            // image2: editingimage2 != null ? editingimage2 : widget.image2,
            finalsticker: sticker,
            finalstickerList: stickerList,
            imageselected1: false,
            imageselected2: false,
            finalstickerList2: stickerlist2,
            editJson: editJson,
          ),
        ),
        SingleChildScrollView(child: bottomnavbar()),
      ],
    );
  }

  Widget bottomnavbar() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        // if (ec.isOpenBackground.value == false && ec.isOpenWidget.value == false)
        //   Padding(
        //     padding: EdgeInsets.only(bottom: 10.0),
        //     child: LikeAndRefresh(),
        //   ),
        (ec.isOpenBackground.value ||
                    ec.isOpenSticker.value ||
                    ec.isOpenTextbar.value) &&
                ec.isOpenWidget.value
            ? myWidget
            : SizedBox(),
       ec.isOpenTextbar.value
            ? textBottomNavBar()
            : bottom_nav()
      ],
    );
  }

  Widget textBottomNavBar() {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 7.0),
      child: Card(
        child:  Container(
          height: 50,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.all(5.0),
                child: InkWell(
                  borderRadius: BorderRadius.circular(5.0),
                    onTap: () {
                      setState(() {
                        sampleData
                            .forEach((element) => element.isSelected = false);
                        sampleData[index].isSelected = true;
                        textIndex = index;
                        if (index == 1) {
                        // ec.isOpenWidget.value = true;
                        setState(() {
                          myWidget = EditText(
                            fontStyleList: fontStyleList,
                            state: this,
                            myWidgetIndex: textStyleIndex,
                          );
                          ec.isOpenWidget.value = true;
                          ec.isOpenTextbar.value = true;
                        });
                      } else if (index == 0) {
                        // ec.isOpenWidget.value = true;
                        setState(() {
                          myWidget = TextColorPicker(
                            width: MediaQuery.of(context).size.width,
                            state: this,
                            selectedTextIndex: textStyleEditingIndex,
                          );
                          ec.isOpenWidget.value = true;
                          ec.isOpenTextbar.value = true;
                        });
                      } else if (index == 2) {
                        setState(() {
                          myWidget = TextStrock(
                            state: this,
                            selectedIndex: textStyleEditingIndex,
                          );
                          ec.isOpenWidget.value = true;
                          ec.isOpenTextbar.value = true;
                        });
                      } else if (index == 3) {
                        setState(() {
                          myWidget = SetTextAlign(
                            state: this,
                            selectedTextIndex: textStyleEditingIndex,
                            textStyleList: fontStyleList,
                          );
                          ec.isOpenWidget.value = true;
                          ec.isOpenTextbar.value = true;
                        });
                      }
                    });
                   },
                    child: Container(
                        // margin: EdgeInsets.all(5.0),
                    // padding: EdgeInsets.all(5.0),
                    width: MediaQuery.of(context).size.width / 5.2,
                        child: RadioItem(sampleData[index]))),
              );
            },
            itemCount: sampleData.length,
          ),
        ),
      ),
    );
  }

  // ignore: non_constant_identifier_names
  Padding bottom_nav() {
    return Padding(
      padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 5.0),
      child: Container(
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            BottomNavBarItem(
                image: "assets/images/add.svg",
                title: "Add",
                onPress: () {
                  setState(() {
                    containerList.add(ContainerList(
                      height: dataheight * 0.80,
                      width: datawidth * 0.80,
                      left: dataleft,
                      xposition: 0,
                      yposition: 0.10,
                      top: datatop,
                      scale: 1.0,
                      rotation: 0.0,
                    ));
                  });
                }),
            BottomNavBarItem(
                image: "assets/images/Background.svg",
                title: "Background",
                onPress: () {
                  setState(() {
                    ec.isOpenSticker.value = false;
                    ec.isOpenTextbar.value = false;
                    ec.isOpenBackground.value = true;
                    myWidget = BackgroundPage(
                      state: this,
                    );
                    ec.isOpenWidget.value = true;
                  });
                }),
            BottomNavBarItem(
              image: "assets/images/sentiment_satisfied_alt-black-18dp.svg",
              title: "Stickers",
              onPress: () {
                setState(() {
                  ec.isOpenSticker.value = true;
                  ec.isOpenWidget.value = true;
                  ec.isOpenBackground.value = false;
                  ec.isOpenTextbar.value = false;
                  myWidget = StickerPage(state: this);
                });
              },
            ),
            BottomNavBarItem(
              image: "assets/images/letter-a.svg",
              title: "Text",
              onPress: () {
                addText();
                // ec.isOpenSticker.value = false;
                //ec.isOpenWidget.value = true;
                // ec.isOpenBackground.value = false;
                //ec.isOpenTextbar.value = true;
                // myWidget = null;
              },
            ),
          ],
        ),
      ),
    );
  }

//   export() {
//     List datacontainer = [];
//     List datasticker = [];
//     // ignore: unused_local_variable
//     String databackground;
//     List datatextlist = [];
//     var datamaincontainer;

//     datamaincontainer = data;
//     for (int i = 0; i < containerList.length; i++) {
//       datacontainer.add(containerList[i]);
//     }
//     for (int i = 0; i < stickerList.length; i++) {
//       datasticker.add(stickerList[i]);
//     }
//     databackground = background_image;
//     for (int i = 0; i < textList.length; i++) {
//       datatextlist.add(textList[i]);
//     }

//     String backgroundJson = '{\"Background\" : \"$databackground\"}';
//    // String mainContainerJson = '{\n "height" : "${datamaincontainer.height}", \n "width" : "${datamaincontainer.width}", \n ""                \n}';
//     String datacontainerJson = "";
//     // '{\n"width" : "${datacontainer[0].width}", \n"height" : "${datacontainer[0].height}", \n"initial-left" : "${datacontainer[0].left}",  \n"initial-top" : "${datacontainer[0].top}",  \n"xposition" : "${datacontainer[0].xposition}",  \n"yposition" : "${datacontainer[0].yposition}", \n"containerImage" : "${datacontainer[0].containerimage}", \n"scale" : "${datacontainer[0].scale}", \n"rotation" : "${datacontainer[0].rotation}"\n}';
//     for (int i = 0; i < datacontainer.length; i++) {
//       datacontainerJson = datacontainerJson +
//           '{\n"width" : "${datacontainer[i].width}", \n"height" : "${datacontainer[i].height}", \n"initial-left" : "${datacontainer[i].left}",  \n"initial-top" : "${datacontainer[i].top}",  \n"xposition" : "${datacontainer[i].xposition}",  \n"yposition" : "${datacontainer[i].yposition}", \n"containerImage" : "${datacontainer[i].containerimage}", \n"scale" : "${datacontainer[i].scale}", \n"rotation" : "${datacontainer[i].rotation}"\n}';
//       if (i != datacontainer.length - 1) {
//         datacontainerJson = datacontainerJson + ',\n';
//       }
//     }
// //print(datacontainerJson);

//     String datastickerJson = "";

//     for (int i = 0; i < datasticker.length; i++) {
//       datastickerJson = datastickerJson +
//           '{\n"name" : "${datasticker[i].name}", \n"width" : "${datasticker[i].width}", \n"height" : "${datasticker[i].height}",  \n"xposition" : "${datasticker[i].xPosition}",  \n"yposition" : "${datasticker[i].yPosition}", \n"scale" : "${datasticker[i].scale}", \n"rotation" : "${datasticker[i].rotation}"\n}';
//       if (i != datasticker.length - 1) {
//         datastickerJson = datastickerJson + ',\n';
//       }
//     }
// //print(datastickerJson);

//     String datatextlistJson = "";

//     for (int i = 0; i < datatextlist.length; i++) {
//       datatextlistJson = datatextlistJson +
//           '{\n"text" : "${datatextlist[i].text}", \n"xPosition" : "${datatextlist[i].xPosition}",  \n"yposition" : "${datatextlist[i].yPosition}", \n"height" : "${datatextlist[i].height}",\n"width" : "${datatextlist[i].width}",\n"rotationPosition" : "${datatextlist[i].rotationPosition}",\n"isEditing" : "${datatextlist[i].isEditing}",\n"textStyleIndex" : "${datatextlist[i].textStyleIndex}",\n"finalTextAlignIndex" : "${datatextlist[i].finalTextAlignIndex}",\n"textColor" : "${datatextlist[i].textColor}",\n"strockColor" : "${datatextlist[i].strockColor}",\n"strockValue" : "${datatextlist[i].strockValue}",\n"textOpacity" : "${datatextlist[i].textOpacity}",\n"rotationAngle" : "${datatextlist[i].rotationAngle}",\n"scaleFactor" : "${datatextlist[i].scaleFactor}",\n"finalHeight" : "${datatextlist[i].finalHeight}",\n"finalWidth" : "${datatextlist[i].finalWidth}",\n"heightScale" : "${datatextlist[i].heightScale}",\n"widthScale" : "${datatextlist[i].widthScale}",\n"containerHeight" : "${datatextlist[i].containerHeight}",\n"containerWidth" : "${datatextlist[i].containerWidth}"\n}';
//         if (i != datatextlist.length - 1) {
//         datatextlistJson = datatextlistJson + ',\n';
//       }
//     }

//   //  print(datatextlistJson);

//    //  print(backgroundJson);

//     String mainJsondata = '{\n "Sticker": [$datastickerJson] , \n"BackGround": [$backgroundJson],\n "containers" : [$datacontainerJson],\n "Textlist": [$datatextlistJson] \n}';

//     Navigator.push(context, MaterialPageRoute(builder: (context){
//       return Extra(json: mainJsondata,);
//     }));
//  //   print (mainJsondata);

//   }
// }

}

/* 
  @override
  void initState() {
  super.initState();
    response = widget.response;
    editJson = widget.editjson;
    if(widget.editjson != null){
      editJson = widget.editjson;
    }
    if(widget.draftJsonCode != null){
      editJson = widget.draftJsonCode;
    }
    GoogleFonts.asMap().forEach((key, value) {
      fontStyleList.add(value);
    });
    sampleData.add(new RadioModel(true, 'Color'));
    sampleData.add(new RadioModel(false, 'Font Style'));
    
    sampleData.add(new RadioModel(false, 'Strock'));
    sampleData.add(new RadioModel(false, 'Align'));

    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    
    background_image =
        "https://wallpaperaccess.com/full/1076238.jpg";
    //print(response);
   // data = widget.dataPotraid;
    // data.width = data.screenwidth - 2*(data.screenwidth / 60 );
    // data.height = data.width * double.parse(response["Templates"]["Funaral"][0]["Main-Container"][0]["Aspect-ratio"]);
    //widget.width = 300;
    datawidth = widget.width;
    dataAspectRatio = 0.70;
    dataheight = datawidth / dataAspectRatio; 
    dataposition1= Offset(datawidth * 0.125, dataheight * 0.125);
     containerList.add(ContainerList(
                      height: dataheight,
                      width: datawidth,
                      left: dataleft,
                      xposition: datawidth * 0,
                      yposition: dataheight * 0.10,
                      top: datatop,
                    ));
  }
*/
