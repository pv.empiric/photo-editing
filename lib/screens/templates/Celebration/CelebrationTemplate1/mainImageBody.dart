import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:gesture_x_detector/gesture_x_detector.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
import 'package:image_editing_flutter_app/models/drag_drop.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/Custom_Constants/widgets.dart';
// import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate0/mainImageBody.dart';
import 'package:photo_view/photo_view.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/text_list.dart';
// import 'package:image_editing_flutter_app/screens/Custom_Constants/color_picker_of_image.dart';

// ignore: unused_import

import 'package:image_editing_flutter_app/screens/Custom_Constants/stickerview.dart';
import 'template1containerView.dart';

// import 'package:image_editing_flutter_app/screens/Custom_Constants/containerView.dart';
// ignore: must_be_immutable
class MainImageEditingBody extends StatefulWidget {
  final DragDrop dataPotraid;
  var state;
  File image1;
  File image2;
  bool imageselected1;
  bool imageselected2;
  String finalsticker;
  List finalstickerList;
  List<String> finalstickerList2;
  var editJson;
  Offset temp = Offset(150, 200);
  MainImageEditingBody({
    this.dataPotraid,
    this.image1,
    this.image2,
    this.finalsticker,
    this.finalstickerList,
    this.imageselected1,
    this.imageselected2,
    this.state,
    this.editJson,
    this.finalstickerList2,
  });
  @override
  _MainImageEditingBodyState createState() => _MainImageEditingBodyState();
}

class _MainImageEditingBodyState extends State<MainImageEditingBody> {
  // DragDrop data;
  var state;
  PhotoViewController photoViewController1;
  PhotoViewController photoViewController2;
  Offset a;
  Color color1 = Colors.white;
  Color color2 = Colors.white;
  
  List<Color> currentColors = [Colors.white, Colors.green];
  // Color currentColor = Colors.white;

  changeColor1(Color color) => setState(() => color1 = color);
  changeColor2(Color color) => setState(() => color2 = color);
  void changeColors(List<Color> colors) =>
      setState(() => currentColors = colors);

  @override
  void initState() {
    // data = widget.dataPotraid;
    state = widget.state;
    photoViewController1 = PhotoViewController();
    photoViewController2 = PhotoViewController();
    a = Offset(
        state.datawidth * 0.0 - state.datawidth * 0.1, state.dataheight * 0.70);
    super.initState();
  }

  EditZoomController eC = Get.put(EditZoomController());

  Widget build(BuildContext context) {
    return Stack(
      children: [
        RepaintBoundary(
          key: state.myglobalKey,
          child: Container(
              margin: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height /
               ( !eC.isEditTemplate.value?
                    60 : double.parse(widget.editJson.mainContainer.marginVerticle)),
                horizontal: MediaQuery.of(context).size.width /
               ( !eC.isEditTemplate.value?
                    60 : double.parse(widget.editJson.mainContainer.marginHorizontal)),
              ),
              child: Container(
                // child: RepaintBoundary(
                //   key: state.myglobalKey,
                child: Stack(
                  // ignore: deprecated_member_use
                  overflow: Overflow.clip,
                  children: [
                    // if (state.image1 != null &&
                    //     state.image2 != null)
                    AspectRatio(
                       aspectRatio: state.dataAspectRatio,
                      child: XGestureDetector(
                        onTap: (_) {
                          setState(() {
                            // state.isTextediting = false;
                            // state.isOpenTextbar = false;
                            // state.isOpenWidget = false;
                          });
                        },
                        child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0),
                          border: Border.all(
                              color: Colors.grey.withOpacity(0.5),
                              width: 2.0),
                        ),
                        child: Stack(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.black,
                                borderRadius: BorderRadius.circular(10.0)
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                                                child: GetFancyShimmerImage(
                                  url: state.background_image,
                                  boxFit: BoxFit.cover,
                                  width: double.infinity
                                ),
                              ),
                            ),
                            Stack(
                            children: [
                              if (state.containerList.length != 0)
                                ContainerView(
                                  state: state,
                                  //  containerlist: state.containerList,
                                ),
                              // Positioned(
                              //   right: state.datawidth * 0.00,
                              //   top: state.dataheight * 0.05,
                              //   child: GestureDetector(
                              //     onTap: () {
                              //       showColorDialog2(context);
                              //     },
                              //     child: Container(
                              //       height: state.dataheight * 0.50,
                              //       width: state.datawidth * 0.325,
                              //       child: ColorFiltered(
                              //         colorFilter: ColorFilter.mode(
                              //             color2, BlendMode.modulate),
                              //         child: Image.network(
                              //           "https://images2.imgbox.com/eb/4b/IzPEkSdu_o.png",
                              //           fit: BoxFit.fill,
                              //         ),
                              //       ),
                              //     ),
                              //   ),
                              // ),
                              // Positioned(
                              //   left: state.datawidth * 0.02,
                              //   bottom: state.dataheight * 0.01,
                              //   child: GestureDetector(
                              //     onTap: () {
                              //       showColorDialog1(context);
                              //     },
                              //     child: Container(
                              //       height: state.dataheight * 0.2,
                              //       width: state.datawidth * 0.4,
                              //       child: ColorFiltered(
                              //           colorFilter: ColorFilter.mode(
                              //               color1, BlendMode.modulate),
                              //           child: Image.network(
                              //             "https://images2.imgbox.com/4b/91/dHQsdohq_o.png",
                              //             fit: BoxFit.fill,
                              //           )),
                              //     ),
                              //   ),
                              // ),
                              if (state.stickerList.length != 0)
                                StickerView(
                                  stickerList: state.stickerList,
                                  state: state,
                                  isImageEditing: state.isImageEditing,
                                ),
                              if (state.textList.length != 0)
                                TextList(
                                  textList: state.textList,
                                  state: state,
                                  isImageEditing: state.isImageEditing,
                                  isTextEditing: state.isTextediting,
                                  fontStyleList: state.fontStyleList,
                                  finalTextStyleIndex: state.textStyleIndex,
                                ),
                            ],
                          ),
                          ])
                        ),
                      ),
                    )
                  ],
                ),
              )),
        )
      ],
    );
  }

  showColorDialog1(context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          titlePadding: const EdgeInsets.all(0.0),
          contentPadding: const EdgeInsets.all(0.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
          ),
          content: SingleChildScrollView(
            child: SlidePicker(
              pickerColor: color1,
              onColorChanged: changeColor1,
              paletteType: PaletteType.rgb,
              enableAlpha: false,
              displayThumbColor: true,
              showLabel: false,
              showIndicator: true,
              indicatorBorderRadius: const BorderRadius.vertical(
                top: const Radius.circular(25.0),
              ),
            ),
          ),
        );
      },
    );
  }

  showColorDialog2(context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          titlePadding: const EdgeInsets.all(0.0),
          contentPadding: const EdgeInsets.all(0.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
          ),
          content: SingleChildScrollView(
            child: SlidePicker(
              pickerColor: color2,
              onColorChanged: changeColor2,
              paletteType: PaletteType.rgb,
              enableAlpha: false,
              displayThumbColor: true,
              showLabel: false,
              showIndicator: true,
              indicatorBorderRadius: const BorderRadius.vertical(
                top: const Radius.circular(25.0),
              ),
            ),
          ),
        );
      },
    );
  }
}
