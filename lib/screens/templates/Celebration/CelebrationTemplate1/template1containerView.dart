import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gesture_x_detector/gesture_x_detector.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
import 'package:image_picker/image_picker.dart';
import 'package:photo_view/photo_view.dart';

// ignore: must_be_immutable
class ContainerView extends StatefulWidget {
  var state;
  List containerlist;
  ContainerView({this.state});
  @override
  _ContainerViewState createState() => _ContainerViewState();
}

class _ContainerViewState extends State<ContainerView> {
  //var data;
  var state;
  // bool innerPhotoview = true;

  // PhotoViewController controller;
  List<PhotoViewController> _controller = [];
  List<PhotoViewController> controller = [];
  List<PhotoViewScaleStateController> scaleStateController = [];
  var screen;
  @override
  void initState() {
    state = widget.state;
    screen = Size(state.datawidth, state.dataheight);
    super.initState();
  }

  void onController(PhotoViewControllerValue value) {
    setState(() {
      // print(value);
    });
  }

  void onScaleState(PhotoViewScaleState scaleState) {
    print(scaleState);
  }

  EditZoomController editZoomController = Get.put(EditZoomController());
  Offset _initPos;
  Offset _currentPos = Offset(0, 0);
  double _currentScale;
  double _currentRotation;

  @override
  Widget build(BuildContext context) {
    for (var i = 0; i < state.containerList.length; i++) {
      controller
          .add(PhotoViewController()..outputStateStream.listen(onController));
    }
    for (var i = 0; i < state.containerList.length; i++) {
      scaleStateController.add(PhotoViewScaleStateController()
        ..outputScaleStateStream.listen(onScaleState));
    }
    for (var i = 0; i < state.containerList.length; i++) {
      _controller.add(PhotoViewController());
    }

    // var x = 0;

    return Stack(
      children: state.containerList.map<Widget>((val) {
        return XGestureDetector(
          onDoubleTap: (_) {
            if (editZoomController.innerPhotoview.value == 0) {
              editZoomController.innerPhotoview.value =
                  state.containerList.indexOf(val) + 1;
            } else {
              editZoomController.innerPhotoview.value = 0;
            }

            setState(() {
              val.rotation = 0.0;
              val.scale = 1.0;
              // val.xposition += 0.001;
              // val.yposition += 0.001;
            });
          },
          child: Stack(
            children: [
              GestureDetector(
                onScaleStart: (details) {
                  if (val == null) return;
                  _initPos = details.focalPoint;
                  _currentPos = Offset(val.xposition, val.yposition);
                  _currentScale = val.scale;
                  _currentRotation = val.rotation;
                },
                onScaleUpdate: (details) {
                  if (val == null) return;
                  final delta = details.focalPoint - _initPos;
                  final left = (delta.dx / screen.width) + _currentPos.dx;
                  final top = (delta.dy / screen.height) + _currentPos.dy;

                  setState(() {
                    val.xposition = Offset(left, top).dx;
                    val.yposition = Offset(left, top).dy;
                    val.rotation = details.rotation + _currentRotation;
                    val.scale = details.scale * _currentScale;
                  });
                },
                child: Stack(
                  children: [
                    editZoomController.innerPhotoview.value ==
                            state.containerList.indexOf(val) + 1
                        ? Positioned(
                            top: val.yposition * screen.height - 45,
                            left:
                                (val.xposition * screen.width + val.width) - 35,
                            child: IconButton(
                                icon: Icon(
                                  Icons.check,
                                  color: Colors.white,
                                  size: 20.0,
                                ),
                                onPressed: () {
                                  editZoomController.innerPhotoview.value = 0;
                                  setState(() {
                                    // val.xposition += 0.001;
                                    // val.yposition += 0.001;
                                  });
                                }),
                          )
                        : Container(),
                    editZoomController.innerPhotoview.value ==
                            state.containerList.indexOf(val) + 1
                        ? Positioned(
                            top: val.yposition * screen.height - 45,
                            left: val.xposition * screen.width - 10,
                            child: IconButton(
                                icon: Icon(
                                  Icons.delete_outline_sharp,
                                  color: Colors.white,
                                  size: 20.0,
                                ),
                                onPressed: () {
                                  editZoomController.innerPhotoview.value = 0;
                                  state.containerList.remove(val);
                                  setState(() {
                                    // val.xposition += 0.01;
                                    // val.yposition += 0.01;
                                  });
                                }),
                          )
                        : Container(),
                    Positioned(
                      left: val.xposition * screen.width,
                      top: val.yposition * screen.height,
                      child: Transform.scale(
                        scale: val.scale,
                        child: Transform.rotate(
                          angle: val.rotation,
                          child: Container(
                            height: val.height,
                            width: val.width,
                            child: FittedBox(
                              fit: BoxFit.fill,
                              child: Listener(
                                onPointerDown: (details) {
                                  // if (_inAction) return;
                                  // _inAction = true;
                                  // _activeItem = val;
                                  _initPos = details.position;
                                  _currentPos =
                                      Offset(val.xposition, val.yposition);
                                  _currentScale = val.scale;
                                  _currentRotation = val.rotation;
                                },
                                onPointerUp: (details) {
                                  // _inAction = false;
                                },
                                child: Container(
                                  height: val.height,
                                  width: val.width,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5.0),
                                      color: val.containerimage == null
                                          ? Color(0xffd2d5e6)
                                          : Colors.transparent,
                                      border:
                                          state.containerList.indexOf(val) == 1
                                              ? Border(
                                                  top: BorderSide(
                                                      color: Colors.white,
                                                      width: 7.5),
                                                  bottom: BorderSide(
                                                      color: Colors.white,
                                                      width: 7.5),
                                                  left: BorderSide(
                                                      color: Colors.white,
                                                      width: 7.5),
                                                  right: BorderSide(
                                                      color: Colors.white,
                                                      width: 7.5),
                                                )
                                              : val.containerimage == null
                                                  ? Border.all(
                                                      color: Colors.grey
                                                          .withOpacity(0.5),
                                                      width: 2.0)
                                                  : Border()),
                                  child: val.containerimage == null
                                      ? Center(
                                          child: GestureDetector(
                                            onTap: () async {
                                              final image = await ImagePicker()
                                                  .getImage(
                                                      source:
                                                          ImageSource.gallery);
                                              setState(() {
                                                if (image != null) {
                                                  val.containerimage =
                                                      File(image.path);
                                                }
                                                state.imageselected1 = true;
                                              });
                                              // getImage();
                                            },
                                            child: SvgPicture.asset(
                                                "assets/images/addpicture1.svg"),
                                          ),
                                        )
                                      : Container(
                                          child: ClipRect(
                                            child: PhotoView.customChild(
                                              enableRotation: true,
                                              scaleStateController:
                                                  scaleStateController[state
                                                      .containerList
                                                      .indexOf(val)],
                                              controller: controller[state
                                                  .containerList
                                                  .indexOf(val)],
                                              child: Image.file(
                                                val.containerimage,
                                                fit: BoxFit.scaleDown,
                                              ),
                                              minScale: 1.0,
                                              maxScale: 5.0,
                                              initialScale: 1.5,
                                              disableGestures:
                                                  editZoomController
                                                          .innerPhotoview
                                                          .value !=
                                                      state.containerList
                                                              .indexOf(val) +
                                                          1,
                                            ),
                                          ),
                                        ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        );
      }).toList(),
    );
  }

  Future getImage() async {
    final image = await ImagePicker().getImage(source: ImageSource.gallery);
    setState(() {
      if (image != null) {
        state.image1 = File(image.path);
      }
      state.imageselected1 = true;
    });
  }
}

/*
Container(
                          height: val.height,
                          width: val.width,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              color: val.containerimage == null
                                  ? Color(0xffd2d5e6)
                                  : Colors.transparent,
                              border: state.containerList.indexOf(val) == 1
                                  ? Border(
                                      top: BorderSide(
                                          color: Colors.white, width: 7.5),
                                      bottom: BorderSide(
                                          color: Colors.white, width: 7.5),
                                      left: BorderSide(
                                          color: Colors.white, width: 7.5),
                                      right: BorderSide(
                                          color: Colors.white, width: 7.5),
                                    )
                                  : val.containerimage == null
                                      ? Border.all(
                                          color: Colors.grey.withOpacity(0.5),
                                          width: 2.0)
                                      : Border()),
                          child: val.containerimage == null
                              ? Center(
                                  child: GestureDetector(
                                    onTap: () async {
                                      final image = await ImagePicker()
                                          .getImage(
                                              source: ImageSource.gallery);
                                      setState(() {
                                        if (image != null) {
                                          val.containerimage = File(image.path);
                                        }
                                        state.imageselected1 = true;
                                      });
                                      // getImage();
                                    },
                                    child: SvgPicture.asset(
                                        "assets/images/addpicture1.svg"),
                                  ),
                                )
                              : Container(
                                  child: ClipRect(
                                    child: PhotoView.customChild(
                                      enableRotation: true,
                                      scaleStateController:
                                          scaleStateController[
                                              state.containerList.indexOf(val)],
                                      controller: controller[
                                          state.containerList.indexOf(val)],
                                      child: Image.file(
                                        val.containerimage,
                                        fit: BoxFit.scaleDown,
                                      ),
                                      minScale: 1.0,
                                      maxScale: 5.0,
                                      initialScale: 1.2,
                                      disableGestures: editZoomController
                                              .innerPhotoview.value !=
                                          state.containerList.indexOf(val) + 1,
                                    ),
                                  ),
                                ),
                        ),
 */
