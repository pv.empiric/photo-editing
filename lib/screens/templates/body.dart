import 'dart:convert';
// import 'package:flutter/services.dart';
import 'package:animate_do/animate_do.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
import 'package:image_editing_flutter_app/components/lib/google_fonts.dart';
import 'package:image_editing_flutter_app/models/draft_template_model.dart';
import 'package:image_editing_flutter_app/models/drag_drop.dart';
import 'package:image_editing_flutter_app/models/saved_template_model.dart';
import 'package:image_editing_flutter_app/models/temp_rsponse.dart';
import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate0/CelebrationTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate1/CelebrationTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate2/CelebrationTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Celebration/CelebrationTemplate3/CelebrationTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate19/CoolTemplate19.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate20/CoolTemplate20.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate21/CoolTemplate21.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate5/CoolTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate4/FestivalTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate5/FestivalTemplate5.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/templates/Funural/Template0/FunuralTemplate0.dart';
// import 'package:image_editing_flutter_app/screens/templates/Funural/Template0/Template0.dart';
// ignore: unused_import
// import 'package:image_editing_flutter_app/screens/templates/Funural/Template1/photo_edit.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template1/FunuralTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template2/FunuralTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template3/FunuralTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template4/FunuralTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template5/FunuralTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template6/FunuralTemplate6.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template7/FunuralTemplate7.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template8/FunuralTemplate8.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate0/FestivalTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate1/FestivalTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate2/FestivalTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Festival/FestivalTemplate3/FestivalTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate0/NewTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate1/NewTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate2/NewTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate3/NewTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate4/NewTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate5/NewTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate6/NewTemplate6.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate7/NewTemplate7.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate8/NewTemplate8.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate9/NewTemplate9.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate10/NewTemplate10.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate11/NewTemplate11.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate12/NewTemplate12.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate13/NewTemplate13.dart';
import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate14/NewTemplate14.dart';
// import 'package:image_editing_flutter_app/screens/templates/New/NewTemplate15/NewTemplate15.dart';
// import 'package:image_editing_flutter_app/screens/templates/New/Template0/NewTemplate0.dart';
// import 'package:image_editing_flutter_app/screens/templates/New/emplate1/NewTemplate1.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate0/SimpleTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate1/SimpleTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate10/SimpleTemplate10.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate11/SimpleTemplate11.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate12/SimpleTemplate12.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate13/SimpleTemplate13.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate14/SimpleTemplate14.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate15/SimpleTemplate15.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate16/SimpleTemplate16.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate17/SimpleTemplate17.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate18/SimpleTemplate18.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate19/SimpleTemplate19.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate2/SimpleTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate3/SimpleTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate4/SimpleTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate5/SimpleTemplate5.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate6/SimpleTemplate6.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate7/SimpleTemplate7.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate8/SimpleTemplate8.dart';
import 'package:image_editing_flutter_app/screens/templates/Simple/SimpleTemplate9/SimpleTemplate9.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate0/CoolTemplate0.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate1/CoolTemplate1.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate2/CoolTemplate2.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate3/CoolTemplate3.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate4/CoolTemplate4.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate6/CoolTemplate6.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate7/CoolTemplate7.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate8/CoolTemplate8.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate9/CoolTemplate9.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate10/CoolTemplate10.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate11/CoolTemplate11.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate12/CoolTemplate12.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate13/CoolTemplate13.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate14/CoolTemplate14.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate15/CoolTemplate15.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate16/CoolTemplate16.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate17/CoolTemplate17.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate18/CoolTemplate18.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate22/CoolTemplate22.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate23/CoolTemplate23.dart';
import 'package:image_editing_flutter_app/screens/templates/Cool/CoolTemplate24/CoolTemplate24.dart';
import 'Funural/Template0/FunuralTemplate0.dart';
// import 'Funural/Template0/FunuralTemplate0.dart';

class Body extends StatefulWidget {
  // var response1;
  // Body({this.response1});
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  _BodyState({this.dataPotraid, this.response});
  DragDrop dataPotraid;
  DragDrop dataLandscape;
  bool checkOrientation;
  
  // var response; //readJson();
  var editjson;
  var apiResponse;
   var response;
   EditZoomController eC = Get.put(EditZoomController());
  readJson() async {
    if(eC.respomse_json.value.length == 0){
    // http.Response data = await http.get('https://jsonkeeper.com/b/T97A');
    var data = await Dio().get('https://lehengascholi.com/api/getappdata',
//     options: Options(
//       headers: {
//   "Access-Control-Allow-Origin": "*", // Required for CORS support to work
//   "Access-Control-Allow-Credentials": true, // Required for cookies, authorization headers with HTTPS
//   "Access-Control-Allow-Headers": "Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,locale",
//   "Access-Control-Allow-Methods": "POST, OPTIONS"
// },
    // )
    );
    print(data.statusCode);
    // SharedPreferences preferences = await SharedPreferences.getInstance();
    // final data = await rootBundle.loadString('assets/data.json');
    if (data.statusCode == 200) {
      setState(() {
        response = data.data;
        eC.respomse_json.value = jsonEncode(data.data);
      });
    }
    }
    else{
      response = jsonDecode(eC.respomse_json.value);
    }

    // http.Response data1 = await http.get("http://165.227.248.244:3333/main/");
    // if (data1.statusCode == 200) {
    //   setState(() {
    //     apiResponse = json.decode(data1.body);
    //   });
    // }
  }

  // tempJson() async{
  //   if(eC.tempResponse.length == 0){
  //   var data = await http.get('https://lehengascholi.com/api/productlist?page=1');
  //   if(data.statusCode == 200){
  //     setState(() {
  //       eC.tempResponse.add(TempResponse.fromJson(jsonDecode(data.body)));
  //       print(eC.tempResponse);
  //     });
  //   }
  // }
  // }

  @override
  void initState() {
    // readJson();
    // response = widget.response1;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      checkOrientation = true;
    } else {
      checkOrientation = false;
    }
//if (checkOrientation == true) {
    // if (response != null)
      dataPotraid = DragDrop(
        screenwidth: MediaQuery.of(context).size.width,
        screenheight: MediaQuery.of(context).size.height - 80,
        width: MediaQuery.of(context).size.width -
            2 * MediaQuery.of(context).size.width / 60,
        height: (MediaQuery.of(context).size.width -
                    2 * MediaQuery.of(context).size.width / 60) 
                    / 0.6363 - MediaQuery.of(context).size.height / 60
            //         /
            //     double.parse(response["Templates"]["Main-Container"][0]
            //         ["Aspect-ratio"]) -
            // MediaQuery.of(context).size.height / 60,
      );

    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Container(
        child: Padding(
          padding: EdgeInsets.only(left: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                  "Tamplates",
                  style: GoogleFonts.abrilFatface(fontSize: 25.0,color: Colors.grey[800]),
                ),
              ),
              TamplatesCategories(
                tamplateCategory: "Fashion",
                categoryCount: 9,
                dataPotraid: dataPotraid,
                dataLandscape: dataLandscape,
                checkOrientation: checkOrientation,
                response: response,
                editjson: editjson,
              ),

              TamplatesCategories(
                tamplateCategory: "Celebration",
                categoryCount: 4,
                dataPotraid: dataPotraid,
                response: response,
              ),
              TamplatesCategories(
                tamplateCategory: "Festival",
                categoryCount: 6,
                dataPotraid: dataPotraid,
                response: response,
              ),
              TamplatesCategories(
                tamplateCategory: "Simple",
                categoryCount: 20,
                dataPotraid: dataPotraid,
                response: response,
              ),
              TamplatesCategories(
                tamplateCategory: "Cool",
                categoryCount: 25,
                dataPotraid: dataPotraid,
                response: response,
              ),
              TamplatesCategories(
                tamplateCategory: "New",
                categoryCount: 15,
                dataPotraid: dataPotraid,
                response: response,
              ),
              // TamplatesCategories(tamplateCategory: "Blank", categoryCount: 2),
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class TamplatesCategories extends StatelessWidget {
  final DragDrop dataPotraid;
  final DragDrop dataLandscape;
  final bool checkOrientation;
  final String tamplateCategory;
  final int categoryCount;
  var response;
  JsonCode editjson;
  DraftJsonCode draftJsonCode;
  var draftId;
  var editId;

  TamplatesCategories({
    this.tamplateCategory,
    this.categoryCount,
    Key key,
    this.response,
    this.dataPotraid,
    this.dataLandscape,
    this.checkOrientation,
    this.editjson,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    TextStyle tamplateCategoryHeading = TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.w600,
      // fontWeight: FontWeight.bold
    );
    // print(Categories[0].CategoryName);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        FadeInLeftBig(
          child: Text(
            tamplateCategory,
            style: GoogleFonts.rye(
              fontSize: 18,
              // fontWeight: FontWeight.w600,
            ),
          ),
        ),
        Container(
          height: 200.0,
          child: ListView.builder(
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: categoryCount,
            itemBuilder: (context, index) {
              return AbsorbPointer(
                absorbing: response == null ? false : false,
                child: InkWell(
                    onTap: () {
                      Navigator.push(context,
                          // ignore: missing_return
                          MaterialPageRoute(builder: (context) {
                        if (tamplateCategory.toString() == "Fashion") {
                          if (index == 0) {
                            return FunuralTemplate0(
                              response: response,
                              editjson: editjson,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 1) {
                            return FunuralTemplate1(
                              response: response,
                              editjson: editjson,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              // dataPotraid: dataPotraid,
                              // response: response,
                            );
                          }
                          if (index == 2) {
                            return FunuralTemplate2(
                              //   dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 3) {
                            return FunuralTemplate3(
                              // dataPotraid: dataPotraid,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              response: response,
                            );
                          }
                          if (index == 4) {
                            return FunuralTemplate4(
                              // dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 5) {
                            return FunuralTemplate5(
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),

                              // dataPotraid: dataPotraid,
                              response: response,
                            );
                          }
                          if (index == 6) {
                            return FunuralTemplate6(
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              // dataPotraid: dataPotraid,
                              response: response,
                            );
                          }
                          if (index == 7) {
                            return FunuralTemplate7(
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              // dataPotraid: dataPotraid,
                              response: response,
                            );
                          }
                          if (index == 8) {
                            return FunuralTemplate8(
                              // dataPotraid: dataPotraid,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              response: response,
                            );
                          }
                        }
                        if (tamplateCategory.toString() == "Festival") {
                          if (index == 0) {
                            return FestivalTemplate0(
                              dataPotraid: dataPotraid,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              response: response,
                            );
                          }
                          if (index == 1) {
                            return FestivalTemplate1(
                              dataPotraid: dataPotraid,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              response: response,
                            );
                          }
                          if (index == 2) {
                            return FestivalTemplate2(
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              dataPotraid: dataPotraid,
                              response: response,
                            );
                          }
                          if (index == 3) {
                            return FestivalTemplate3(
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              dataPotraid: dataPotraid,
                              response: response,
                            );
                          }
                          if (index == 4) {
                            return FestivalTemplate4(
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              dataPotraid: dataPotraid,
                              response: response,
                            );
                          }
                          if (index == 5) {
                            return FestivalTemplate5(
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              dataPotraid: dataPotraid,
                              response: response,
                            );
                          }
                        }

                        if (tamplateCategory.toString() == "Celebration") {
                          if (index == 0) {
                            return CelebrationTemplate0(
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              dataPotraid: dataPotraid,
                              response: response,
                            );
                          }
                          if (index == 1) {
                            return CelebrationTemplate1(
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              dataPotraid: dataPotraid,
                              response: response,
                            );
                          }
                          if (index == 2) {
                            return CelebrationTemplate2(
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              dataPotraid: dataPotraid,
                              response: response,
                            );
                          }
                          if (index == 3) {
                            return CelebrationTemplate3(
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                              dataPotraid: dataPotraid,
                              response: response,
                            );
                          }
                          return Container(
                            child: Center(
                              child: Text("Please Go Back!!"),
                            ),
                          );
                        }
                        if (tamplateCategory.toString() == "Simple") {
                          if (index == 0) {
                            return SimpleTemplate0(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 1) {
                            return SimpleTemplate1(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 2) {
                            return SimpleTemplate2(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 3) {
                            return SimpleTemplate3(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 4) {
                            return SimpleTemplate4(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 5) {
                            return SimpleTemplate5(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 6) {
                            return SimpleTemplate6(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 7) {
                            return SimpleTemplate7(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 8) {
                            return SimpleTemplate8(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 9) {
                            return SimpleTemplate9(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 10) {
                            return SimpleTemplate10(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 11) {
                            return SimpleTemplate11(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 12) {
                            return SimpleTemplate12(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 13) {
                            return SimpleTemplate13(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 14) {
                            return SimpleTemplate14(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 15) {
                            return SimpleTemplate15(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 16) {
                            return SimpleTemplate16(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 17) {
                            return SimpleTemplate17(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 18) {
                            return SimpleTemplate18(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 19) {
                            return SimpleTemplate19(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          return Container(
                            child: Center(
                              child: Text("Go Back!"),
                            ),
                          );
                        }
                        if (tamplateCategory.toString() == "Cool") {
                          if (index == 0) {
                            return CoolTemplate0(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 1) {
                            return CoolTemplate1(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 2) {
                            return CoolTemplate2(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 3) {
                            return CoolTemplate3(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 4) {
                            return CoolTemplate4(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 5) {
                            return CoolTemplate5(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 6) {
                            return CoolTemplate6(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 7) {
                            return CoolTemplate7(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 8) {
                            return CoolTemplate8(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 9) {
                            return CoolTemplate9(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 10) {
                            return CoolTemplate10(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 11) {
                            return CoolTemplate11(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 12) {
                            return CoolTemplate12(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 13) {
                            return CoolTemplate13(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 14) {
                            return CoolTemplate14(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 15) {
                            return CoolTemplate15(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 16) {
                            return CoolTemplate16(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 17) {
                            return CoolTemplate17(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 18) {
                            return CoolTemplate18(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 19) {
                            return CoolTemplate19(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 20) {
                            return CoolTemplate20(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 21) {
                            return CoolTemplate21(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 22) {
                            return CoolTemplate22(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 23) {
                            return CoolTemplate23(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 24) {
                            return CoolTemplate24(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                        }
                        if (tamplateCategory.toString() == "New") {
                          if (index == 0) {
                            return NewTemplate0(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 1) {
                            return NewTemplate1(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 2) {
                            return NewTemplate2(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 3) {
                            return NewTemplate3(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 4) {
                            return NewTemplate4(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 5) {
                            return NewTemplate5(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 6) {
                            return NewTemplate6(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 7) {
                            return NewTemplate7(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 8) {
                            return NewTemplate8(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 9) {
                            return NewTemplate9(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 10) {
                            return NewTemplate10(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 11) {
                            return NewTemplate11(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 12) {
                            return NewTemplate12(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 13) {
                            return NewTemplate13(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                          if (index == 14) {
                            return NewTemplate14(
                              dataPotraid: dataPotraid,
                              response: response,
                              width: dataPotraid.screenwidth -
                                  2 * (dataPotraid.screenwidth / 60),
                            );
                          }
                           
                        }
                      }));
                    },
                    child: ZoomIn(
                      duration: Duration(milliseconds: 500),
                      child: Padding(
                        padding:
                            EdgeInsets.only(right: 5.0, top: 5.0, bottom: 5.0),
                        // child: Card(
                        //   elevation: 5.0,
                        //   child: Container(
                        //       width: 100,
                        //       decoration: BoxDecoration(
                        //         color: Colors.red,
                        //         borderRadius: BorderRadius.circular(5.0),
                        //       ),
                        //       child: Center(
                        //         child: Text("Hello"),
                        //       )),
                        // ),
                        child: Card(
                          elevation: 5.0,
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(
                                    color: Colors.white, width: 2.0)),
                            child: Image.asset(
                                "assets/Thumbimage/${tamplateCategory.toString()}/$index.PNG"),
                          ),
                        ),
                      ),
                    )),
              );
            },
          ),
        )
      ],
    );
  }
}

//checkOrientation: true,
// width: MediaQuery.of(context).size.width -
//     2 * MediaQuery.of(context).size.width / 60,
// height: (MediaQuery.of(context).size.width -
//             2 * MediaQuery.of(context).size.width / 60) /
//         double.parse(response["Templates"]["Main-Container"][0]
//             ["Aspect-ratio"]) -
//     MediaQuery.of(context).size.height / 60,
// left1: (MediaQuery.of(context).size.width -
//         2 * MediaQuery.of(context).size.width / 60) *
//     double.parse(
//         response["Templates"]["Funaral"][0]["Image"][0]["left"]),
// top1: ((MediaQuery.of(context).size.width -
//                 2 * MediaQuery.of(context).size.width / 60) /
//             double.parse(response["Templates"]["Main-Container"][0]
//                 ["Aspect-ratio"]) -
//         MediaQuery.of(context).size.height / 60) *
//     double.parse(
//         response["Templates"]["Funaral"][0]["Image"][0]["top"]),
// screenwidth: MediaQuery.of(context).size.width,
// screenheight: MediaQuery.of(context).size.height - 80,
// left2: (MediaQuery.of(context).size.width -
//         2 * MediaQuery.of(context).size.width / 60) *
//     double.parse(
//         response["Templates"]["Funaral"][0]["Image"][1]["left"]),
// top2: ((MediaQuery.of(context).size.width -
//                 2 * MediaQuery.of(context).size.width / 60) /
//             double.parse(response["Templates"]["Funaral"][0]
//                 ["Main-Container"][0]["Aspect-ratio"]) -
//         MediaQuery.of(context).size.height / 60) *
//     double.parse(
//         response["Templates"]["Funaral"][0]["Image"][1]["top"]),
// position1: Offset(
//     (MediaQuery.of(context).size.width -
//             2 * MediaQuery.of(context).size.width / 60) *
//         double.parse(
//             response["Templates"]["Funaral"][0]["Image"][0]["left"]),
//     ((MediaQuery.of(context).size.width -
//                     2 * MediaQuery.of(context).size.width / 60) /
//                 double.parse(response["Templates"]["Main-Container"][0]
//                     ["Aspect-ratio"]) -
//             MediaQuery.of(context).size.height / 60) *
//         double.parse(
//             response["Templates"]["Funaral"][0]["Image"][0]["top"])),
// position2: Offset(
//     (MediaQuery.of(context).size.width -
//             2 * MediaQuery.of(context).size.width / 60) *
//         double.parse(
//             response["Templates"]["Funaral"][0]["Image"][1]["left"]),
//     ((MediaQuery.of(context).size.width -
//                     2 * MediaQuery.of(context).size.width / 60) /
//                 double.parse(response["Templates"]["Funaral"][0]
//                     ["Main-Container"][0]["Aspect-ratio"]) -
//             MediaQuery.of(context).size.height / 60) *
//         double.parse(
//             response["Templates"]["Funaral"][0]["Image"][1]["top"])),

//   dataLandscape = DragDrop(
//     height: MediaQuery.of(context).size.width -80 - 2*MediaQuery.of(context).size.width/30,
//     width: (MediaQuery.of(context).size.width - 80 - 2*MediaQuery.of(context).size.width/30)*9/11 - MediaQuery.of(context).size.height/30,
//     left1: ((MediaQuery.of(context).size.width - 80 - 2*MediaQuery.of(context).size.width/30)*9/11 - MediaQuery.of(context).size.height/30)*0.125,
//     top1: (MediaQuery.of(context).size.width - 80 - 2*MediaQuery.of(context).size.width/30)*0.125,
//     left2: ((MediaQuery.of(context).size.width - 80 - 2*MediaQuery.of(context).size.width/30)*9/11 - MediaQuery.of(context).size.height/30)*0.375,
//     top2: (MediaQuery.of(context).size.width - 80 -2* MediaQuery.of(context).size.width/30)*0.375,
//     position1: Offset(((MediaQuery.of(context).size.width - 80 -2* MediaQuery.of(context).size.width/30)*9/11 - MediaQuery.of(context).size.height/30)*0.125,(MediaQuery.of(context).size.width - 80 - MediaQuery.of(context).size.width/30)*0.125),
//     position2: Offset(((MediaQuery.of(context).size.width - 80 - 2*MediaQuery.of(context).size.width/30)*9/11 - MediaQuery.of(context).size.height/30)*0.375,(MediaQuery.of(context).size.width - 80 - MediaQuery.of(context).size.width/30)*0.375),
//   );
// }
//  if (checkOrientation == false) {
//   dataLandscape = DragDrop(
//       //  checkOrientation: false,
//         height: MediaQuery.of(context).size.height - 2 * (MediaQuery.of(context).size.height) / 30 - 80,
//         width:( MediaQuery.of(context).size.height - 2 * (MediaQuery.of(context).size.height) / 30 - 80)* 9 / 11 - MediaQuery.of(context).size.width / 30,
//         left1: (( MediaQuery.of(context).size.height - 2 * (MediaQuery.of(context).size.height) / 30 - 80)* 9 / 11 - MediaQuery.of(context).size.width / 30) * 0.125,
//         top1:  (MediaQuery.of(context).size.height - 2 * (MediaQuery.of(context).size.height) / 30 - 80) * 0.125,
//         left2: (( MediaQuery.of(context).size.height - 2 * (MediaQuery.of(context).size.height) / 30 - 80)* 9 / 11 - MediaQuery.of(context).size.width / 30)*0.375,
//         top2:  (MediaQuery.of(context).size.height - 2 * (MediaQuery.of(context).size.height) / 30 - 80) * 0.375,
//         position1: Offset((( MediaQuery.of(context).size.height - 2 * (MediaQuery.of(context).size.height) / 30 - 80)* 9 / 11 - MediaQuery.of(context).size.width / 30) * 0.125,(MediaQuery.of(context).size.height - 2 * (MediaQuery.of(context).size.height) / 30 - 80) * 0.125,),
//         position2: Offset((( MediaQuery.of(context).size.height - 2 * (MediaQuery.of(context).size.height) / 30 - 80)* 9 / 11 - MediaQuery.of(context).size.width / 30)*0.375,(MediaQuery.of(context).size.height - 2 * (MediaQuery.of(context).size.height) / 30 - 80) * 0.375),
//       );

//   dataPotraid = DragDrop(
//     width: MediaQuery.of(context).size.height - 2*MediaQuery.of(context).size.height/30,
//     height: (MediaQuery.of(context).size.height - 2*MediaQuery.of(context).size.height/30)*11/9 -  MediaQuery.of(context).size.width/30,
//     left1: (MediaQuery.of(context).size.height - 2*MediaQuery.of(context).size.height/30)*0.125,
//     top1: (((MediaQuery.of(context).size.height - 2*MediaQuery.of(context).size.height/30)*11/9  -  MediaQuery.of(context).size.width/30))*0.125,
//     left2: (MediaQuery.of(context).size.height - 2*MediaQuery.of(context).size.height/30)*0.375,
//     top2:  (((MediaQuery.of(context).size.height - 2*MediaQuery.of(context).size.height/30)*11/9 - MediaQuery.of(context).size.width/30))*0.375,
//     position1: Offset((MediaQuery.of(context).size.height - 2*MediaQuery.of(context).size.height/30)*0.125,(((MediaQuery.of(context).size.height - 2*MediaQuery.of(context).size.height/30)*11/9 - 80 -  MediaQuery.of(context).size.width/30))*0.125),
//     position2: Offset((MediaQuery.of(context).size.height - 2*MediaQuery.of(context).size.height/30)*0.375,(((MediaQuery.of(context).size.height - 2*MediaQuery.of(context).size.height/30)*11/9 - 80 -  MediaQuery.of(context).size.width/30))*0.375),
//   );
//}
