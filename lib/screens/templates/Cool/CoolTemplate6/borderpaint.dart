import 'package:flutter/material.dart';

class RPSCustomPainter extends CustomPainter{
  
  @override
  void paint(Canvas canvas, Size size) {
    
    

  Paint paint_0 = new Paint()
      ..color = Color.fromARGB(255, 62, 60, 60)
      ..style = PaintingStyle.stroke
      ..strokeWidth = 15.880000114440918;
     
         
    Path path_0 = Path();
    path_0.moveTo(size.width*0.0122500,size.height*0.0424286);
    path_0.lineTo(size.width*0.0134000,size.height*0.9884000);
    path_0.lineTo(size.width*0.9817500,size.height*0.9901143);
    path_0.lineTo(size.width*0.9827250,size.height*0.0438571);
    path_0.lineTo(size.width*0.0122500,size.height*0.0424286);
    path_0.close();

    canvas.drawPath(path_0, paint_0);
  

  Paint paint_1 = new Paint()
      ..color = Color.fromARGB(255, 32, 31, 31)
      ..style = PaintingStyle.stroke
      ..strokeWidth = 25.0;
     
         
    Path path_1 = Path();
    path_1.moveTo(size.width*-0.0122500,size.height*0.0166000);
    path_1.lineTo(size.width*1.0104500,size.height*0.0168286);

    canvas.drawPath(path_1, paint_1);
  

  Paint paint_2 = new Paint()
      ..color = Color.fromARGB(255, 196, 196, 196)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1.0;
     
         
    Path path_2 = Path();
    path_2.moveTo(size.width*0.0463500,size.height*0.0075857);
    path_2.cubicTo(size.width*0.0588500,size.height*0.0075857,size.width*0.0624750,size.height*0.0096571,size.width*0.0624750,size.height*0.0168000);
    path_2.cubicTo(size.width*0.0624750,size.height*0.0239286,size.width*0.0588500,size.height*0.0260000,size.width*0.0463500,size.height*0.0260000);
    path_2.cubicTo(size.width*0.0338750,size.height*0.0260000,size.width*0.0302500,size.height*0.0239286,size.width*0.0302500,size.height*0.0168000);
    path_2.cubicTo(size.width*0.0302500,size.height*0.0096571,size.width*0.0338750,size.height*0.0075857,size.width*0.0463500,size.height*0.0075857);
    path_2.close();

    canvas.drawPath(path_2, paint_2);
  

  Paint paint_3 = new Paint()
      ..color = Color.fromARGB(255, 196, 196, 196)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1.0;
     
         
    Path path_3 = Path();
    path_3.moveTo(size.width*0.0955750,size.height*0.0081857);
    path_3.cubicTo(size.width*0.1085000,size.height*0.0081857,size.width*0.1117000,size.height*0.0102571,size.width*0.1117000,size.height*0.0174000);
    path_3.cubicTo(size.width*0.1117000,size.height*0.0245286,size.width*0.1080750,size.height*0.0266000,size.width*0.0955750,size.height*0.0266000);
    path_3.cubicTo(size.width*0.0831000,size.height*0.0266000,size.width*0.0794750,size.height*0.0245286,size.width*0.0794750,size.height*0.0174000);
    path_3.cubicTo(size.width*0.0794750,size.height*0.0102571,size.width*0.0835250,size.height*0.0081857,size.width*0.0955750,size.height*0.0081857);
    path_3.close();

    canvas.drawPath(path_3, paint_3);
  

  Paint paint_4 = new Paint()
      ..color = Color.fromARGB(255, 196, 196, 196)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1.0;
     
         
    Path path_4 = Path();
    path_4.moveTo(size.width*0.1471250,size.height*0.0084857);
    path_4.cubicTo(size.width*0.1596750,size.height*0.0084857,size.width*0.1628750,size.height*0.0103429,size.width*0.1628750,size.height*0.0174857);
    path_4.cubicTo(size.width*0.1628750,size.height*0.0246143,size.width*0.1596250,size.height*0.0264714,size.width*0.1471250,size.height*0.0264714);
    path_4.cubicTo(size.width*0.1346500,size.height*0.0264714,size.width*0.1314000,size.height*0.0246143,size.width*0.1314000,size.height*0.0174857);
    path_4.cubicTo(size.width*0.1314000,size.height*0.0103429,size.width*0.1350750,size.height*0.0084857,size.width*0.1471250,size.height*0.0084857);
    path_4.close();

    canvas.drawPath(path_4, paint_4);
  
    
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
  
}