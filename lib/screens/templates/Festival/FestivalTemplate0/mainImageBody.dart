import 'dart:io';
import 'package:flutter/material.dart';
import 'package:gesture_x_detector/gesture_x_detector.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
import 'package:image_editing_flutter_app/models/drag_drop.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/widgets.dart';
import 'package:photo_view/photo_view.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/text_list.dart';
// ignore: unused_import

import 'package:image_editing_flutter_app/screens/Custom_Constants/stickerview.dart';
import './template0containerView.dart';

// import 'package:image_editing_flutter_app/screens/Custom_Constants/containerView.dart';
// ignore: must_be_immutable
class MainImageEditingBody extends StatefulWidget {
  final DragDrop dataPotraid;
  var state;
  File image1;
  File image2;
  bool imageselected1;
  bool imageselected2;
  String finalsticker;
  List finalstickerList;
  List<String> finalstickerList2;
  var editJson;
  Offset temp = Offset(150, 200);
  MainImageEditingBody({
    this.dataPotraid,
    this.image1,
    this.image2,
    this.finalsticker,
    this.finalstickerList,
    this.imageselected1,
    this.imageselected2,
    this.state,
    this.editJson,
    this.finalstickerList2,
  });
  @override
  _MainImageEditingBodyState createState() => _MainImageEditingBodyState();
}

class _MainImageEditingBodyState extends State<MainImageEditingBody> {
  // DragDrop data;
  var state;
  PhotoViewController photoViewController1;
  PhotoViewController photoViewController2;
  Offset a;
  @override
  void initState() {
    // data = widget.dataPotraid;
    state = widget.state;
    photoViewController1 = PhotoViewController();
    photoViewController2 = PhotoViewController();
    a = Offset(state.datawidth * 0.0 - state.datawidth * 0.1, state.dataheight * 0.70);
    super.initState();
  }

  EditZoomController eC = Get.put(EditZoomController());

  Widget build(BuildContext context) {
    return Stack(
      children: [
        RepaintBoundary(
          key: state.myglobalKey,
          child: Container(
              margin: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height /
               ( !eC.isEditTemplate.value?
                    60 : double.parse(widget.editJson.mainContainer.marginVerticle)),
                horizontal: MediaQuery.of(context).size.width /
               ( !eC.isEditTemplate.value?
                    60 : double.parse(widget.editJson.mainContainer.marginHorizontal)),
              ),
              // child: state.imageselected1 &&
              //         state.imageselected2 &&
              //         state.image1 != null &&
              //         state.image2 != null
              child: Container(
                // child: RepaintBoundary(
                //   key: state.myglobalKey,
                child: Stack(
                  // ignore: deprecated_member_use
                  overflow: Overflow.clip,
                  children: [
                    // if (state.image1 != null &&
                    //     state.image2 != null)
                    AspectRatio(
                      aspectRatio: state.dataAspectRatio,
                      child: XGestureDetector(
                        onTap: (_) {
                          setState(() {
                            // // state.isTextediting = false;
                            // state.isOpenTextbar = false;
                            // state.isOpenWidget = false;
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(
                                color: Colors.grey.withOpacity(0.5),
                                width: 2.0),
                          ),
                          child: Stack(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.circular(10.0)
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10.0),
                                                                  child: GetFancyShimmerImage(
                                    url: state.background_image,
                                    boxFit: BoxFit.cover,
                                    width: double.infinity
                                  ),
                                ),
                              ),
                              Stack(
                            children: [
                              //

                              
                                Positioned(
                                  top: 0.0,
                                  left: 0.0,
                                  child: GetFancyShimmerImage(
                                    url: "https://images2.imgbox.com/a6/c2/2jJa10Bl_o.png",
                                    height: 150,
                                    width: state.datawidth - widget.dataPotraid.screenwidth / 30
                                  ),
                                  // child: Image.network("https://images2.imgbox.com/a6/c2/2jJa10Bl_o.png", height: 150,width: state.datawidth - widget.dataPotraid.screenwidth / 30,),
                                ),
                                if (state.containerList.length != 0)
                                ContainerView(
                                  state: state,
                                  //  containerlist: state.containerList,
                                ),
                              Positioned(
                                bottom: 0,
                                right: 0,
                                child: GetFancyShimmerImage(
                                  url: "https://images2.imgbox.com/b4/9b/EAWF1TK9_o.png",
                                  height: 150,
                                  width: 200,
                                ),
                                // child: Image.network("https://images2.imgbox.com/b4/9b/EAWF1TK9_o.png", height: 150,width: 200,),
                              ),  
                              Positioned(
                                top: a.dy,
                                left: a.dx,
                                bottom: state.datawidth * 0.1,
                                child: XGestureDetector(
                                  onMoveUpdate: (detail) {
                                    setState(() {
                                      a += detail.delta;
                                    });
                                  },
                                  child: Container(
                                    width: state.datawidth * 0.75,
                                    child: GetFancyShimmerImage(
                                      url: "https://images2.imgbox.com/f7/a3/PKdjWSV4_o.png",
                                      boxFit: BoxFit.fitWidth
                                    )
                                  ),
                                ),
                              ),



                              if (state.stickerList.length != 0)
                                StickerView(
                                  stickerList: state.stickerList,
                                  state: state,
                                  isImageEditing: state.isImageEditing,
                                ),

                              if (state.textList.length != 0)
                                TextList(
                                  textList: state.textList,
                                  state: state,
                                  isImageEditing: state.isImageEditing,
                                  isTextEditing: state.isTextediting,
                                  fontStyleList: state.fontStyleList,
                                  finalTextStyleIndex: state.textStyleIndex,
                                ),

                              // if(state.textList.length != 0)
                              //     TextView(
                              //       isImageEditing: state.isImageEditing,
                              //       template0state: state,
                              //       textlist: state.textList,
                              //     )
                            ],
                          ),]),
                        ),
                      ),
                    )
                    // else
                    //   MyBody(
                    //     template0state: state,
                    //     dataPotraid: data,
                    //   )
                  ],
                ),
                //    ),
              )
              // : MyBody(
              //     template0state: state,
              //     dataPotraid: data,
              //   ),
              ),
        )
      ],
    );
  }

// ignore: non_constant_identifier_names
  StreamBuilder<PhotoViewControllerValue> Data() {
    return StreamBuilder(
      stream: photoViewController1.outputStateStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Container(
            child: Text("dsfhjlsahdlkjshfkjshafhajksfdah"),
          );
        return Center(
          child: Column(
            children: [
              Text("${snapshot.data.scale}"),
              Text(" ${snapshot.data.rotation}"),
              Text("${snapshot.data.position}")
            ],
          ),
        );
      },
    );
  }

  // ignore: non_constant_identifier_names
  StreamBuilder<PhotoViewControllerValue> Data1() {
    return StreamBuilder(
      stream: photoViewController1.outputStateStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Container();
        }
        print(snapshot.data.scale);
        print(snapshot.data.rotation);
        print(snapshot.data.position);
        return SizedBox();
      },
    );
  }

  // ignore: non_constant_identifier_names
  StreamBuilder<PhotoViewControllerValue> Data2() {
    return StreamBuilder(
      stream: photoViewController2.outputStateStream,
      // ignore: missing_return
      builder: (context, snapshot) {
        if (!snapshot.hasData) return Container();
        print(snapshot.data.scale);
        print(snapshot.data.rotation);
        print(snapshot.data.position);
      },
    );
  }
}
