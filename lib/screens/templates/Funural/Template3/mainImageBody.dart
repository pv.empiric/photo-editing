import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:gesture_x_detector/gesture_x_detector.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
import 'package:image_editing_flutter_app/models/drag_drop.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/models/saved_template_model.dart';
import 'package:photo_view/photo_view.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/text_list.dart';

import 'package:image_editing_flutter_app/screens/Custom_Constants/stickerview.dart';
import 'template3containerView.dart';

// import 'package:image_editing_flutter_app/screens/Custom_Constants/containerView.dart';
// ignore: must_be_immutable
class MainImageEditingBody extends StatefulWidget {
  DragDrop dataPotraid;
  var state;
  File image1;
  File image2;
  bool imageselected1;
  bool imageselected2;
  String finalsticker;
  List finalstickerList;
  List<String> finalstickerList2;
  var editJson;
  Offset temp = Offset(150, 200);
  MainImageEditingBody({
    this.dataPotraid,
    this.image1,
    this.image2,
    this.finalsticker,
    this.finalstickerList,
    this.imageselected1,
    this.imageselected2,
    this.state,
    this.editJson,
    this.finalstickerList2,
  });
  @override
  _MainImageEditingBodyState createState() => _MainImageEditingBodyState();
}

class _MainImageEditingBodyState extends State<MainImageEditingBody> {
  DragDrop data;
  var state;
  PhotoViewController photoViewController1;
  PhotoViewController photoViewController2;

  @override
  void initState() {
    // data = widget.dataPotraid;
    state = widget.state;
    photoViewController1 = PhotoViewController();
    photoViewController2 = PhotoViewController();
    super.initState();
  }

  EditZoomController eC = Get.put(EditZoomController());

  Widget build(BuildContext context) {
    return Stack(
      children: [
        RepaintBoundary(
          key: state.myglobalKey,
          child: Container(
              margin: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height /
               ( !eC.isEditTemplate.value?
                    60 : double.parse(widget.editJson.mainContainer.marginVerticle)),
                horizontal: MediaQuery.of(context).size.width /
               ( !eC.isEditTemplate.value?
                    60 : double.parse(widget.editJson.mainContainer.marginHorizontal)),
              ),
              child: Container(
                // child: RepaintBoundary(
                //   key: state.myglobalKey,
                child: Stack(
                  // ignore: deprecated_member_use
                  overflow: Overflow.clip,
                  children: [
                    // if (state.image1 != null &&
                    //     state.image2 != null)
                    AspectRatio(
                      aspectRatio: state.dataAspectRatio,
                      child: XGestureDetector(
                        onTap: (_) {
                          setState(() {
                            // state.isTextediting = false;
                            // state.isOpenTextbar = false;
                            // state.isOpenWidget = false;
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(
                                color: Colors.grey.withOpacity(0.5),
                                width: 2.0),
                            //  image: FileImage(child: Image.file(state.background_image));
                            // image: DecorationImage(
                            //   image: FileImage(state.background_image),
                            // )
                          ),
                          child: Stack(
                            children: [
                              // if (state.containerList[0].containerimage != null)
                                Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      
                                      image: state.background_image.toString().contains('File:') ? FileImage(state.background_image) : NetworkImage(state.background_image),
                                      
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  child: BackdropFilter(
                                    filter:
                                        ImageFilter.blur(sigmaX: 3, sigmaY: 3),
                                    child: Container(
                                      color: Colors.white.withOpacity(0.1),
                                    ),
                                  ),
                                ),

                              if (state.containerList.length != 0)
                                ContainerView(
                                  state: state,
                                  // containerList: state.containerList,
                                  //  containerlist: state.containerList,
                                ),
                              if (state.stickerList.length != 0)
                                StickerView(
                                  stickerList: state.stickerList,
                                  state: state,
                                  isImageEditing: state.isImageEditing,
                                ),

                              if (state.textList.length != 0)
                                TextList(
                                  textList: state.textList,
                                  state: state,
                                  isImageEditing: state.isImageEditing,
                                  isTextEditing: state.isTextediting,
                                  fontStyleList: state.fontStyleList,
                                  finalTextStyleIndex: state.textStyleIndex,
                                ),

                              // if(state.textList.length != 0)
                              //     TextView(
                              //       isImageEditing: state.isImageEditing,
                              //       template3state: state,
                              //       textlist: state.textList,
                              //     )
                            ],
                          ),
                        ),
                      ),
                    )
                    // else
                    //   MyBody(
                    //     template3state: state,
                    //     dataPotraid: data,
                    //   )
                  ],
                ),
                //    ),
              )
              // : MyBody(
              //     template3state: state,
              //     dataPotraid: data,
              //   ),
              ),
        )
      ],
    );
  }
}
