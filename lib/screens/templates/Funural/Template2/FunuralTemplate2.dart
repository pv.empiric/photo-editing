import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_editing_flutter_app/models/ContainerList.dart';
import 'package:image_editing_flutter_app/models/draft_template_model.dart';
// import 'package:image_editing_flutter_app/models/drag_drop.dart';
import 'package:image_editing_flutter_app/models/radio_model.dart';
import 'package:image_editing_flutter_app/models/saved_template_model.dart';
import 'package:image_editing_flutter_app/models/sticker_list_model.dart';
import 'package:image_editing_flutter_app/models/text_list_model.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/Appbar.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/show_final_image.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/screens/Custom_Constants/widgets.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/EditText.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/bottom_nav_bar_item.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/color_picker.dart';
// import 'package:image_editing_flutter_app/screens/Custom_Constants/like_refresh.dart';
// import 'package:image_editing_flutter_app/screens/Custom_Constants/radio_item.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/text_align.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/text_strock.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/text_style_list.dart';
import 'package:image_editing_flutter_app/screens/collection/components/radio_item.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template2/mainImageBody.dart';
import 'package:fluttertoast/fluttertoast.dart';
// import 'mainImageBody.dart';
import '../../../../SizeConfig.dart';
import '../../../Custom_Constants/StickerPage.dart';
import '../../../Custom_Constants/BackgroundPage.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
import '../../../../models/drag_drop.dart';

// ignore: must_be_immutable
class FunuralTemplate2 extends StatefulWidget {
  //DragDrop dataLandscape;
  DragDrop dataPotraid;
   var response;
  JsonCode editjson;
  DraftJsonCode draftJsonCode;
  var draftId;
  var editId;
  var width;
  File image1;
  File image2;

  FunuralTemplate2({
    Key key,
    this.width,
    this.image1,
    this.image2,
    this.editjson,
    this.response,
    this.draftJsonCode,
    this.draftId,
    this.editId,
    this.dataPotraid
  }) : super(key: key);

  static FunuralTemplate2State of(BuildContext context) =>
      //   ignore: deprecated_member_use
      context.findAncestorStateOfType<FunuralTemplate2State>();
  @override
  FunuralTemplate2State createState() => FunuralTemplate2State();
}

class FunuralTemplate2State extends State<FunuralTemplate2> {
  File image1;
  File image2;
  File editingimage2;
  bool imageselected1 = false;
  bool imageselected2 = false;
  bool isImageEditing = false;

  bool isTextediting = false;
  // bool ec.isOpenWidget.va = false;
  double textOpacity = 1.0;
  // bool ec.isOpenWidget.value = false;
  // bool ec.isOpenBackground.value = false;
  // bool isOpenTextbar = false;
  bool isImageSaveLoading = false;
  final GlobalKey myglobalKey = new GlobalKey();
  final GlobalKey<ScaffoldState> myglobalkey1 = new GlobalKey<ScaffoldState>();
  final GlobalKey flexibleKey = new GlobalKey();
  AppbardataState appbardataState = AppbardataState();
  ShowImageState showImageState = ShowImageState();
  String sticker = "assets/stickers/sticker_3.png";
  List<String> stickerlist2 = [];
  List<StickerList> stickerList = <StickerList>[];
  List<TextListModel> textList = <TextListModel>[];
  List<RadioModel> sampleData = <RadioModel>[];
  List<ContainerList> containerList = <ContainerList>[];
  var editId;
  var draftId;
  // List<ContainerList> containerList = <ContainerList>[];
  Color textColor = Color.fromARGB(255, 0, 0, 0);
  int textStyleIndex;
  double strockSliderValue = 0.0;
  int textAlignIndex = 0;
  int textIndex = 0;
  List fontStyleList = [];
  Color textStrockColor = Color.fromARGB(255, 0, 0, 0);
  Random rng = new Random();
  Widget myWidget;
  File imageFile;
  var response;

  // ignore: non_constant_identifier_names
  String background_image;
  // ignore: avoid_init_to_null
  int textStyleEditingIndex = null;
  var datawidth;
  var dataheight;
  var dataleft;
  File fileImage;
  var finalimage;
  var datatop;
  var dataAspectRatio;
  var dataposition1;
  var templateKey = 'fn_2';
  var jsonCode1 = "";
  var isSaved = false;
  File imagePath;
  var editJson;

  @override
  void initState() {
    super.initState();
    response = widget.response;
    if(widget.editjson != null){
      editJson = widget.editjson;
    }
    else if(widget.draftJsonCode != null){
      editJson = widget.draftJsonCode;
    }
    GoogleFonts.asMap().forEach((key, value) {
      fontStyleList.add(value);
    });
    sampleData.add(new RadioModel(true, 'Color'));
    sampleData.add(new RadioModel(false, 'Font Style'));

    sampleData.add(new RadioModel(false, 'Strock'));
    sampleData.add(new RadioModel(false, 'Align'));
   editId = widget.editId;
    draftId = widget.draftId;
    if(ec.isdraftLoading.value == true){
      ec.isdraftLoading.value = false;
    }

    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    datawidth = widget.width;
    dataAspectRatio = 0.75;
    dataheight = datawidth / dataAspectRatio;

    if (ec.isEditTemplate.value) {
      background_image = editJson.backGround.background;
    editJson.containers.forEach((element) {
        containerList.add(ContainerList(
          height: double.parse(element.height),
          width: double.parse(element.width),
          // ignore: unrelated_type_equality_checks
          containerimage: File(element.containerImage
              .replaceAll('File:', "")
              .replaceAll('\'', "")
              .replaceAll(' ', "")),
          scale: double.parse(element.scale),
          xposition: double.parse(element.xposition),
          yposition: double.parse(element.yposition),
          rotation: double.parse(element.rotation),
        ));
      });
      ec.callDecoratedfun.value = true;

      editJson.textlist.forEach((element) {
        textList.add(TextListModel(
          text: element.text,
          finalTextAlignIndex: int.parse(element.finalTextAlignIndex),
          height: double.parse(element.height),
          width: double.parse(element.width),
          rotationAngle: double.parse(element.rotationAngle),
          scaleFactor: double.parse(element.scaleFactor),
          textStyleIndex: int.parse(element.textStyleIndex),
          strockValue: double.parse(element.strockValue),
          textOpacity: double.parse(element.textOpacity),
          strockColor: Color(int.parse(element.strockColor
              .replaceAll('Color(', '')
              .replaceAll(')', '')
              .replaceAll('Materialprimary value: ', ''))),
          textColor: Color(int.parse(element.textColor
              .replaceAll('Color(', '')
              .replaceAll(')', '')
              .replaceAll('Materialprimary value: ', ''))),
          xPosition: double.parse(element.xPosition),
          yPosition: double.parse(element.yposition),
          textStyle: textstyleList[int.parse(element.textStyleIndex)](
              color: Color(int.parse(element.textColor
                  .replaceAll('Color(', '')
                  .replaceAll(')', '')
                  .replaceAll('Materialprimary value: ', '')))),
          textStyle2: textstyleList[int.parse(element.textStyleIndex)](
              color: Color(int.parse(element.textColor
                  .replaceAll('Color(', '')
                  .replaceAll(')', '')
                  .replaceAll('Materialprimary value: ', '')))),
        ));
      });

      editJson.sticker.forEach((element) {
        stickerList.add(StickerList(
          height: double.parse(element.height),
          width: double.parse(element.width),
          xPosition: double.parse(element.xposition),
          yPosition: double.parse(element.yposition),
          name: element.name,
          rotation: double.parse(element.rotation),
          scale: double.parse(element.scale),
        ));
      });
    } else {
      background_image =
         "https://i.pinimg.com/originals/af/8d/63/af8d63a477078732b79ff9d9fc60873f.jpg";
      //print(response);
      //data = widget.dataPotraid;

      containerList.add(ContainerList(
        height: dataheight *
            0.50,
        width: datawidth *
            0.50,
        left: dataleft,
        xposition: 0.25,
        yposition: 0.25,
        // xposition: 0.25,
        // yposition: 0.25,
        top: datatop,
        scale: 1.0,
        rotation: 0.0,
      ));
      ec.callDecoratedfun.value = true;
    }
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  final snackBarForImage = SnackBar(
    content: Text('Please select image!!'),
  );

  TextStyle textStyle = GoogleFonts.aBeeZee(fontSize: 20.0);

  TextStyle textStyle1 = GoogleFonts.aBeeZee(fontSize: 20.0);
  EditZoomController ec = Get.put(EditZoomController());

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        resizeToAvoidBottomInset: false,
        key: myglobalkey1,
        appBar: (ec.isOpenBackground.value ||
                ec.isOpenTextbar.value ||
                ec.isOpenWidget.value ||
                ec.isOpenSticker.value)
            ? PreferredSize(
                child: Container(
                  color: Colors.transparent,
                ),
                preferredSize: Size.fromHeight(0.0))
            : PreferredSize(
                child: Appbardata(
                  state: this,
                ),
                preferredSize: Size.fromHeight(50)),
        body: WillPopScope(
            child: isImageSaveLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : SizeConfig.orientation == Orientation.portrait
                    ? editingBody()
                    : editingBody(),
            onWillPop: _onBackPressed),
      );
    });
  }

  Widget editingBody() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          flex: 4,
          key: flexibleKey,
          child: MainImageEditingBody(
            //   dataPotraid: widget.dataPotraid,
            state: this,
            //  image1: editingimage1 != null ? editingimage1 : widget.image1,
            //  image2: editingimage2 != null ? editingimage2 : widget.image2,
            finalsticker: sticker,
            finalstickerList: stickerList,
            imageselected1: false,
            imageselected2: false,
            finalstickerList2: stickerlist2,
            editJson: editJson,
          ),
        ),
        SingleChildScrollView(child: bottomnavbar()),
      ],
    );
  }

  // ignore: missing_return
  Future<bool> _onBackPressed() {
    isTextediting = false;
    if (ec.isOpenSticker.value == false &&
        ec.isOpenTextbar.value == false &&
        ec.isOpenWidget.value == false &&
        ec.isOpenBackground.value == false &&
        isTextediting == false) {
      onBackPressed();
    }
    if (ec.isOpenSticker.value == true) {
      setState(() {
        ec.isOpenSticker.value = false;
        ec.isOpenWidget.value = false;
      });
    }
    if (ec.isOpenBackground.value == true) {
      setState(() {
        ec.isOpenWidget.value = false;
        ec.isOpenBackground.value = false;
      });
    }
    if (ec.isOpenTextbar.value == true) {
      setState(() {
        ec.isOpenTextbar.value = false;
        ec.isOpenWidget.value = false;
        isTextediting = false;
      });
    }
    if (isTextediting == true) {
      setState(() {
        isTextediting = false;
      });
    }

    // if (popScreen == true) {
    //   // print("popped screen");
    //   // Navigator.pop(context);
    // }
  }

// ignore: missing_return
   Widget onBackPressed() {
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            child: Container(
              height: 300,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    height: 100,
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.circular(30.0),
                        ),
                    ),
                     child: Center(
                      child: Obx((){
                        return ec.isdraftLoading.value ? CircularProgressIndicator(color: Colors.white,):Icon(
                        Icons.exit_to_app_rounded,
                        size: 50,
                        color: Colors.white,
                      );
                      })
                    ),
                  ),
                  Container(
                    height: 130,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Alert!",
                            style: TextStyle(
                                fontSize: 28, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Are you sure, you want to Exit?",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                (editId == null && draftId == null) ? !isSaved ?
                       Container(
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                ElevatedButton(
                                  onPressed: () {
                                    ec.isdraftLoading.value = true;
                                    saveAsDraft();
                                  },
                                  child: Text("Save as draft"),
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.blue[500],
                                    shadowColor: Colors.grey,
                                    onPrimary: Colors.white,
                                  ),
                                ),
                                Row(
                                  children: [
                                    ElevatedButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: Text("No")),
                                    SizedBox(
                                      width: 7.5,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          ec.innerPhotoview.value = 1;
                                          ec.textediting.value = 0;
                                          ec.stickerview.value = 1;
                                          ec.ListofFiles();
                                          ec.ListofFilesDraft();
                                          ec.isEditTemplate.value = false;
                                          //Function called
                                          Navigator.of(context).pop();
                                          Navigator.pop(context);
                                        },
                                        child: Text("Yes"))
                                  ],
                                )
                              ],
                            ),
                          ),
                        )
                      : Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("No")),
                              ElevatedButton(
                                  onPressed: () {
                                    ec.innerPhotoview.value = 1;
                                    ec.textediting.value = 0;
                                    ec.stickerview.value = 1;
                                    ec.ListofFiles();
                                    ec.isEditTemplate.value = false;
                                    //Function called
                                    Navigator.of(context).pop();
                                    Navigator.pop(context);
                                  },
                                  child: Text("Yes"))
                            ],
                          ),
                        ) :
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("No")),
                              ElevatedButton(
                                  onPressed: () {
                                    ec.innerPhotoview.value = 1;
                                    ec.textediting.value = 0;
                                    ec.stickerview.value = 1;
                                    ec.ListofFiles();
                                    ec.isEditTemplate.value = false;
                                    //Function called
                                    Navigator.of(context).pop();
                                    Navigator.pop(context);
                                  },
                                  child: Text("Yes"))
                            ],
                          ),
                        ) 
                ],
              ),
            ),
          );
        });
  }

 Future saveAsDraft() async{
   bool value = true;
   containerList.forEach((element) {
     if(element.containerimage == null){
       value = false;
     }
   });
    // ec.isdraft.value = true;
    if(value){
    appbardataState.export(this);
    await appbardataState.takeScreenshot(this,true);
    print(fileImage);
    print(finalimage);
    savefileDraft(fileImage, finalimage,this).then((value){
      Navigator.of(context).pop();
    Navigator.pop(context);
    ec.ListofFilesDraft();
    });    
    }
    else{
      ec.isdraftLoading.value = false;
      Navigator.pop(context);
      Fluttertoast.showToast(msg: "Add image to save in draft");
    }
  }

  // ignore: missing_return

  void addText() {
    ec.callDecoratedfun.value = true;
    setState(() {
      textList.add(TextListModel(
        text: "Double tap to edit",
        xPosition: 0.50,
        yPosition: 0.50,
        finalTextAlignIndex: 1,
        height: 30,
        width: 80,
        rotationAngle: 0.0,
        scaleFactor: 2.0,
        textStyle: GoogleFonts.concertOne(),
        textStyle2: GoogleFonts.concertOne(),
        textStyleIndex: 0,
        textColor: Colors.black,
        strockColor: Colors.black,
        strockValue: 0,
        textOpacity: 1,
      ));
      ec.textediting.value = textList.length;
    });
  }

  Widget bottomnavbar() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        // if (ec.isOpenBackground.value == false && ec.isOpenWidget.value == false)
        //   Padding(
        //     padding: EdgeInsets.only(bottom: 10.0),
        //     child: LikeAndRefresh(),
        //   ),
        (ec.isOpenBackground.value ||
                    ec.isOpenSticker.value ||
                    ec.isOpenTextbar.value) &&
                ec.isOpenWidget.value
            ? myWidget
            : SizedBox(),
        ec.isOpenTextbar.value ? textBottomNavBar() : bottom_nav()
        // ec.isOpenBackground.value
        //     ? Container() // here background code
        //     : ec.isOpenSticker.value
        //         ? Container()
        //         : bottom_nav()
      ],
    );
  }

  Widget textBottomNavBar() {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 7.0),
      child: Card(
        child: Container(
          height: 50,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.all(5.0),
                child: InkWell(
                    borderRadius: BorderRadius.circular(5.0),
                    onTap: () {
                      setState(() {
                        sampleData
                            .forEach((element) => element.isSelected = false);
                        sampleData[index].isSelected = true;
                        textIndex = index;
                        if (index == 1) {
                          // ec.isOpenWidget.value = true;
                          setState(() {
                            myWidget = EditText(
                              fontStyleList: fontStyleList,
                              state: this,
                              myWidgetIndex: textStyleIndex,
                            );
                            ec.isOpenWidget.value = true;
                            ec.isOpenTextbar.value = true;
                          });
                        } else if (index == 0) {
                          // ec.isOpenWidget.value = true;
                          setState(() {
                            myWidget = TextColorPicker(
                              width: MediaQuery.of(context).size.width,
                              state: this,
                              selectedTextIndex: textStyleEditingIndex,
                            );
                            ec.isOpenWidget.value = true;
                            ec.isOpenTextbar.value = true;
                          });
                        } else if (index == 2) {
                          setState(() {
                            myWidget = TextStrock(
                              state: this,
                              selectedIndex: textStyleEditingIndex,
                            );
                            ec.isOpenWidget.value = true;
                            ec.isOpenTextbar.value = true;
                          });
                        } else if (index == 3) {
                          setState(() {
                            myWidget = SetTextAlign(
                              state: this,
                              selectedTextIndex: textStyleEditingIndex,
                              textStyleList: fontStyleList,
                            );
                            ec.isOpenWidget.value = true;
                            ec.isOpenTextbar.value = true;
                          });
                        }
                      });
                    },
                    child: Container(
                        // margin: EdgeInsets.all(5.0),
                        // padding: EdgeInsets.all(5.0),
                        width: MediaQuery.of(context).size.width / 5.2,
                        child: RadioItem(sampleData[index]))),
              );
            },
            itemCount: sampleData.length,
          ),
        ),
      ),
    );
  }

  // ignore: non_constant_identifier_names
  Padding bottom_nav() {
    return Padding(
      padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 5.0),
      child: Container(
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            BottomNavBarItem(
                image: "assets/images/add.svg",
                title: "Add",
                onPress: () {
                  setState(() {
                    containerList.add(ContainerList(
        height: dataheight *
            0.50,
        width: datawidth *
            0.50,
        left: dataleft,
        xposition: 0.25,
        yposition: 0.25,
        // xposition: 0.25,
        // yposition: 0.25,
        top: datatop,
        scale: 1.0,
        rotation: 0.0,
      ));
                    // ec.innerPhotoview.value = containerList.length;
                  });
                }),
            BottomNavBarItem(
                image: "assets/images/Background.svg",
                title: "Background",
                onPress: () {
                  setState(() {
                    ec.isOpenWidget.value = false;
                    ec.isOpenTextbar.value = false;
                    ec.isOpenBackground.value = true;
                    myWidget = BackgroundPage(
                      state: this,
                    );
                    ec.isOpenWidget.value = true;
                  });
                }),
            BottomNavBarItem(
              image: "assets/images/sentiment_satisfied_alt-black-18dp.svg",
              title: "Stickers",
              onPress: () {
                setState(() {
                  ec.isOpenSticker.value = true;
                  ec.isOpenWidget.value = true;
                  ec.isOpenBackground.value = false;
                  ec.isOpenTextbar.value = false;
                  myWidget = StickerPage(state: this);
                });
              },
            ),
            BottomNavBarItem(
              image: "assets/images/letter-a.svg",
              title: "Text",
              onPress: () {
                addText();
                // ec.isOpenWidget.value = false;
                //ec.isOpenWidget.value = true;
                // ec.isOpenBackground.value = false;
                //ec.isOpenTextbar.value = true;
                // myWidget = null;
              },
            ),
          ],
        ),
      ),
    );
  }
}
