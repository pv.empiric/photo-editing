import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gesture_x_detector/gesture_x_detector.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
// import 'package:image_editing_flutter_app/screens/Custom_Constants/zoomwidget.dart';
// ignore: unused_import
// import 'package:image_editing_flutter_app/screens/templates/Funural/Template0/FunuralTemplate0.dart';
import 'package:image_picker/image_picker.dart';
import 'package:photo_view/photo_view.dart';

// ignore: must_be_immutable
class ContainerView extends StatefulWidget {
  var state;
  List containerlist;
  ContainerView({this.state});
  @override
  _ContainerViewState createState() => _ContainerViewState();
}

class _ContainerViewState extends State<ContainerView> {
  //var data;
  var state;
  // bool innerPhotoview = true;

  // PhotoViewController controller;
  List<PhotoViewController> _controller = [];
  List<PhotoViewController> controller = [];
  List<PhotoViewScaleStateController> scaleStateController = [];
  @override
  void initState() {
    state = widget.state;

    super.initState();
  }

  void onController(PhotoViewControllerValue value) {
    setState(() {
      // print(value);
    });
  }

  void onScaleState(PhotoViewScaleState scaleState) {
    print(scaleState);
  }

  EditZoomController editZoomController = Get.put(EditZoomController());

  @override
  Widget build(BuildContext context) {
    for (var i = 0; i < state.containerList.length; i++) {
      controller
          .add(PhotoViewController()..outputStateStream.listen(onController));
    }
    for (var i = 0; i < state.containerList.length; i++) {
      scaleStateController.add(PhotoViewScaleStateController()
        ..outputScaleStateStream.listen(onScaleState));
    }
    for (var i = 0; i < state.containerList.length; i++) {
      _controller.add(PhotoViewController());
    }

    var x = 0;

    return Stack(
      children: state.containerList.map<Widget>((val) {
        // val.datajsonheight = state.dataheight *
        //     double.parse(state.response["Templates"]["Funaral"][2]["Image"][0]
        //         ["height"]);
        // val.datajsonwidth = state.datawidth *
        //                       double.parse(state.response["Templates"]
        //                           ["Funaral"][2]["Image"][0]["width"]);
        return XGestureDetector(
          onDoubleTap: (_) {
            if (editZoomController.innerPhotoview.value == 0) {
              editZoomController.innerPhotoview.value =
                  state.containerList.indexOf(val) + 1;
            } else {
              // editZoomController.innerPhotoview.value = 0;
            }
            setState(() {
              val.xposition += 0.001;
              val.yposition += 0.001;
            });
          },
          child: Stack(
            children: [
              editZoomController.innerPhotoview.value ==
                      state.containerList.indexOf(val) + 1
                  ? Positioned(
                      top: val.yposition - 45,
                      left: val.xposition + val.width - 35,
                      child: IconButton(
                          icon: Icon(
                            Icons.check,
                            color: Colors.white,
                            size: 20.0,
                          ),
                          onPressed: () {
                            editZoomController.innerPhotoview.value = 0;
                            setState(() {
                              val.xposition += 0.01;
                              val.yposition += 0.01;
                            });
                          }),
                    )
                  : Container(),
              editZoomController.innerPhotoview.value ==
                      state.containerList.indexOf(val) + 1
                  ? Positioned(
                      top: val.yposition - 45,
                      left: val.xposition - 10,
                      child: IconButton(
                          icon: Icon(
                            Icons.delete_outline_sharp,
                            color: Colors.white,
                            size: 20.0,
                          ),
                          onPressed: () {
                            editZoomController.innerPhotoview.value = 0;
                            state.containerList.remove(val);
                            setState(() {
                              val.xposition += 0.01;
                              val.yposition += 0.01;
                            });
                          }),
                    )
                  : Container(),
              Positioned(
                left: val.xposition,
                top: val.yposition,
                child: XGestureDetector(
                  child: Container(
                    color: val.containerimage == null
                        ? Colors.transparent
                        : Colors.transparent,
                    height: val.height,
                    width: val.width,
                    child: PhotoView.customChild(
                      // controller: photoviewcontroller[val],
                      controller: _controller[state.containerList.indexOf(val)],
                      backgroundDecoration: BoxDecoration(
                        color: Colors.transparent,
                      ),
                      child: Container(
                        height: val.height,
                        width: val.width,
                        decoration: BoxDecoration(
                          color: val.containerimage == null
                              ? Color(0xffd2d5e6)
                              : Colors.transparent,
                          border: Border(
                            top: BorderSide(
                              color: Colors.white,
                              width: 10.0,
                            ),
                            left: BorderSide(
                              color: Colors.white,
                              width: 10.0,
                            ),
                            right: BorderSide(
                              color: Colors.white,
                              width: 10.0,
                            ),
                            bottom: BorderSide(
                              color: Colors.white,
                              width: 45.0,
                            ),
                          ),
                        ),
                        child: val.containerimage == null
                            ? Center(
                                child: GestureDetector(
                                  onTap: () async {
                                    final image = await ImagePicker()
                                        .getImage(source: ImageSource.gallery);
                                    setState(() {
                                      if (image != null) {
                                        val.containerimage = File(image.path);
                                      }
                                      state.imageselected1 = true;
                                    });
                                    // getImage();
                                  },
                                  child: SvgPicture.asset(
                                      "assets/images/addpicture1.svg"),
                                ),
                              )
                            : Container(
                                child: ClipRect(
                                  child: PhotoView.customChild(
                                    scaleStateController: scaleStateController[
                                        state.containerList.indexOf(val)],
                                    controller: controller[
                                        state.containerList.indexOf(val)],
                                    child: Image.file(
                                      val.containerimage,
                                      fit: BoxFit.scaleDown,
                                    ),
                                    minScale: 1.0,
                                    maxScale: 5.0,
                                    initialScale: 1.2,
                                    enableRotation: true,
                                    disableGestures: editZoomController
                                            .innerPhotoview.value !=
                                        state.containerList.indexOf(val) + 1,
                                  ),
                                ),
                              ),
                      ),
                      enableRotation: true,
                      disableGestures:
                          editZoomController.innerPhotoview.value ==
                              state.containerList.indexOf(val) + 1,
                    ),
                  ),
                  onMoveUpdate: (details) {
                    setState(() {
                      if (editZoomController.innerPhotoview.value !=
                          state.containerList.indexOf(val) + 1) {
                        val.xposition += details.delta.dx;
                        val.yposition += details.delta.dy;
                      }
                    });
                  },
                ),
              ),
              if (x <= state.containerList.length - 1) Data(x++),
            ],
          ),
        );
      }).toList(),
    );
  }

  // ignore: non_constant_identifier_names
  StreamBuilder<PhotoViewControllerValue> Data(i) {
    return StreamBuilder(
      stream: _controller[i].outputStateStream,
      // ignore: missing_return
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Container(
            child: Text("."),
          );
        else {
          // state.textList[i].scale = snapshot.data.scale;
          // state.textList[i].rotation = snapshot.data.scale;
          state.containerList[i].scale = snapshot.data.scale;
          state.containerList[i].rotation = snapshot.data.rotation;
        }
        return Opacity(
          opacity: 0,
          child: Container(),
        );
      },
    );
  }

  Future getImage() async {
    final image = await ImagePicker().getImage(source: ImageSource.gallery);
    setState(() {
      if (image != null) {
        state.image1 = File(image.path);
      }
      state.imageselected1 = true;
    });
  }
}
/* 
child: PhotoView.customChild(
                                  
                                  // tightMode: true,
                                  childSize: Size(state.datawidth *
                                  double.parse(state.response["Templates"]
                                      ["Funaral"][2]["Image"][0]["width"]),state.dataheight *
                                  double.parse(state.response["Templates"]
                                      ["Funaral"][2]["Image"][0]["height"])),
                                   customSize: Size(state.datawidth *
                                  double.parse(state.response["Templates"]
                                      ["Funaral"][2]["Image"][0]["width"]),state.dataheight *
                                  double.parse(state.response["Templates"]
                                      ["Funaral"][2]["Image"][0]["height"])),
                                  maxScale:
                                      PhotoViewComputedScale.covered * 3.0,
                                  minScale:
                                      PhotoViewComputedScale.contained * 2.2,
                                  initialScale: PhotoViewComputedScale.covered,
                                  child: Image.file(
                                    val.containerimage,
                                    fit: BoxFit.contain,
                                  ),
                                  disableGestures: false,
                                ),
                                */
