import 'dart:io';
// ignore: unused_import
// import 'package:image_editing_flutter_app/screens/Custom_Constants/containerView.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
// ignore: unused_import
import 'package:image_editing_flutter_app/models/saved_template_model.dart';

// import './template2containerView.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/text_list.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/widgets.dart';
import 'package:image_editing_flutter_app/screens/templates/Funural/Template2/template2containerView.dart';

// ignore: unused_import
import '../../../Custom_Constants/BackgroundPage.dart';
// ignore: unused_import

// ignore: unused_import
import '../../../Custom_Constants/StickerPage.dart';
import '../../../Custom_Constants/stickerview.dart';
import 'package:flutter/material.dart';
import 'package:gesture_x_detector/gesture_x_detector.dart';
import 'package:image_editing_flutter_app/models/drag_drop.dart';

import 'package:photo_view/photo_view.dart';

// ignore: must_be_immutable
class MainImageEditingBody extends StatefulWidget {
 DragDrop dataPotraid;
  var state;
  
  File image1;
  File image2;
  bool imageselected1;
  bool imageselected2;
  String finalsticker;
  List finalstickerList;
  List<String> finalstickerList2;
  var editJson;
  Offset temp = Offset(150, 200);
  MainImageEditingBody({
 this.dataPotraid,
    this.image1,
    this.image2,
    this.finalsticker,
    this.finalstickerList,
    this.imageselected1,
    this.imageselected2,
    this.state,
    this.editJson,
    this.finalstickerList2,
    
  });
  @override
  _MainImageEditingBodyState createState() => _MainImageEditingBodyState();
}

class _MainImageEditingBodyState extends State<MainImageEditingBody> {
  DragDrop data;
   EditZoomController ec = Get.put(EditZoomController());

  var state;
  PhotoViewController photoViewController1;
  PhotoViewController photoViewController2;

  @override
  void initState() {
 //   data = widget.dataPotraid;
    state = widget.state;
    photoViewController1 = PhotoViewController();
    photoViewController2 = PhotoViewController();
    super.initState();
  }

  EditZoomController eC = Get.put(EditZoomController());

  Widget build(BuildContext context) {
    
      return Stack(
        // overflow: Overflow.clip,
        clipBehavior: Clip.hardEdge,
      children: [
        RepaintBoundary(
          key: state.myglobalKey,
          child: Container(
            margin: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height /
               ( !eC.isEditTemplate.value?
                    60 : double.parse(widget.editJson.mainContainer.marginVerticle)),
                horizontal: MediaQuery.of(context).size.width /
               ( !eC.isEditTemplate.value?
                    60 : double.parse(widget.editJson.mainContainer.marginHorizontal)),
              ),
            // child: state.imageselected1 &&
            //         state.imageselected2 &&
            //         state.image1 != null &&
            //         state.image2 != null
            child : Container(
                    // child: RepaintBoundary(
                    //   key: state.myglobalKey,
                    child: Stack(
                      // ignore: deprecated_member_use
                      overflow: Overflow.clip,
                      children: [
                        // if (state.image1 != null &&
                        //     state.image2 != null)
                          AspectRatio(
                            aspectRatio: 0.75,
                            child: XGestureDetector(
                              onTap: (_) {
                                
                                  // state.isTextediting = false;
                                  // state.isOpenTextbar = false;
                                  // ec.isOpenWidget.value = false;
                                
                              },
                              child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(
                                color: Colors.grey.withOpacity(0.5),
                                width: 2.0),
                          ),
                          child: Stack(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.circular(10.0)
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10.0),
                                                                  child: GetFancyShimmerImage(
                                    url: state.background_image,
                                    boxFit: BoxFit.cover,
                                    width: double.infinity
                                  ),
                                ),
                              ),
                              Stack(
                                  children: [
                                    if (state.containerList.length != 0)
                                      ContainerView(
                                        state: state,
                                      //  containerlist: state.containerList,
                                      ),
                                    if (state.stickerList
                                            .length !=
                                        0)
                                      StickerView(
                                        stickerList:
                                            state.stickerList,
                                        state: state,
                                        isImageEditing: state.isImageEditing,
                                      ),

                                    if (state.textList.length !=
                                        0)
                                      TextList(
                                        textList:
                                            state.textList,
                                        state: state,
                                        isImageEditing: state.isImageEditing,
                                        isTextEditing:
                                            state.isTextediting,
                                        fontStyleList:
                                            state.fontStyleList,
                                        finalTextStyleIndex: state.textStyleIndex,
                                      ),

                                    // if(state.textList.length != 0)
                                    //     TextView(
                                    //       isImageEditing: state.isImageEditing,
                                    //       template2state: state,
                                    //       textlist: state.textList,
                                    //     )
                                  ],
                                ),
                            ]),
                              ),
                            ),
                          )
                        // else
                        //   MyBody(
                        //     template2state: state,
                        //     dataPotraid: data,
                        //   )
                      ],
                    ),
                    //    ),
                  )
                // : MyBody(
                //     template2state: state,
                //     dataPotraid: data,
                //   ),
          ),
        )
      ],
    );
  
  }
}

