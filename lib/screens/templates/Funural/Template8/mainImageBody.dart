import 'dart:io';
import 'package:flutter/material.dart';
import 'package:gesture_x_detector/gesture_x_detector.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/controller/editzoomcontroller.dart';
import 'package:image_editing_flutter_app/models/drag_drop.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/widgets.dart';
import 'package:photo_view/photo_view.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/text_list.dart';

import 'package:image_editing_flutter_app/screens/Custom_Constants/stickerview.dart';
import 'template8containerView.dart';

// import 'package:image_editing_flutter_app/screens/Custom_Constants/containerView.dart';
// ignore: must_be_immutable
class MainImageEditingBody extends StatefulWidget {
  DragDrop dataPotraid;
  var state;
  File image1;
  File image2;
  bool imageselected1;
  bool imageselected2;
  String finalsticker;
  List finalstickerList;
  List<String> finalstickerList2;
  var editJson;
  Offset temp = Offset(150, 200);
  MainImageEditingBody({
    this.dataPotraid,
    this.image1,
    this.image2,
    this.finalsticker,
    this.finalstickerList,
    this.imageselected1,
    this.imageselected2,
    this.state,
    this.editJson,
    this.finalstickerList2,
  });
  @override
  _MainImageEditingBodyState createState() => _MainImageEditingBodyState();
}

class _MainImageEditingBodyState extends State<MainImageEditingBody> {
  DragDrop data;
  Offset a;
  var state;
  PhotoViewController photoViewController1;
  PhotoViewController photoViewController2;

  @override
  void initState() {
    // data = widget.dataPotraid;
    photoViewController1 = PhotoViewController();
    photoViewController2 = PhotoViewController();
    state = widget.state;
    a = Offset(state.datawidth * 0.65, state.dataheight * 0.75);
    super.initState();
  }

  EditZoomController eC = Get.put(EditZoomController());


  Widget build(BuildContext context) {
    return Stack(
      children: [
        RepaintBoundary(
          key: state.myglobalKey,
          child: Container(
            margin: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height /
               ( !eC.isEditTemplate.value?
                    60 : double.parse(widget.editJson.mainContainer.marginVerticle)),
                horizontal: MediaQuery.of(context).size.width /
               ( !eC.isEditTemplate.value?
                    60 : double.parse(widget.editJson.mainContainer.marginHorizontal)),
              ),
            child: Container(
              // child: RepaintBoundary(
              //   key: state.myglobalKey,
              child: Stack(
                // ignore: deprecated_member_use
                overflow: Overflow.clip,
                children: [
                  // if (state.image1 != null &&
                  //     state.image2 != null)
                  AspectRatio(
                    aspectRatio: 0.75,
                    child: XGestureDetector(
                      onTap: (_) {
                        setState(() {
                          // // state.isTextediting = false;
                          // state.isOpenTextbar = false;
                          // state.isOpenWidget = false;
                        });
                      },
                      child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(
                                color: Colors.grey.withOpacity(0.5),
                                width: 2.0),
                          ),
                          child: Stack(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.circular(10.0)
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10.0),
                                                                  child: GetFancyShimmerImage(
                                    url: state.background_image,
                                    boxFit: BoxFit.cover,
                                    width: double.infinity
                                  ),
                                ),
                              ),
                              Stack(
                          children: [
                            
                             Positioned(
                              bottom: 0.0,
                              child: Container(
                                  width: state.datawidth,
                                  child: GetFancyShimmerImage(
                                    url: "https://images2.imgbox.com/15/66/7BjQRHEb_o.png",
                                    boxFit: BoxFit.fitWidth
                                  )
                                  ),
                            ),
                            Positioned(
                              top: 0.0,
                              child: Container(
                                width: state.datawidth,
                                
                                 child: GetFancyShimmerImage(
                                    url: "https://images2.imgbox.com/02/25/1wiG5PQN_o.png",
                                    boxFit: BoxFit.fitWidth
                                  )
                              ),
                            ),
                           
                            if (state.containerList.length != 0)
                              ContainerView(
                                state: state,
                                //  containerlist: state.containerList,
                              ),
                               Positioned(
                              top: a.dy,
                              left: a.dx,
                              child: XGestureDetector(
                                  onMoveUpdate: (detail) {
                                    setState(() {
                                      a += detail.delta;
                                    });
                                  },
                                  child: Container(
                                    height: 150,
                                    width: 150,
                                    child: PhotoView.customChild(
                                        enableRotation: true,
                                        backgroundDecoration: BoxDecoration(
                                            color: Colors.transparent),
                                        child: GetFancyShimmerImage(
                                          url: "https://images2.imgbox.com/de/1f/drgVxgbX_o.png",
                                          height: 150
                                        )
                                        ),
                                  )),
                            ),
                            if (state.stickerList.length != 0)
                              StickerView(
                                stickerList: state.stickerList,
                                state: state,
                                isImageEditing:
                                    state.isImageEditing,
                              ),
                            if (state.textList.length != 0)
                              TextList(
                                textList: state.textList,
                                state: state,
                                isImageEditing:
                                    state.isImageEditing,
                                isTextEditing:
                                    state.isTextediting,
                                fontStyleList:
                                    state.fontStyleList,
                                finalTextStyleIndex:
                                    state.textStyleIndex,
                              ),
                             
                          ],
                        ),]),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
