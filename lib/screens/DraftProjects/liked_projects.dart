import 'package:flutter/material.dart';
// import 'package:image_editing_flutter_app/screens/myprojects/components/Body.dart';
import './components/Body.dart';

class LikedProjects extends StatefulWidget {

  final String title;

  const LikedProjects({Key key, this.title}) : super(key: key);

  @override
  _LikedProjectsState createState() => _LikedProjectsState(title);
}

class _LikedProjectsState extends State<LikedProjects> {
  final String title;
  _LikedProjectsState(this.title);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(title),
      // bottomNavigationBar: MyHomePage,
    );
  }


}
