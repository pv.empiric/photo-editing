import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_editing_flutter_app/components/textEdit.dart';
import 'package:image_editing_flutter_app/components/textStyle.dart';
import 'package:image_editing_flutter_app/constant.dart';


class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {


  bool isVisible = false;
  bool isVisible1 = false;

  final TextEditingController name = TextEditingController();
  final TextEditingController userName = TextEditingController();
  final TextEditingController password = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController re_enterPassword = TextEditingController();

  @override
  void initState() {
    
    name.text = "Moonmun Desuza";
    userName.text = "Munmundesu@gmail.com";
    password.text = "123456";
    re_enterPassword.text = "123456";
    super.initState();
  }

  showPassword(){
    return Icon(Icons.visibility);
  }

  hidePassword(){
    return Icon(Icons.visibility_off);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Text("Profile",style: headerTextStyle),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(11.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(100.0),
                      child: Container(
                        child: Image.asset("assets/jpgImages/userProfilePic.png",
                          height: 95.0,
                          width: 95.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text("Edit Profile",style:appTextStyleWithoutBold(
                        textColor: Colors.red,
                        fontSize: 8
                      ),textScaleFactor: 1.0,),
                      Text("photo",style:appTextStyleWithoutBold(
                          textColor: Colors.red,
                          fontSize: 8
                      ),textScaleFactor: 1.0),
                    ],
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Name",
                      style:appTextStyleWithBold(
                        fontsize: 14,
                        textColor: Colors.black
                      ),
                    ),
                    MyTextEdit(
                      label: "Name",
                      isSecured: false,
                      textEditingController: name,
                      decoration: InputDecoration(
                          focusColor: kSecondaryColor,
                          fillColor: kSecondaryColor,
                      ),
                    ),
                    Text(
                      "Username",
                      style:appTextStyleWithBold(
                          fontsize: 14,
                          textColor: Colors.black
                      ),
                    ),
                    MyTextEdit(
                      label: "UserName",
                      isSecured: false,
                      textEditingController: userName,
                      decoration: InputDecoration(
                        focusColor: kSecondaryColor,
                        fillColor: kSecondaryColor,
                      ),
                    ),
                    Text(
                      "Password",
                      style:appTextStyleWithBold(
                          fontsize: 14,
                          textColor: Colors.black
                      ),
                    ),
                    MyTextEdit(
                      label: "Password",
                      // isSecured: true,
                      isSecured: isVisible ? false : true,
                      textEditingController: password,
                      decoration: InputDecoration(
                        // labelText: "Name"
                        // suffixIcon: GestureDetector(
                        //   onTap: (){
                        //     setState(() {
                        //       isVisible = !isVisible;
                        //     });
                        //   },
                        //   child: isVisible ? showPassword() : hidePassword(),
                        // ),
                        focusColor: kSecondaryColor,
                        fillColor: kSecondaryColor,
                      ),
                    ),
                    Text(
                        "Re-Enter Password",
                        style:appTextStyleWithBold(
                            fontsize: 14,
                            textColor: Colors.black
                        )
                    ),
                    MyTextEdit(
                      label: "Re-enterPassword",
                      isSecured: isVisible1 ? false : true,
                      textEditingController: re_enterPassword,
                      decoration: InputDecoration(
                        // labelText: "Name"
                        //   suffixIcon: GestureDetector(
                        //     onTap: (){
                        //       setState(() {
                        //         isVisible1 = !isVisible1;
                        //       });
                        //     },
                        //     child: isVisible1 ? showPassword() : hidePassword(),
                        //   ),
                          focusColor: kSecondaryColor,
                          fillColor: kSecondaryColor
                      ),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
