// ignore: unused_import
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/components/lib/google_fonts.dart';
import 'package:image_editing_flutter_app/components/textStyle.dart';
// import 'package:image_editing_flutter_app/components/transitions.dart';
import 'package:image_editing_flutter_app/constant.dart';
import 'package:image_editing_flutter_app/models/tamplate_model.dart';
import 'package:image_editing_flutter_app/screens/DraftProjects/liked_projects.dart';
import 'package:image_editing_flutter_app/screens/collection/collection.dart';
import 'package:image_editing_flutter_app/screens/myprojects/my_projects.dart';
// import 'package:image_editing_flutter_app/screens/templates/new/Template1/photo_edit.dart';
import 'package:image_editing_flutter_app/screens/profile/profile.dart';
import 'package:image_editing_flutter_app/screens/templates/templates.dart';
// ignore: unused_import
import 'package:http/http.dart' as http;

import 'components/controller/editzoomcontroller.dart';

void main() {
  runApp(GetMaterialApp(
    title: 'Flutter Demo',
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      primarySwatch: Colors.blue,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      appBarTheme: AppBarTheme(color: Colors.white, elevation: 0),
      canvasColor: Colors.white,
      scaffoldBackgroundColor: Colors.white,
    ),
    home: MyHomePage(),
  ));
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  EditZoomController editZoomController = Get.put(EditZoomController());
  List<String> texts = [
    'Templates',
    'collection',
    'MyProject',
    // "Rate Us",
    // "Share"
  ];

  List<String> icons = [
    "assets/images/menubarhome.svg",
    "assets/images/menubarcollection.svg",
    "assets/images/menubarcontect.svg",
    // "assets/images/rate.svg",
    // "assets/images/share.svg"
  ];

  List<bool> isHighlighted = [true, false, false, false, false];

  int _selectedIndex = 0;
  int _otherselectedIndex = 0;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var response;
  // var x;
  List<Widget> pages = [
    Templates(),
    Collection(),
    MyProjects(
      title: "My Projects",
    ),
    LikedProjects(
      title: "Your drafts",
    ),
    Profile()
  ];

  @override
  void initState() {
    // readJson();
    super.initState();
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            child: Container(
              height: 300,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    height: 100,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0),
                      ),
                    ),
                    child: Center(
                        child: Icon(
                      Icons.exit_to_app_rounded,
                      size: 50,
                      color: Colors.white,
                    )),
                  ),
                  Container(
                    height: 130,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Alert!",
                            style: TextStyle(
                                fontSize: 28, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Are you sure, you want to Exit?",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        ElevatedButton(onPressed: () {Navigator.of(context).pop(false);}, child: Text("No")),
                        ElevatedButton(onPressed: () {Navigator.of(context).pop(true);}, child: Text("Yes"))
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    // print(fontList);
    if (_otherselectedIndex < 3) {
      _selectedIndex = _selectedIndex;
    } else if (_otherselectedIndex >= 3) {
      _selectedIndex = _otherselectedIndex;
    }
    return Scaffold(
        drawerEdgeDragWidth: 50,
        endDrawerEnableOpenDragGesture: true,
        key: _scaffoldKey,
        appBar: _appBar(context),
        bottomNavigationBar: bottomNavBar(), //
        endDrawer: _endDrawer(
      _selectedIndex), // This trailing comma makes auto-formatting nicer for build methods.
        body:
      WillPopScope(onWillPop: _onBackPressed, child: pages[_selectedIndex]),
      );
  }

  Widget bottomNavBar() {
    return BottomNavigationBar(
      currentIndex: _selectedIndex < 2 ? _selectedIndex : 2,
      onTap: (int index) {
        setState(() {
          _selectedIndex = index;
          _otherselectedIndex = index;
          _endDrawer(index);
          for (int i = 0; i < isHighlighted.length; i++) {
            isHighlighted[i] = false;
          }
          isHighlighted[index] = true;
          if (_selectedIndex == 0) {
            editZoomController.isEditTemplate.value = false;
          }
        });
      },
      selectedItemColor: Color(0XFF306060),
      unselectedItemColor: Color(0XFFA9A9A9),
      selectedIconTheme: IconThemeData(color: Color(0XFF306060), opacity: 0.5),
      unselectedIconTheme: IconThemeData(color: Color(0XFFA9A9A9), opacity: 5),
      backgroundColor: Colors.white,
      elevation: 0,
      items: [
        BottomNavigationBarItem(
          icon: _selectedIndex == 0
              ? SvgPicture.asset(
                  "assets/images/home.svg",
                  color: kPrimaryColor,
                  height: 25.0,
                )
              : SvgPicture.asset("assets/images/home.svg",
                  height: 25.0, color: kUnselectedColor),
          // ignore: deprecated_member_use
          title: _selectedIndex == 0
              ? Text(
                  'Templates',
                  style: TextStyle(
                    shadows: [
                      Shadow(
                        // blurRadius: 10.0,
                        color: Colors.black54.withOpacity(0.16),
                        offset: Offset(1.0, 1.0),
                      ),
                    ],
                  ),
                )
              : Text('Templates'),
        ),
        BottomNavigationBarItem(
          icon: _selectedIndex == 1
              ? SvgPicture.asset("assets/images/collection.svg",
                  height: 25.0, color: kPrimaryColor)
              : SvgPicture.asset("assets/images/collection.svg",
                  height: 25.0, color: kUnselectedColor),
          // ignore: deprecated_member_use
          title: _selectedIndex == 1
              ? Text(
                  'Collection',
                  style: TextStyle(
                    fontFamily: appFontFamily,
                    shadows: [
                      Shadow(
                        // blurRadius: 10.0,
                        color: Colors.black54.withOpacity(0.16),
                        offset: Offset(1.0, 1.0),
                      ),
                    ],
                  ),
                )
              : Text('Collection'),
        ),
        BottomNavigationBarItem(
          icon: _selectedIndex == 2
              ? SvgPicture.asset("assets/images/contact.svg",
                  height: 25.0, color: kPrimaryColor)
              : SvgPicture.asset("assets/images/contact.svg",
                  height: 25.0, color: kUnselectedColor),
          // ignore: deprecated_member_use
          title: _selectedIndex == 2
              ? Text(
                  'My Projects',
                  style: TextStyle(
                    fontFamily: appFontFamily,
                    shadows: [
                      Shadow(
                        // blurRadius: 3.0,
                        color: Colors.black54.withOpacity(0.16),
                        offset: Offset(1.0, 1.0),
                      ),
                    ],
                  ),
                )
              : Text('My Projects'),
        ),
      ],
    );
  }

  AppBar _appBar(BuildContext context) {
    return AppBar(
      leading: IconButton(
        icon: SvgPicture.asset(
          "assets/images/menu.svg",
          color: Colors.black,
        ),
        onPressed: () => _scaffoldKey.currentState.openEndDrawer(),
      ),
      automaticallyImplyLeading: false,
      centerTitle: true,
      // title: Row(
      //   mainAxisSize: MainAxisSize.min,
      //   children: [
      //     IconButton(
      //       icon: SvgPicture.asset(
      //         "assets/images/whishlist.svg",
      //         color: Colors.black,
      //       ),
      //       onPressed: () {
      //         setState(() {
      //           _selectedIndex = 3;
      //           _otherselectedIndex = 3;
      //         });
      //       },
      //     ),
      //   ],
      // ),
      title: Text("Photo Editing",
          style: GoogleFonts.abrilFatface(
              fontSize: 28.0,
              color: Colors.black,
              fontWeight: FontWeight.w200)),
      actions: [
        IconButton(
          onPressed: () {
            setState(() {
              _selectedIndex = 3;
              _otherselectedIndex = 3;
            });
          },
          icon: Icon(
            Icons.drafts_outlined,
            color: Colors.black,
          ),
        )
      ],
      // actions: [
      //   IconButton(
      //     icon: SvgPicture.asset(
      //       "assets/images/whishlist.svg",
      //       color: Colors.black,
      //     ),
      //     onPressed: () {
      //       setState(() {
      //         _selectedIndex = 3;
      //         _otherselectedIndex = 3;
      //       });
      //     },
      //   )
      // ],
      // actions: [
      //   IconButton(
      //     icon: SvgPicture.asset(
      //       "assets/images/settings.svg",
      //       color: Colors.black,
      //     ),
      //     onPressed: () {
      //       setState(() {
      //         _selectedIndex = 4;
      //         _otherselectedIndex = 4;
      //       });
      //     },
      //   )
      // ],
    );
  }

  final tamplatePhoto = TamplateModel(
    tamplate_image: "assets/jpgImages/hiclipart.com.png",
  );

  Widget _endDrawer(int selectedIndex) {
    // print(selectedIndex);
    return Theme(
      data: Theme.of(context).copyWith(canvasColor: kSecondaryColor),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Container(
          child: Drawer(
              child: Container(
            height: MediaQuery.of(context).size.height,
            child: Stack(
              // alignment: Alignment(-1,0),
              children: [
                DrawerHeader(
                  child: Padding(
                    // padding: EdgeInsets.only(top: 42, left: 22),
                    padding: EdgeInsets.only(top: 0, left: 22),
                    child: Container(
                      height: 40,
                      width: 250,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          "Photo Editing",
                          style: GoogleFonts.abrilFatface(
                            fontSize: 60.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    // child: Row(
                    //   crossAxisAlignment: CrossAxisAlignment.start,
                    //   children: [
                    //     Image.asset(
                    //       "assets/jpgImages/userimage.png",
                    //       height: 40.0,
                    //     ),
                    //     Padding(
                    //       padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    //       child: Column(
                    //         crossAxisAlignment: CrossAxisAlignment.start,
                    //         // mainAxisAlignment: MainAxisAlignment.center,
                    //         children: [
                    //           Text(
                    //             "Moonmun Desuza",
                    //             style: TextStyle(
                    //                 fontWeight: FontWeight.bold,
                    //                 fontSize: 20,
                    //                 color: kTextColor),
                    //           ),
                    //           Text(
                    //             "Munmundesu@gmail.com",
                    //             style: TextStyle(
                    //                 fontWeight: FontWeight.bold,
                    //                 fontSize: 13,
                    //                 color: kTextColor),
                    //           )
                    //         ],
                    //       ),
                    //     )
                    //   ],
                    // ),
                  ),
                ),
                // Drawer right side image code
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _selectedIndex = 1;
                      Navigator.pop(context, true);
                    });
                  },
                  child: Opacity(
                    opacity: 0.5,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        // color: Colors.white,
                        height: MediaQuery.of(context).size.height / 2.3,
                        width: MediaQuery.of(context).size.width / 2.5,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30.0),
                                bottomLeft: Radius.circular(30.0))),
                        child: SingleChildScrollView(
                          physics: NeverScrollableScrollPhysics(),
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              Image.asset(
                                "assets/jpgImages/userProfile.png",
                                fit: BoxFit.cover,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      // print("object");
                      // Navigator.push(context,SlideRightRoute(
                      //     page: PhotoEdit(
                      //         // tamplateImage: tamplatePhoto,
                      //     )
                      // ));
                      // _selectedIndex = 1;
                      // Navigator.pop(context,true);
                    });
                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      // color: Colors.white,
                      height: MediaQuery.of(context).size.height / 2,
                      width: MediaQuery.of(context).size.width / 3.5,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30.0),
                              bottomLeft: Radius.circular(30.0))),
                      child: SingleChildScrollView(
                        physics: NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            Image.asset(
                              "assets/jpgImages/userProfile.png",
                              fit: BoxFit.cover,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                // Positioned(
                //     right: 0,
                //     top: MediaQuery.of(context).size.height / 4,
                //     child: ClipRect(
                //       child: Container(
                //         child: Align(
                //           alignment: Alignment.centerLeft,
                //           // heightFactor: 0.5,
                //           widthFactor: 0.56,
                //           child: Image.asset(
                //             "assets/jpgImages/userProfile.png",
                //           ),
                //         ),
                //       ),
                //     )
                // ),
                // code of drawer items...
                Container(
                  width: MediaQuery.of(context).size.width / 2,
                  child: Center(
                    child: ListView.builder(
                        physics: ClampingScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: texts.length,
                        itemBuilder: (_, index) {
                          return Container(
                            child: GestureDetector(
                              onTap: () {
                                for (int i = 0; i < isHighlighted.length; i++) {
                                  setState(() {
                                    if (index == i) {
                                      if (index < 3) {
                                        // print(_selectedIndex);
                                        // print(index);
                                        _selectedIndex = index;
                                        isHighlighted[index] = true;
                                      } else if (index >= 3) {
                                        isHighlighted[index] = true;
                                      }
                                    } else {
                                      //the condition to change the highlighted item
                                      isHighlighted[i] = false;
                                    }
                                  });
                                }
                                Navigator.pop(context, true);
                                // Navigator.of(context).pop();
                              },
                              child: Column(
                                children: [
                                  Container(
                                    child: ListTile(
                                      title: Text(
                                        texts[index],
                                        style: GoogleFonts.abrilFatface(
                                          color: isHighlighted[index]
                                              ? kTextColor
                                              : Color(0XFF83AEAE),
                                        ),
                                        // style: TextStyle(
                                        //     color: isHighlighted[index]
                                        //         ? kTextColor
                                        //         : Color(0XFF83AEAE)),
                                      ),
                                      leading: SvgPicture.asset(icons[index],
                                          color: isHighlighted[index]
                                              ? kTextColor
                                              : Color(0XFF83AEAE)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                ),

                // code of setting and layout part
                // Column(
                //   mainAxisAlignment: MainAxisAlignment.end,
                //   children: [
                //     Padding(
                //       padding: const EdgeInsets.only(left: 20.0, bottom: 33.0),
                //       child: Row(
                //         children: [
                //           GestureDetector(
                //             onTap: () {
                //               setState(() {
                //                 _selectedIndex = 3;
                //                 _otherselectedIndex = 4;
                //                 Navigator.pop(context, true);
                //               });
                //             },
                //             child: Row(
                //               children: [
                //                 SvgPicture.asset(
                //                   "assets/images/settings.svg",
                //                   color: Color(0XFF83AEAE),
                //                 ),
                //                 Padding(
                //                   padding: EdgeInsets.only(left: 20.0),
                //                   child: Text(
                //                     "Setting",
                //                     style: TextStyle(color: Color(0XFF83AEAE)),
                //                   ),
                //                 ),
                //               ],
                //             ),
                //           ),
                //           Container(
                //               height: 20,
                //               child: VerticalDivider(
                //                 color: Color(0XFF83AEAE),
                //                 thickness: 2,
                //               )),
                //           Text(
                //             "Log Out",
                //             style: TextStyle(color: Color(0XFF83AEAE)),
                //           )
                //         ],
                //       ),
                //     ),
                //   ],
                // )
              ],
            ),
          )),
        ),
      ),
    );
  }
}
