import 'package:flutter/material.dart';
import 'package:image_editing_flutter_app/components/textStyle.dart';

// ignore: must_be_immutable
class MyTextEdit extends StatelessWidget {

  TextEditingController textEditingController = TextEditingController();
  final String label;
  bool isSecured;
  InputDecoration decoration;

   MyTextEdit({Key key, this.label,this.textEditingController, this.isSecured,this.decoration}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20.0),
      child: TextField(
        controller: textEditingController,
        style: appTextStyleWithoutBold(
          fontSize: 14
        ),
        obscureText: isSecured,
        decoration: decoration,
      ),
    );
  }
}
