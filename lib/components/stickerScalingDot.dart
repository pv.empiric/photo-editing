import 'package:flutter/material.dart';

class StickerScalingDot extends StatelessWidget {

  final double size;
  final Color color;
  StickerScalingDot({this.size,this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      height: size,
      width: size,
      decoration: new BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black,
            blurRadius: 2,
            offset: Offset(0.5,0.5)
          )
        ],
        shape: BoxShape.circle,
        color: color
      ),
    );
  }
}
