import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_editing_flutter_app/constant.dart';


const appFontFamily = "proximaNova";

TextStyle headerTextStyle = GoogleFonts.playfairDisplay(fontSize: 25);

// TextStyle contentTextStyle = TextStyle(fontFamily: "proximaNova", fontSize: 15);
// TextStyle profileTextStyle = TextStyle(fontFamily: "proximaNova", fontSize: 14,fontWeight: FontWeight.bold);
// TextStyle profile1TextStyle = TextStyle(fontFamily: "proximaNova", fontSize: 14);
// TextStyle content1TextStyle = TextStyle(fontFamily: "proximaNova", color: Colors.red,fontSize: 8);
// TextStyle bottomNavigationTextStyle = TextStyle(fontFamily: "proximaNova", color: Colors.black,fontSize: 7);
TextStyle photoEditBottomTextStyle(bool isselected) {
  return TextStyle(fontFamily: appFontFamily, color:isselected? Colors.black:kUnselectedColor,fontSize: 10,decoration: TextDecoration.underline);
}
// TextStyle bottomNavTextStyle = TextStyle(fontFamily: "proximaNova", color: Colors.red,fontSize: 10);
// TextStyle textStylePage = TextStyle(fontFamily: "proximaNova", color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold,);
TextStyle appBarTextStyle = TextStyle(fontFamily: appFontFamily, color: Colors.red,fontSize: 11);


TextStyle appTextStyleWithBold({Color textColor = Colors.black,double fontsize}){
  return TextStyle(
    fontFamily: appFontFamily,
    color: textColor,
    fontSize: fontsize,
    fontWeight: FontWeight.bold
  );
}

TextStyle appTextStyleWithoutBold({Color textColor = Colors.black,double fontSize}){
  return TextStyle(
      fontFamily: appFontFamily,
      color: textColor,
      fontSize: fontSize
  );
}