import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:image_editing_flutter_app/models/draft_template_model.dart';
import 'package:image_editing_flutter_app/models/saved_template_model.dart';
import 'package:image_editing_flutter_app/models/temp_rsponse.dart';
import 'package:image_editing_flutter_app/screens/Custom_Constants/database_helper.dart';

class EditZoomController extends GetxController{
  RxInt innerPhotoview = 1.obs;
  RxInt stickerview = 1.obs;
  RxInt textediting = 0.obs;
  RxBool isOpenTextbar = false.obs;
  RxBool isOpenBackground = false.obs;
  RxBool isOpenWidget = false.obs;
  RxBool isOpenSticker = false.obs;
  RxInt textPhotoviewController = 0.obs;
  RxBool callDecoratedfun = true.obs;
  RxDouble zoom = 1.0.obs;
  RxDouble previousZoom = 1.0.obs;
  Rx<Offset> previousOffset = Offset.zero.obs;
  Rx<Offset> offset = Offset.zero.obs;
  Rx<Offset> position = Offset(0,0).obs;
  RxString response = "".obs;
  RxInt primaryKey = 10.obs;
  RxBool isEditTemplate = false.obs;
  // ignore: non_constant_identifier_names
  RxString respomse_json = "".obs;
  RxList<SavedTemplate> dataList = <SavedTemplate>[].obs;
  RxList<Map<dynamic,dynamic>> imgList = [{}].obs;
  RxList<DraftTemplate> dataListDraft = <DraftTemplate>[].obs;
  RxList<Map<dynamic,dynamic>> imgListDraft = [{}].obs;
  RxBool isdraft = false.obs;
  RxBool isdraftLoading = false.obs;
  RxBool isfinalimageProcess = false.obs;
  RxList<TempResponse> tempResponse = <TempResponse>[].obs;

   // ignore: non_constant_identifier_names
   ListofFiles() async {
    var data = await DatabaseHelper.instance.queryAllRows();
    dataList.clear();
    data.forEach((element) {
      // print(element);
      var x = jsonDecode(jsonEncode(element)
          .replaceAll('\\"', '"')
          .replaceAll('\"[{', '[{')
          .replaceAll('}]\"}', '}]}')
          .replaceAll('\"{ "', '{"')
          .replaceAll('}"}', '}}'));
      dataList.add(SavedTemplate.fromJson(x));
      
    });
    imgList.clear();
    for (int i = 0; i < dataList.length; i++) {
      imgList.add({
        dataList[i].id:dataList[i].age
      });
    }
    return imgList;
    
  }

  // ignore: non_constant_identifier_names
  ListofFilesDraft() async {
    var data = await DatabaseHelper.instance.queryAllRowsDraft();
    dataListDraft.clear();
    data.forEach((element) {
      // print(element);
      var x = jsonDecode(jsonEncode(element)
          .replaceAll('\\"', '"')
          .replaceAll('\"[{', '[{')
          .replaceAll('}]\"}', '}]}')
          .replaceAll('\"{ "', '{"')
          .replaceAll('}"}', '}}'));
      dataListDraft.add(DraftTemplate.fromJson(x));
      
    });
    imgListDraft.clear();
    for (int i = 0; i < dataListDraft.length; i++) {
      imgListDraft.add({
        dataListDraft[i].draftId:dataListDraft[i].draftImagePath
      });
    }
    return imgListDraft;
    
  }
}

