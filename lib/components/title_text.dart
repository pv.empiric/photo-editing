import 'package:flutter/material.dart';
import 'package:image_editing_flutter_app/components/textStyle.dart';

class TitleText extends StatelessWidget {
  final String titleName;
  final Color finalColor;

  TitleText({this.titleName, this.finalColor});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          titleName,
          style: appTextStyleWithBold(
            fontsize: 16
          ),
        ),
      ],
    );
  }
}