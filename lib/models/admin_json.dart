class AdminJson {
  Templates templates;

  AdminJson({this.templates});

  AdminJson.fromJson(Map<String, dynamic> json) {
    templates = json['Templates'] != null
        ? new Templates.fromJson(json['Templates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.templates != null) {
      data['Templates'] = this.templates.toJson();
    }
    return data;
  }
}

class Templates {
  List<Funaral> funaral;
 

  Templates(
      {this.funaral,});

  Templates.fromJson(Map<String, dynamic> json) {
    if (json['Funaral'] != null) {
      funaral = <Funaral>[];
      json['Funaral'].forEach((v) {
        funaral.add(new Funaral.fromJson(v));
      });
    }
    
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.funaral != null) {
      data['Funaral'] = this.funaral.map((v) => v.toJson()).toList();
    }
    
    return data;
  }
}

class Funaral {
  List<Image> image;
  List<MainContainer> mainContainer;
  List<Sticker> sticker;

  Funaral({this.image, this.mainContainer, this.sticker});

  Funaral.fromJson(Map<String, dynamic> json) {
    if (json['Image'] != null) {
      image = <Image>[];
      json['Image'].forEach((v) {
        image.add(new Image.fromJson(v));
      });
    }
    if (json['Main-Container'] != null) {
      mainContainer = <MainContainer>[];
      json['Main-Container'].forEach((v) {
        mainContainer.add(new MainContainer.fromJson(v));
      });
    }
    if (json['sticker'] != null) {
      sticker = <Sticker>[];
      json['sticker'].forEach((v) {
        sticker.add(new Sticker.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.image != null) {
      data['Image'] = this.image.map((v) => v.toJson()).toList();
    }
    if (this.mainContainer != null) {
      data['Main-Container'] =
          this.mainContainer.map((v) => v.toJson()).toList();
    }
    if (this.sticker != null) {
      data['sticker'] = this.sticker.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Image {
  String left;
  String top;
  String height;
  String width;

  Image({this.left, this.top, this.height, this.width});

  Image.fromJson(Map<String, dynamic> json) {
    left = json['left'];
    top = json['top'];
    height = json['height'];
    width = json['width'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['left'] = this.left;
    data['top'] = this.top;
    data['height'] = this.height;
    data['width'] = this.width;
    return data;
  }
}

class MainContainer {
  String height;
  String marginVerticle;
  String marginHorizontal;
  String aspectRatio;

  MainContainer(
      {this.height,
      this.marginVerticle,
      this.marginHorizontal,
      this.aspectRatio});

  MainContainer.fromJson(Map<String, dynamic> json) {
    height = json['height'];
    marginVerticle = json['margin-verticle'];
    marginHorizontal = json['margin-horizontal'];
    aspectRatio = json['Aspect-ratio'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['height'] = this.height;
    data['margin-verticle'] = this.marginVerticle;
    data['margin-horizontal'] = this.marginHorizontal;
    data['Aspect-ratio'] = this.aspectRatio;
    return data;
  }
}

class Sticker {
  String stickerImage;
  String height;
  String width;

  Sticker({this.stickerImage, this.height, this.width});

  Sticker.fromJson(Map<String, dynamic> json) {
    stickerImage = json['stickerImage'];
    height = json['height'];
    width = json['width'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['stickerImage'] = this.stickerImage;
    data['height'] = this.height;
    data['width'] = this.width;
    return data;
  }
}