class StickerList {
  String name;
  double xPosition;
  double yPosition;
 
  double height;
 
  double width;
  double rotation;
  double scale;
  
  // final String text;

  StickerList({this.name,this.xPosition,this.yPosition,this.height,this.width,this.rotation,this.scale});
}
