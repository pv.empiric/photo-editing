import 'package:flutter/cupertino.dart';
// import 'package:event_bus/event_bus.dart';

// EventBus eventBus = EventBus();

class TextListModel {

  String text;
  TextStyle textStyle;
  TextStyle textStyle2;
  // FontWeight fontWeight;
  // double fontSize;
  double xPosition;
  double yPosition;
  // double xPosition1;
  // double yPosition1;
  double height;
  double width;
  // double rotationPosition;
  // bool isEditing;
  int textStyleIndex;
  int finalTextAlignIndex;
  Color textColor;
  Color strockColor;
  double strockValue;
  double textOpacity;
  double rotationAngle;
  double scaleFactor;
  // double finalHeight;
  // double finalWidth;
  // double heightScale;
  // double widthScale;
  // final double containerHeight;
  // final double containerWidth;



  TextListModel({
    // this.containerWidth,
    // this.fontSize,
    // this.containerHeight,
    // this.fontWeight,
    this.text,
    this.textStyle,
    this.xPosition,
    this.yPosition,
    // this.xPosition1,
    // this.yPosition1,
    this.height,
    this.width,
    // this.rotationPosition,
    // this.isEditing,
    this.textStyle2,
    this.textStyleIndex,
    this.finalTextAlignIndex,
    this.textColor,
    this.strockColor,
    this.strockValue,
    this.textOpacity,
    this.rotationAngle,
    this.scaleFactor,
    // this.heightScale,
    // this.widthScale,
    // this.finalHeight,
    // this.finalWidth
  });
}
