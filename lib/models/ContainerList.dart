import 'dart:io';

class ContainerList {
  double height;
  double width;
  double left;
  double top;
  double xposition;
  double yposition;
  File containerimage;
  double scale;
  double rotation;
  double datajsonheight;
  double datajsonwidth;

  ContainerList({
    this.height,
    this.left,
    this.top,
    this.width,
    this.xposition,
    this.yposition,
    this.containerimage,
    this.scale,
    this.rotation,
    this.datajsonheight,
    this.datajsonwidth,
  });
}
