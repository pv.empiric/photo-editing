class BackgroundCategory{
  String backgroundCategoryName;
  String backgroundCategoryImage;
  bool categoryIsSelected;
  BackgroundCategory({
    this.backgroundCategoryImage,
    this.backgroundCategoryName,
    this.categoryIsSelected
  });
}