class SavedTemplate {
  int id;
  String templateId;
  String age;
  JsonCode jsonCode;

  SavedTemplate({this.id, this.templateId, this.age, this.jsonCode});

  SavedTemplate.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    templateId = json['template_id'];
    age = json['age'];
    jsonCode = json['json_code'] != null
        ? new JsonCode.fromJson(json['json_code'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['template_id'] = this.templateId;
    data['age'] = this.age;
    if (this.jsonCode != null) {
      data['json_code'] = this.jsonCode.toJson();
    }
    return data;
  }
}

class JsonCode {
  MainContainer mainContainer;
  List<Sticker> sticker;
  BackGround backGround;
  List<Containers> containers;
  List<Textlist> textlist;

  JsonCode(
      {this.mainContainer,
      this.sticker,
      this.backGround,
      this.containers,
      this.textlist});

  JsonCode.fromJson(Map<String, dynamic> json) {
    mainContainer = json['mainContainer'] != null
        ? new MainContainer.fromJson(json['mainContainer'])
        : null;
    if (json['Sticker'] != null) {
      // ignore: deprecated_member_use
      sticker = new List<Sticker>();
      json['Sticker'].forEach((v) {
        sticker.add(new Sticker.fromJson(v));
      });
    }
    backGround = json['BackGround'] != null
        ? new BackGround.fromJson(json['BackGround'])
        : null;
    if (json['containers'] != null) {
      // ignore: deprecated_member_use
      containers = new List<Containers>();
      json['containers'].forEach((v) {
        containers.add(new Containers.fromJson(v));
      });
    }
    if (json['Textlist'] != null) {
      // ignore: deprecated_member_use
      textlist = new List<Textlist>();
      json['Textlist'].forEach((v) {
        textlist.add(new Textlist.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mainContainer != null) {
      data['mainContainer'] = this.mainContainer.toJson();
    }
    if (this.sticker != null) {
      data['Sticker'] = this.sticker.map((v) => v.toJson()).toList();
    }
    if (this.backGround != null) {
      data['BackGround'] = this.backGround.toJson();
    }
    if (this.containers != null) {
      data['containers'] = this.containers.map((v) => v.toJson()).toList();
    }
    if (this.textlist != null) {
      data['Textlist'] = this.textlist.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MainContainer {
  String aspectRatio;
  String height;
  String width;
  String marginVerticle;
  String marginHorizontal;

  MainContainer(
      {this.aspectRatio,
      this.height,
      this.width,
      this.marginVerticle,
      this.marginHorizontal});

  MainContainer.fromJson(Map<String, dynamic> json) {
    aspectRatio = json['aspectRatio'];
    height = json['height'];
    width = json['width'];
    marginVerticle = json['marginVerticle'];
    marginHorizontal = json['marginHorizontal'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['aspectRatio'] = this.aspectRatio;
    data['height'] = this.height;
    data['width'] = this.width;
    data['marginVerticle'] = this.marginVerticle;
    data['marginHorizontal'] = this.marginHorizontal;
    return data;
  }
}

class Sticker {
  String name;
  String width;
  String height;
  String xposition;
  String yposition;
  String scale;
  String rotation;

  Sticker(
      {this.name,
      this.width,
      this.height,
      this.xposition,
      this.yposition,
      this.scale,
      this.rotation});

  Sticker.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    width = json['width'];
    height = json['height'];
    xposition = json['xposition'];
    yposition = json['yposition'];
    scale = json['scale'];
    rotation = json['rotation'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['width'] = this.width;
    data['height'] = this.height;
    data['xposition'] = this.xposition;
    data['yposition'] = this.yposition;
    data['scale'] = this.scale;
    data['rotation'] = this.rotation;
    return data;
  }
}

class BackGround {
  String background;

  BackGround({this.background});

  BackGround.fromJson(Map<String, dynamic> json) {
    background = json['Background'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Background'] = this.background;
    return data;
  }
}

class Containers {
  String width;
  String height;
  String xposition;
  String yposition;
  String containerImage;
  String scale;
  String rotation;

  Containers(
      {this.width,
      this.height,
      this.xposition,
      this.yposition,
      this.containerImage,
      this.scale,
      this.rotation});

  Containers.fromJson(Map<String, dynamic> json) {
    width = json['width'];
    height = json['height'];
    xposition = json['xposition'];
    yposition = json['yposition'];
    containerImage = json['containerImage'];
    scale = json['scale'];
    rotation = json['rotation'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['width'] = this.width;
    data['height'] = this.height;
    data['xposition'] = this.xposition;
    data['yposition'] = this.yposition;
    data['containerImage'] = this.containerImage;
    data['scale'] = this.scale;
    data['rotation'] = this.rotation;
    return data;
  }
}

class Textlist {
  String text;
  String xPosition;
  String yposition;
  String height;
  String width;
  String textStyleIndex;
  String finalTextAlignIndex;
  String textColor;
  String strockColor;
  String strockValue;
  String textOpacity;
  String rotationAngle;
  String scaleFactor;

  Textlist(
      {this.text,
      this.xPosition,
      this.yposition,
      this.height,
      this.width,
      this.textStyleIndex,
      this.finalTextAlignIndex,
      this.textColor,
      this.strockColor,
      this.strockValue,
      this.textOpacity,
      this.rotationAngle,
      this.scaleFactor});

  Textlist.fromJson(Map<String, dynamic> json) {
    text = json['text'];
    xPosition = json['xPosition'];
    yposition = json['yposition'];
    height = json['height'];
    width = json['width'];
    textStyleIndex = json['textStyleIndex'];
    finalTextAlignIndex = json['finalTextAlignIndex'];
    textColor = json['textColor'];
    strockColor = json['strockColor'];
    strockValue = json['strockValue'];
    textOpacity = json['textOpacity'];
    rotationAngle = json['rotationAngle'];
    scaleFactor = json['scaleFactor'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['text'] = this.text;
    data['xPosition'] = this.xPosition;
    data['yposition'] = this.yposition;
    data['height'] = this.height;
    data['width'] = this.width;
    data['textStyleIndex'] = this.textStyleIndex;
    data['finalTextAlignIndex'] = this.finalTextAlignIndex;
    data['textColor'] = this.textColor;
    data['strockColor'] = this.strockColor;
    data['strockValue'] = this.strockValue;
    data['textOpacity'] = this.textOpacity;
    data['rotationAngle'] = this.rotationAngle;
    data['scaleFactor'] = this.scaleFactor;
    return data;
  }
}
