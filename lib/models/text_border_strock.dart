import 'package:flutter/material.dart';

class FinalTextAlign{
  TextAlign textAlign;
  String textAlignImage;
  TextDirection textDirection;
  FinalTextAlign({
    @required this.textAlign,
    @required this.textAlignImage,
    @required this.textDirection
  });

}