// To parse this JSON data, do
//
//     final tempResponse = tempResponseFromJson(jsonString);

import 'dart:convert';

TempResponse tempResponseFromJson(String str) => TempResponse.fromJson(json.decode(str));

String tempResponseToJson(TempResponse data) => json.encode(data.toJson());

class TempResponse {
    TempResponse({
        this.success,
        this.data,
        this.message,
    });

    bool success;
    Data data;
    String message;

    factory TempResponse.fromJson(Map<String, dynamic> json) => TempResponse(
        success: json["success"] == null ? null : json["success"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        message: json["message"] == null ? null : json["message"],
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "data": data == null ? null : data.toJson(),
        "message": message == null ? null : message,
    };
}

class Data {
    Data({
        this.totalPage,
        this.products,
    });

    int totalPage;
    List<Product> products;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        totalPage: json["total_page"] == null ? null : json["total_page"],
        products: json["products"] == null ? null : List<Product>.from(json["products"].map((x) => Product.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total_page": totalPage == null ? null : totalPage,
        "products": products == null ? null : List<dynamic>.from(products.map((x) => x.toJson())),
    };
}

class Product {
    Product({
        this.pid,
        this.productName,
        this.productCode,
        this.productCategory,
        this.productPrice,
        this.productInformation,
        this.additionalProductInformation,
        this.productRating,
        this.productRateBy,
        this.productFabric,
        this.discountEnable,
        this.productDiscountRate,
        this.productDiscount,
        this.productTrending,
        this.productImages,
    });

    int pid;
    String productName;
    String productCode;
    String productCategory;
    String productPrice;
    String productInformation;
    List<AdditionalProductInformation> additionalProductInformation;
    String productRating;
    String productRateBy;
    String productFabric;
    String discountEnable;
    String productDiscountRate;
    String productDiscount;
    String productTrending;
    List<String> productImages;

    factory Product.fromJson(Map<String, dynamic> json) => Product(
        pid: json["pid"] == null ? null : json["pid"],
        productName: json["product_name"] == null ? null : json["product_name"],
        productCode: json["product_code"] == null ? null : json["product_code"],
        productCategory: json["product_category"] == null ? null : json["product_category"],
        productPrice: json["product_price"] == null ? null : json["product_price"],
        productInformation: json["product_information"] == null ? null : json["product_information"],
        additionalProductInformation: json["additional_product_information"] == null ? null : List<AdditionalProductInformation>.from(json["additional_product_information"].map((x) => AdditionalProductInformation.fromJson(x))),
        productRating: json["product_rating"] == null ? null : json["product_rating"],
        productRateBy: json["product_rate_by"] == null ? null : json["product_rate_by"],
        productFabric: json["product_fabric"] == null ? null : json["product_fabric"],
        discountEnable: json["discount_enable"] == null ? null : json["discount_enable"],
        productDiscountRate: json["product_discount_rate"] == null ? null : json["product_discount_rate"],
        productDiscount: json["product_discount"] == null ? null : json["product_discount"],
        productTrending: json["product_trending"] == null ? null : json["product_trending"],
        productImages: json["product_images"] == null ? null : List<String>.from(json["product_images"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "pid": pid == null ? null : pid,
        "product_name": productName == null ? null : productName,
        "product_code": productCode == null ? null : productCode,
        "product_category": productCategory == null ? null : productCategory,
        "product_price": productPrice == null ? null : productPrice,
        "product_information": productInformation == null ? null : productInformation,
        "additional_product_information": additionalProductInformation == null ? null : List<dynamic>.from(additionalProductInformation.map((x) => x.toJson())),
        "product_rating": productRating == null ? null : productRating,
        "product_rate_by": productRateBy == null ? null : productRateBy,
        "product_fabric": productFabric == null ? null : productFabric,
        "discount_enable": discountEnable == null ? null : discountEnable,
        "product_discount_rate": productDiscountRate == null ? null : productDiscountRate,
        "product_discount": productDiscount == null ? null : productDiscount,
        "product_trending": productTrending == null ? null : productTrending,
        "product_images": productImages == null ? null : List<dynamic>.from(productImages.map((x) => x)),
    };
}

class AdditionalProductInformation {
    AdditionalProductInformation({
        this.type,
        this.occasion,
        this.work,
        this.size,
        this.color,
        this.additionalProductInformationWork,
    });

    String type;
    String occasion;
    String work;
    String size;
    String color;
    String additionalProductInformationWork;

    factory AdditionalProductInformation.fromJson(Map<String, dynamic> json) => AdditionalProductInformation(
        type: json["type"] == null ? null : json["type"],
        occasion: json["occasion"] == null ? null : json["occasion"],
        work: json["WORK"] == null ? null : json["WORK"],
        size: json["size"] == null ? null : json["size"],
        color: json["color"] == null ? null : json["color"],
        additionalProductInformationWork: json["work"] == null ? null : json["work"],
    );

    Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "occasion": occasion == null ? null : occasion,
        "WORK": work == null ? null : work,
        "size": size == null ? null : size,
        "color": color == null ? null : color,
        "work": additionalProductInformationWork == null ? null : additionalProductInformationWork,
    };
}
