import 'package:flutter/cupertino.dart';

class TamplateModel{
  // ignore: non_constant_identifier_names
  String tamplate_image;
  bool isSelected;

  TamplateModel({
    // ignore: non_constant_identifier_names
    @required this.tamplate_image,
    this.isSelected,
  });
}