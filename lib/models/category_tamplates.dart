import 'package:image_editing_flutter_app/models/tamplate_model.dart';

class CategoryTamplates{
  String categoryName;
  List<TamplateModel> categories;

  CategoryTamplates({
    this.categoryName,
    this.categories
  });

}