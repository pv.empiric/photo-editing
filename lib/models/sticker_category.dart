class StickerCategory{
  String stickerCategoryName;
  String stickerCategoryImage;
  bool categoryIsSelected;
  StickerCategory({
    this.stickerCategoryImage,
    this.stickerCategoryName,
    this.categoryIsSelected
  });
}