import 'package:flutter/cupertino.dart';

class DragDrop {
  Offset position1;
  Offset position2;
  double top1;
  double top2;
  double left1;
  double left2;
  double height;
  double width;
  double screenwidth;
  double screenheight;
  bool checkOrientation;

  DragDrop({this.left1,this.left2,this.position1,this.position2,this.top1,this.top2,this.width,this.height,this.checkOrientation,this.screenheight,this.screenwidth});
}