import 'package:flutter/material.dart';

class FinalTextAlign {
  TextAlign textAlign;
  String textAlignImage;
  TextDirection textDirection;
  FinalTextAlign(
      {this.textAlign, @required this.textAlignImage, this.textDirection});
}
